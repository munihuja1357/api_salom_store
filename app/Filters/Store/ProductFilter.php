<?php


namespace App\Filters\Store;


use App\Filters\QueryFilter;

class ProductFilter extends QueryFilter
{
    public function name($query)
    {
        if (strlen($query) > 1) {
            return $this->builder->where('name', 'LIKE', "%$query%");
        }
    }
}
