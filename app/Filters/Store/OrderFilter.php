<?php
namespace App\Filters\Store;

use App\Filters\QueryFilter;

class OrderFilter extends QueryFilter
{
    public function active($query)
    {
        if (strlen($query) > 1) {
            return $this->builder
                ->whereNull('delivered_at')
                ->orWhereNull('cancelled_at');
        }
    }

    public function store($query)
    {
        if ($query) {
            return $this->builder->where('store_id', $query);
        }
    }

    public function customer()
    {
        return $this->builder->with('customer');
    }
}
