<?php

namespace App\Jobs;

use App\UseCases\Cpanel\Db;
use App\UseCases\Cpanel\Domain;

class SetupFirm
{
    public function handle()
    {
        /*if (config('app.env') === 'production') {
            (new \App\UseCases\Cpanel\Db)->setup();
            (new \App\UseCases\Cpanel\Domain)->addSubDomain();
        }*/

        (new \App\UseCases\Cpanel\Db)->setup();
        (new \App\UseCases\Cpanel\Domain)->addSubDomain();
    }
}
