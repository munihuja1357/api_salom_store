<?php

namespace App\Jobs;

use Stancl\Tenancy\Contracts\Tenant;
use Illuminate\Support\Facades\Artisan;

class CreateFrameworkDirectoriesForTenant
{
    protected $tenant;

    public function __construct(Tenant $tenant)
    {
        $this->tenant = $tenant;
    }

    public function handle()
    {
        $this->tenant->run(function ($tenant) {
            $storage_path = storage_path();

            mkdir("$storage_path/framework/cache", 0777, true);
            mkdir("$storage_path/app/public", 0777, true);
            mkdir("$storage_path/logs", 0777, true);

            \Log::debug('Folders for tenancy created.');
        });
    }
}
