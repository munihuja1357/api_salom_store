<?php

namespace App\Notifications;

use App\Services\Sms\SmsSender;
use Fouladgar\MobileVerification\Contracts\SMSClient;
use Fouladgar\MobileVerification\Notifications\Messages\Payload;

class SmsChannel implements SMSClient
{
    private $sender;

    public function __construct(SmsSender $sender)
    {
        $this->sender = $sender;
    }

    public function sendMessage(Payload $payload): void
    {
        $this->sender->send($payload->getTo(), $payload->getToken());
    }
}
