<?php

namespace App\Providers;

use Inertia\Inertia;
use Illuminate\Support\ServiceProvider;

class InertiaServiceProvider extends ServiceProvider
{
    public function register()
    {
        Inertia::version(function () {
            return md5_file(public_path('mix-manifest.json'));
        });

        Inertia::share([
            'menu' => function () {
                if (auth()->check() && auth()->user()->hasRole('Store')) {
                    $menu = collect([
                        [
                            'title' => 'Главная панель',
                            'route' => route('store:dashboard'),
                            'icon' => 'estate',
                            'active' => '',
                        ],
                        [
                            'title' => 'Заказы',
                            'route' => route('store:orders.index'),
                            'active' => request()->routeIs('store:orders.index'),
                            'icon' => 'clipboard-notes',
                        ],
                        [
                            'title' => 'Магазин',
                            'route' => '',
                            'icon' => 'shopping-cart',
                            'children' => [
                                [
                                    'title' => 'Прайс-лист',
                                    'route' => route('store:price_list.index'),
                                ],
                                [
                                    'title' => 'Накладные',
                                    'route' => route('store:overheads.index'),
                                ],
                            ],
                        ],
                    ]);
                } else {
                    $menu = collect([
                        [
                            'title' => 'Главная панель',
                            'route' => route('manager:dashboard'),
                            'icon' => 'estate',
                            'active' => request()->routeIs('manager:dashboard'),
                        ],
                        [
                            'title' => 'Заказы',
                            'route' => route('manager:orders.index'),
                            'active' => request()->routeIs('manager:orders.index'),
                            'icon' => 'clipboard-notes',
                        ],
                        [
                            'title' => 'Магазины',
                            'route' => route('manager:stores.index'),
                            'active' => request()->routeIs('manager:stores.index'),
                            'icon' => 'shopping-cart',
                        ],
                    ]);
                }

                return $menu->all();
            },
            'user' => function () {
                if (auth()->check()) {
                    return [
                        'id' => auth()->user()->id,
                        'full_name' => auth()->user()->FULL_NAME,
                        'avatar_url' => auth()->user()->avatar_url,
                        'email' => auth()->user()->email,
                    ];
                }

                return [];
            },
            'errors' => function () {
                if (session()->get('errors')) {
                    return session()->get('errors')->getBag('default')->getMessages();
                }

                return (object)[];
            },

            'flash' => function () {
                return session()->get('message');
            },
        ]);
    }
}
