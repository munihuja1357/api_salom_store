<?php

namespace App\Providers;

use App\Services\Sms\SmsRu;
use App\Services\Sms\OsonSms;
use App\Services\Sms\SmsSender;
use App\Services\Sms\ArraySender;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Foundation\Application;

class SmsSenderProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton(SmsSender::class, function (Application $app) {
            $config = $app->make('config')->get('sms');

            switch ($config['driver']) {
                case 'sms.ru':
                    $params = $config['drivers']['sms.ru'];

                    if (!empty($params['url'])) {
                        return new SmsRu($params['app_id'], $params['url']);
                    }

                    return new SmsRu($params['app_id']);
                case 'osonsms':
                    $params = $config['drivers']['osonsms'];

                    if (!empty($params['url'])) {
                        return new OsonSms(
                            $params['login'],
                            $params['hash'],
                            $params['url']
                        );
                    }

                    return new OsonSms($params['login'], $params['hash']);
                case 'array':
                    return new ArraySender();
                default:
                    throw new \InvalidArgumentException('Неопределенный SMS-драйвер '.$config['driver']);
            }
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        //
    }
}
