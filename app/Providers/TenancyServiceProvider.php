<?php

declare(strict_types=1);

namespace App\Providers;

use Stancl\Tenancy\Jobs;
use Stancl\Tenancy\Events;
use Stancl\Tenancy\Listeners;
use Stancl\Tenancy\Middleware;
use Stancl\JobPipeline\JobPipeline;
use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use App\Jobs\CreateFrameworkDirectoriesForTenant;
use Illuminate\Database\Eloquent\Relations\Relation;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;
use App\Observers\OrderObserver;
use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Entity\Tenant\Firm\Store\Overhead;
use App\Observers\Firm\Store\OverheadObserver;
use App\Listeners\Customer\SendVerificationSmsNotification;
use App\Events\Customer\CustomerRegistered;

class TenancyServiceProvider extends ServiceProvider
{
    public function events()
    {
        return [
            // Firm events
            Events\CreatingTenant::class => [],
            Events\TenantCreated::class => [
                JobPipeline::make([
                    Jobs\CreateDatabase::class,
                    // SetupFirm::class,
                    Jobs\MigrateDatabase::class,
                    Jobs\SeedDatabase::class,

                    CreateFrameworkDirectoriesForTenant::class,
                    // Your own jobs to prepare the tenant.
                    // Provision API keys, create S3 buckets, anything you want!

                ])->send(function (Events\TenantCreated $event) {
                    return $event->tenant;
                })->shouldBeQueued(false), // `false` by default, but you probably want to make this `true` for production.
            ],
            Events\SavingTenant::class => [],
            Events\TenantSaved::class => [],
            Events\UpdatingTenant::class => [],
            Events\TenantUpdated::class => [],
            Events\DeletingTenant::class => [],
            Events\TenantDeleted::class => [
                JobPipeline::make([
                    Jobs\DeleteDatabase::class,
                ])->send(function (Events\TenantDeleted $event) {
                    return $event->tenant;
                })->shouldBeQueued(false), // `false` by default, but you probably want to make this `true` for production.
            ],

            // Domain events
            Events\CreatingDomain::class => [],
            Events\DomainCreated::class => [],
            Events\SavingDomain::class => [],
            Events\DomainSaved::class => [],
            Events\UpdatingDomain::class => [],
            Events\DomainUpdated::class => [],
            Events\DeletingDomain::class => [],
            Events\DomainDeleted::class => [],

            // Database events
            Events\DatabaseCreated::class => [],
            Events\DatabaseMigrated::class => [],
            Events\DatabaseSeeded::class => [],
            Events\DatabaseRolledBack::class => [],
            Events\DatabaseDeleted::class => [],

            // Tenancy events
            Events\InitializingTenancy::class => [],
            Events\TenancyInitialized::class => [
                Listeners\BootstrapTenancy::class,
            ],

            Events\EndingTenancy::class => [],
            Events\TenancyEnded::class => [
                Listeners\RevertToCentralContext::class,
            ],

            Events\BootstrappingTenancy::class => [
            ],
            Events\TenancyBootstrapped::class => [
            ],
            Events\RevertingToCentralContext::class => [],
            Events\RevertedToCentralContext::class => [],

            // Resource syncing
            Events\SyncedResourceSaved::class => [
                Listeners\UpdateSyncedResource::class,
            ],

            // Fired only when a synced resource is changed in a different DB than the origin DB (to avoid infinite loops)
            Events\SyncedResourceChangedInForeignDatabase::class => [],

            CustomerRegistered::class => [
                SendVerificationSmsNotification::class,
            ],
        ];
    }

    public function register()
    {
        if ($this->app->environment('local')) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    public function boot()
    {
        Relation::morphMap([
            'user' => 'App\Entity\Tenant\Firm\User',
            'product' => 'App\Entity\Tenant\Firm\Store\Product',
            'courier' => 'App\Entity\Tenant\Firm\Store\Courier',
            'customer' => 'App\Entity\Tenant\Firm\Customer\Customer',
            'store' => 'App\Entity\Tenant\Firm\Store\Store',
        ]);

        Overhead::observe(OverheadObserver::class);

        $this->bootEvents();
        $this->mapRoutes();

        $this->makeTenancyMiddlewareHighestPriority();

        $this->defineOwner();
        $this->defineAutoDiscover();
    }

    protected function bootEvents()
    {
        foreach ($this->events() as $event => $listeners) {
            foreach (array_unique($listeners) as $listener) {
                if ($listener instanceof JobPipeline) {
                    $listener = $listener->toListener();
                }

                Event::listen($event, $listener);
            }
        }
    }

    protected function mapRoutes()
    {
        if (file_exists(base_path('routes/tenant/api.php'))) {
            Route::prefix('api')
                ->name('api.')
                ->middleware([
                    'api',
                    InitializeTenancyByDomain::class,
                    PreventAccessFromCentralDomains::class,
                ])
                ->group(base_path('routes/tenant/api.php'));
        }
        if (file_exists(base_path('routes/tenant/web.php'))) {
            Route::middleware([
                    'web',
                    InitializeTenancyByDomain::class,
                    PreventAccessFromCentralDomains::class,
                ])
                ->group(base_path('routes/tenant/web.php'));
        }
    }

    protected function makeTenancyMiddlewareHighestPriority()
    {
        $tenancyMiddleware = [
            // Even higher priority than the initialization middleware
            Middleware\PreventAccessFromCentralDomains::class,

            Middleware\InitializeTenancyByDomain::class,
            Middleware\InitializeTenancyBySubdomain::class,
            Middleware\InitializeTenancyByDomainOrSubdomain::class,
            Middleware\InitializeTenancyByPath::class,
            Middleware\InitializeTenancyByRequestData::class,
        ];

        foreach ($tenancyMiddleware as $middleware) {
            $this->app[Kernel::class]->prependToMiddlewarePriority($middleware);
        }
    }

    private function defineAutoDiscover()
    {
        Gate::guessPolicyNamesUsing(function ($class) {
            return str_replace('\\Entity\\Tenant\\Firm\\', '\\Policies\\Tenant\\Firm\\', $class).'Policy';
        });
    }

    private function defineOwner()
    {
        Gate::before(function ($user) {
            return $user->hasRole('owner') ? true : null;
        });
    }
}
