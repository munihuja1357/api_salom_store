<?php

namespace App\Events\Customer;

use Illuminate\Foundation\Events\Dispatchable;
use App\Entity\Tenant\Firm\Customer\Customer;

class CustomerRegistered
{
    use Dispatchable;

    public Customer $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }
}
