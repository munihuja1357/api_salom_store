<?php

namespace App\Utilities;

use DateTimeInterface;
use Spatie\MediaLibrary\Support\UrlGenerator\BaseUrlGenerator;

class TenantAwareUrlGenerator extends BaseUrlGenerator
{
    public function getUrl(): string
    {
        $url = asset($this->getPathRelativeToRoot());

        $url = $this->versionUrl($url);

        return $url;
    }

    public function getPath(): string
    {
        // TODO: Implement getPath() method.
    }

    public function getTemporaryUrl(DateTimeInterface $expiration, array $options = []): string
    {
        // TODO: Implement getTemporaryUrl() method.
    }

    public function getResponsiveImagesDirectoryUrl(): string
    {
        // TODO: Implement getResponsiveImagesDirectoryUrl() method.
    }
}
