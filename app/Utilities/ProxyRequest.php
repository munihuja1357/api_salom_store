<?php

namespace App\Utilities;

use Illuminate\Http\Request;

class ProxyRequest
{
    public function grantPasswordToken(array $params)
    {
        return $this->makePostRequest($params);
    }

    public function refreshAccessToken()
    {
        $refreshToken = request()->cookie('refresh_token');
        abort_unless($refreshToken, 403, 'Your refresh token is expired.');

        $params = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refreshToken,
            'client_id' => request('client_id'),
            'client_secret' => request('client_secret'),
            'scope' => '*',
        ];

        return $this->makePostRequest($params);
    }

    public function makePostRequest(array $params)
    {
        $proxy = Request::create('oauth/token', 'post', $params);
        $resp = json_decode((string) app()->handle($proxy)->getContent());
        $this->setHttpOnlyCookie($resp->refresh_token);

        return $resp;
    }

    protected function setHttpOnlyCookie(string $refreshToken)
    {
        cookie()->queue(
            'refresh_token',
            $refreshToken,
            14400, // 10 days
            null,
            null,
            false,
            true // httponly
        );
    }
}
