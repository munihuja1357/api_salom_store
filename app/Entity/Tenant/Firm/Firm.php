<?php

namespace App\Entity\Tenant\Firm;

use Stancl\Tenancy\Contracts\TenantWithDatabase;
use Stancl\Tenancy\Database\Concerns\HasDomains;
use Stancl\Tenancy\Database\Concerns\HasDatabase;
use Stancl\Tenancy\Database\Models\Tenant as BaseTenant;

class Firm extends BaseTenant implements TenantWithDatabase
{
    use HasDatabase, HasDomains;

    const STATUS_UNPAID = 'unpaid';

    const STATUS_PAID = 'paid';

    const STATUS_CLOSED = 'closed';

    protected $table = 'firms';

    public function domains()
    {
        return $this->hasMany(config('tenancy.domain_model'), 'firm_id');
    }

    public static function getCustomColumns(): array
    {
        return [
            'id',
            'name',
            'address',
            'city_id',
            'slug',
            'tenant_id',
            'is_main',
            'status',
            'tenancy_db_name',
            'tenancy_db_username',
            'tenancy_db_password',
            'created_at',
            'updated_at',
        ];
    }

    protected $fillable = ['id', 'name', 'address', 'tenant_id', 'status', 'city_id', 'is_main', 'tenancy_db_name', 'tenancy_db_username', 'tenancy_db_password', 'slug'];
}
