<?php

namespace App\Entity\Tenant\Firm;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = [
        'account_id',
        'amount',
        'date',
        'note',
        'parent_id',
        'type_id',
        'order_id',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(TransactionType::class, 'type_id', 'id');
    }

}
