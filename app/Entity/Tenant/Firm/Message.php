<?php

namespace App\Entity\Tenant\Firm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'chat_id',
        'message_from_model_id',
        'message_from_model_type',
        'message_to_model_id',
        'message_to_model_type',
        'text',
        'is_seen',
        'sort',
        'replied_id',
    ];

    public function message_from_model()
    {
        return $this->morphTo();
    }
    public function message_to_model()
    {
        return $this->morphTo();
    }
    public function replied()
    {
        return $this->belongsTo(User::class, 'replied_id', 'id');
    }
}
