<?php

namespace App\Entity\Tenant\Firm;

use Illuminate\Database\Eloquent\Model;
use App\Entity\Tenant\Firm\Store\Order\Order;

/**
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $address_secondary
 * @property double $lat
 * @property double $lng
 * @property string $phone
 * @property string $additional_phone
 * @property string $company
 * @property string $alias
 * @property string $custom_fields
 **/
class Address extends Model
{
    protected $table = 'addresses';

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
