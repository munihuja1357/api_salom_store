<?php

namespace App\Entity\Tenant\Firm;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false;
}
