<?php

namespace App\Entity\Tenant\Firm;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = ['id', 'domain', 'firm_id'];

    public function tenant()
    {
        return $this->belongsTo(config('tenancy.tenant_model'), 'firm_id', 'id');
    }
}
