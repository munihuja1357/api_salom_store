<?php

namespace App\Entity\Tenant\Firm;

use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    protected $table = 'account_types';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function accounts()
    {
        return $this->belongsToMany(Account::class);
    }
}
