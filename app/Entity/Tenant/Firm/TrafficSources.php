<?php

namespace App\Entity\Tenant\Firm;

use Illuminate\Database\Eloquent\Model;

class TrafficSources extends Model
{
    protected $table = 'traffic_sources';
}
