<?php

namespace App\Entity\Tenant\Firm\Customer;

use App\Entity\Tenant\Firm\Chat;
use App\Entity\Tenant\Firm\Message;
use Carbon\Carbon;
use App\Entity\Tenant\Firm\City;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Entity\Tenant\Firm\Account;
use App\Entity\Tenant\Firm\Address;

/**
 * @property int $id
 * @property string $full_name
 * @property string $last_name
 * @property string $first_name
 * @property string $phone
 * @property string $email
 * @property Address $addresses
 * @property City $city
 * @property bool $phone_verified
 * @property string $password
 * @property string $verify_token
 * @property string $verification_code
 * @property Carbon $verification_code_expire
 */
class Customer extends Authenticatable
{
    use Notifiable, HasApiTokens;

    const STATUS_NOT_VERIFIED = 'not_verified';

    const STATUS_DEACTIVATED = 'deactivated';

    const STATUS_ACTIVE = 'active';
    const STATUS_WAIT = 'wait';

    protected $table = 'customers';

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'phone_verified',
        'verification_code',
        'verification_code_expire',
        'email',
        'password',
        'city_id',
        'status',
    ];

    protected $hidden = [
        'password',
    ];

    protected $casts = [
        'phone_verified' => 'boolean',
        'verification_code_expire' => 'datetime',
    ];

    public function username()
    {
        return 'phone';
    }

    public function isWait(): bool
    {
        return $this->status === self::STATUS_WAIT;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function unverifyPhone(): void
    {
        $this->phone_verified = false;
        $this->verification_code = null;
        $this->verification_code_expire = null;
        $this->saveOrFail();
    }

    public function requestPhoneVerification(): string
    {
        if (empty($this->phone)) {
            throw new \DomainException('Номер телефона пустой.');
        }
        if (!empty($this->verification_code) && $this->verification_code_expire && $this->verification_code_expire->gt(now())) {
            throw new \DomainException('Код подтверждения уже отправлен.');
        }
        $this->phone_verified = false;
        $this->verification_code = (string) random_int(10000, 99999);
        $this->verification_code_expire = now()->copy()->addMinute(config('auth.customer.verification_code_lifetime'));
        $this->saveOrFail();

        return $this->verification_code;
    }

    public function verifyPhone($code): void
    {
        if ($code !== $this->verification_code) {
            throw new \DomainException('Неверный код подтверждения.');
        }
        if ($this->verification_code_expire->lt(now())) {
            throw new \DomainException('Код подтверждения просрочен.');
        }
        $this->phone_verified = true;
        $this->verification_code = null;
        $this->verification_code_expire = null;
        $this->saveOrFail();
    }

    public function findForPassport($phoneNumber)
    {
        return static::where('phone', '=', $phoneNumber)->first();
    }

    /**
     * Check the verification code is valid.
     *
     * @return bool
     */
    public function validateForPassportPasswordGrant()
    {
        return true;
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function accounts()
    {
        return $this->morphMany(Account::class, 'owner');
    }


    public function chat()
    {
        return $this->morphMany(Chat::class, 'chat_model');
    }

    public function classModel()
    {
        return Customer::class;
    }

    public function message_to()
    {
        return $this->morphMany(Message::class, 'message_to_model');
    }

    public function message_from()
    {
        return $this->morphMany(Message::class, 'message_from_model');
    }
}
