<?php

namespace App\Entity\Tenant\Firm\Customer;

use App\Entity\Tenant\Firm\Store\Store;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use SoftDeletes;
    protected $table = 'carts';
    protected $fillable = [
        'name',
        'customer_id',
        'store_id',
        'is_template'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
    public function cart_items()
    {
        return $this->hasMany(CartItem::class);
    }
}
