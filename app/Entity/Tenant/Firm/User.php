<?php

namespace App\Entity\Tenant\Firm;

use Spatie\MediaLibrary\HasMedia;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use App\Entity\Tenant\Firm\Store\Store;
use Illuminate\Notifications\Notifiable;
use App\Entity\Tenant\Firm\Store\Courier;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Utilities\GuardedModel;

/**
 * @property int $id
 * @property string $full_name
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property string $gender
 **/
class User extends Authenticatable implements HasMedia
{
    use Notifiable, InteractsWithMedia, HasRoles, HasApiTokens, GuardedModel;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'email',
        'gender',
        'timezone',
        'lang_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected function authView(): array
    {
        return [];
    }

    protected function authUpdate(): array
    {
        return [];
    }


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'avatar_url',
        'full_name',
    ];

    public function store()
    {
        return $this->belongsTo(Store::class, 'id', 'owner_id');
    }

    public function getAvatarUrlAttribute()
    {
        $avatar = $this->getMedia('avatars')->last();

        if ($avatar) {
            return $avatar->getUrl();
        }

        return url('img/avatar.jpg');
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function courier()
    {
        return $this->hasOne(Courier::class);
    }


}
