<?php

namespace App\Entity\Tenant\Firm;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'accounts';

    protected $fillable = [
        'name',
        'type_id',
        'balance',
        'date_opened',
        'date_closed',
        'is_blocked',
    ];

    public $timestamps = false;

    public function type()
    {
        return $this->belongsTo(AccountType::class, 'type_id', 'id');
    }

    public function owner()
    {
        return $this->morphTo();
    }


}
