<?php

namespace App\Entity\Tenant\Firm\Store;

use App\Entity\Tenant\Firm\Customer\Customer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommentProduct extends Model
{
    use SoftDeletes;
    protected $table = 'comment_products';

    protected $fillable = [
        'product_id',
        'parent_id',
        'comment',
        'customer_id'
    ];

    public  function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public  function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id','id');
    }
}
