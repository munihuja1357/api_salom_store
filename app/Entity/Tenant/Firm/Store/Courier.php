<?php

namespace App\Entity\Tenant\Firm\Store;

use App\Entity\Tenant\Firm\Chat;
use App\Entity\Tenant\Firm\Message;
use App\Entity\Tenant\Firm\User;
use App\Entity\Tenant\Firm\Account;
use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    protected $table = 'couriers';

    protected $fillable = [
        'user_id',
        'total_orders',
        'is_free',
        'blocked_until',
        'lat',
        'lng',
    ];

    public const STATUS_FREE = 1;

    public const STATUS_BUSY = 0;

    public static function statusesList(): array
    {
        return [
            self::STATUS_FREE => 'Свободен',
            self::STATUS_BUSY => 'Занят',
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(User::class, 'courier_id', 'id');
    }

    public function accounts()
    {
        return $this->morphMany(Account::class, 'owner');
    }

    public function scopeFree($query)
    {
        return $query->where('is_free', self::STATUS_FREE);
    }

    public function scopeNotBlocked($query)
    {
        return $query->whereNull('blocked_until');
    }

    public function chat()
    {
        return $this->morphMany(Chat::class, 'chat_model');
    }

    public function message_to()
    {
        return $this->morphMany(Message::class, 'message_to_model');
    }

    public function message_from()
    {
        return $this->morphMany(Message::class, 'message_from_model');
    }


    public function classModel()
    {
        return Courier::class;
    }
}
