<?php

namespace App\Entity\Tenant\Firm\Store;

use App\Entity\Tenant\Firm\User;
use Illuminate\Database\Eloquent\Model;

class Overhead extends Model
{
    protected $table = 'overheads';

    protected $fillable = [
        'store_id',
        'discount_id',
        'status',
        'number',
        'created_by',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public const STATUS_ACTIVE = 'active';

    public const STATUS_IN_ACTIVE = 'in_active';

    public static function statusesList(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_IN_ACTIVE => 'Не активен',
        ];
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function priceList()
    {
        return $this->hasMany(PriceList::class);
    }

    public function scopeByStore($query)
    {
        return $query->where('store_id', store()->id);
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }
}
