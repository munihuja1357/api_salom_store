<?php

namespace App\Entity\Tenant\Firm\Store;

use App\Entity\Tenant\Firm\User;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Filters\Filterable;
use App\Utilities\GuardedModel;

class Product extends Model implements HasMedia
{
    use InteractsWithMedia, SoftDeletes, Filterable, GuardedModel;

    protected $table = 'products';

    protected $fillable = [
        'name',
        'barcode',
        'short_description',
        'description',
        'manufacture_id',
        'status',
        'created_by',
        'deleted_at',
    ];

    public const STATUS_SALE = 'sale';

    public const STATUS_HIDE = 'hide';

    public const STATUS_MODERATION = 'moderation';

    public static function statusesList(): array
    {
        return [
            self::STATUS_SALE => 'В продаже',
            self::STATUS_MODERATION => 'На модерации',
            self::STATUS_HIDE => 'Скрыто',
        ];
    }

    protected function authView(): array
    {
        return [];
    }

    protected function authUpdate(): array
    {
        return [];
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function isOnModeration(): bool
    {
        return $this->status === self::STATUS_MODERATION;
    }

    public function isOnSale(): bool
    {
        return $this->status === self::STATUS_SALE;
    }

    public function scopeSale($query)
    {
        return $query->where('status', self::STATUS_SALE);
    }

    public function scopeHide($query)
    {
        return $query->where('status', self::STATUS_HIDE);
    }

    public function manufacture()
    {
        return $this->belongsTo(Manufacture::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function priceList()
    {
        return $this->hasMany(PriceList::class);
    }
}
