<?php

namespace App\Entity\Tenant\Firm\Store;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use NodeTrait;

    protected $table = 'categories';

    protected $fillable = ['name', 'is_adult', '_lft', '_rgt', 'parent_id', 'status'];

    public const STATUS_ACTIVE = 'active';

    public const STATUS_CLOSED = 'closed';

    public const STATUS_DRAFT = 'draft';

    public static function statusesList(): array
    {
        return [
            self::STATUS_DRAFT => 'В корзине',
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_CLOSED => 'Закрытый',
        ];
    }

    public function stories()
    {
        return $this->belongsToMany(Store::class);
    }

    public function sections()
    {
        return $this->belongsToMany(Section::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeMain($query)
    {
        return $query->where('parent_id', 0);
    }
}
