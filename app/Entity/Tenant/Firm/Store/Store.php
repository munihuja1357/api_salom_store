<?php

namespace App\Entity\Tenant\Firm\Store;

use App\Entity\Tenant\Firm\Chat;
use App\Entity\Tenant\Firm\Message;
use App\Entity\Tenant\Firm\User;
use Spatie\MediaLibrary\HasMedia;
use App\Entity\Tenant\Firm\Account;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use App\Entity\Tenant\Firm\Store\Order\Order;
use Spatie\Activitylog\Traits\LogsActivity;

class Store extends Model implements HasMedia
{
    use InteractsWithMedia, LogsActivity;

    protected $table = 'stores';

    protected $fillable = [
        'name', 'address', 'lat', 'lng', 'phone', 'additional_phone', 'owner_id', 'responsible_user_id', 'status'
    ];

    protected static array $ignoreChangedAttributes = [
        'updated_at'
    ];

    protected static string $logName = 'store';

    protected static bool $logFillable = true;

    protected static bool $logOnlyDirty = true;

    public const STATUS_ACTIVE = 'active';

    public const STATUS_CLOSED = 'closed';

    public const STATUS_MODERATION = 'moderation';

    public static function statusesList(): array
    {
        return [
            self::STATUS_MODERATION => 'На модерации',
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_CLOSED => 'Закрытый',
        ];
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function section()
    {
        return $this->belongsToMany(Section::class);
    }

    public function overheads()
    {
        return $this->hasMany(Overhead::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'responsible_user_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function accounts()
    {
        return $this->morphMany(Account::class, 'owner');
    }
  
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
  
    public function chat()
    {
        return $this->morphMany(Chat::class, 'chat_model');
    }

    public function message_to()
    {
        return $this->morphMany(Message::class, 'message_to_model');
    }

    public function message_from()
    {
        return $this->morphMany(Message::class, 'message_from_model');
    }


    public function classModel()
    {
        return Store::class;
    }
}
