<?php

namespace App\Entity\Tenant\Firm\Store;

use Illuminate\Database\Eloquent\Model;
use App\Entity\Tenant\Firm\Store\Order\Order;

class DeliveryMethod extends Model
{
    protected $table = 'delivery_methods';

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }
}
