<?php

namespace App\Entity\Tenant\Firm\Store;

use Illuminate\Database\Eloquent\Model;
use App\Entity\Tenant\Firm\Store\Order\Order;

/**
 * Class PaymentMethod.
 *
 * @property int id
 * @property string name
 * @property bool is_active
 */
class PaymentMethod extends Model
{
    protected $table = 'payment_methods';

    public function orders()
    {
        return $this->hasMany(Order::class);
    }


    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }
}
