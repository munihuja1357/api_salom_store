<?php

namespace App\Entity\Tenant\Firm\Store;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;

class Manufacture extends Model implements HasMedia
{
    use InteractsWithMedia;

    /*use InteractsWithMedia {
        getFirstMediaUrl as protected getFirstMediaUrlTrait;
    }*/

    protected $table = 'manufactures';

    protected $fillable = ['name', 'brand_name', 'address', 'status', 'created_by'];

    public const STATUS_ACTIVE = 'active';

    public const STATUS_HIDE = 'hide';

    public static function statusesList(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_HIDE => 'Скрыть',
        ];
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
