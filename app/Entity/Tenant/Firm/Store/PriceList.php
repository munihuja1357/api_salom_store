<?php

namespace App\Entity\Tenant\Firm\Store;

use Illuminate\Database\Eloquent\Model;
use App\Entity\Tenant\Firm\User;

class PriceList extends Model
{
    protected $table = 'price_lists';

    protected $fillable = [
        'overhead_id',
        'product_id',
        'quantity',
        'old_price',
        'price',
        'discount_id',
        'created_by',
        'updated_by',
        'status',
    ];

    public const STATUS_IN_STOCK = 'in_stock';

    public const STATUS_OUT_STOCK = 'out_stock';

    public static function statusesList(): array
    {
        return [
            self::STATUS_IN_STOCK => 'Есть в наличии',
            self::STATUS_OUT_STOCK => 'Нет на складе',
        ];
    }

    public function stories()
    {
        return $this->belongsToMany(Store::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function scopeStock($query)
    {
        return $query->where('status', self::STATUS_IN_STOCK);
    }

    public function scopeSale($query)
    {
        return $query->whereHas('product', function ($q) {
            $q->sale();
        });
    }
    public function overhead()
    {
        return $this->belongsTo(Overhead::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function scopeByStore($query)
    {
        return $query->where('store_id', store()->id);
    }
}
