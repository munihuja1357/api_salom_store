<?php

namespace App\Entity\Tenant\Firm\Store\Order;

use Illuminate\Database\Eloquent\Model;
use App\Entity\Tenant\Firm\Store\PriceList;

/**
 * @property int id
 * @property Order order
 * @property PriceList priceList
 * @property string product_name
 * @property float price
 * @property int quantity
 * @property int total
 **/
class OrderItem extends Model
{
    protected $table = 'order_items';

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function priceList()
    {
        return $this->belongsTo(PriceList::class, 'price_list_id');
    }
}
