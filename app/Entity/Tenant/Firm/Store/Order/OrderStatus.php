<?php

namespace App\Entity\Tenant\Firm\Store\Order;

use Illuminate\Database\Eloquent\Model;
use App\Entity\Tenant\Firm\User;

/** @mixin OrderStatus
 * @property int id
 * @property string name
 * @property string color
 * @property bool unremovable
 * @property int sort_order
 * @property Order orders
 */

class OrderStatus extends Model
{
    protected $table = 'order_statuses';

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function scopeVisible($query)
    {
        $query->where('is_hidden', false);
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'status_id', 'id');
    }
}
