<?php

namespace App\Entity\Tenant\Firm\Store\Order;

use Illuminate\Database\Eloquent\Model;

class CancelReason extends Model
{
    protected $table = 'cancel_reasons';
}
