<?php

namespace App\Entity\Tenant\Firm\Store\Order;

use App\Entity\Tenant\Firm\Chat;
use App\Filters\Filterable;
use App\Entity\Tenant\Firm\User;
use App\Entity\Tenant\Firm\Address;
use App\Entity\Tenant\Firm\Store\Store;
use Illuminate\Database\Eloquent\Model;
use App\Entity\Tenant\Firm\Store\Courier;
use App\Entity\Tenant\Firm\TrafficSources;
use App\Entity\Tenant\Firm\Customer\Customer;
use App\Entity\Tenant\Firm\Store\PaymentMethod;
use App\Entity\Tenant\Firm\Store\DeliveryMethod;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Order extends Model implements HasMedia
{
    use Filterable,InteractsWithMedia;

    protected $table = 'orders';

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'custom_fields',
        'note',
        'amount',
        'discount_percent',
        'delivery_cost',
        'delivered_at',
        'store_id',
        'customer_id',
        'delivery_method_id',
        'delivery_address_id',
        'payment_method_id',
        'status_id',
        'responsible_user_id',
        'courier_id',
        'source_id',
        'cancelled_at',
        'cancel_reason_id',
    ];

    public const STATUS_UNPROCESSED = 1;
    public const STATUS_SHIPPED = 2;
    public const STATUS_DELIVERED = 3;
    public const STATUS_CANCELED = 4;


    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function deliveryMethod()
    {
        return $this->belongsTo(DeliveryMethod::class, 'delivery_method_id', 'id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'responsible_user_id', 'id');
    }

    public function source()
    {
        return $this->belongsTo(TrafficSources::class, 'sources_id', 'id');
    }

    public function cancelReason()
    {
        return $this->hasOne(CancelReason::class);
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'status_id', 'id');
    }

    public function deliveryAddress()
    {
        return $this->belongsTo(Address::class);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function courier()
    {
        return $this->belongsTo(Courier::class, 'courier_id', 'id');
    }

    public function scopeUncompleted($query)
    {
        return $query->whereNull('delivered_at');
    }

    public function scopeDelivered($query)
    {
        return $query->whereNotNull('delivered_at');
    }

    public function scopeByStore($query)
    {
        return $query->where('store_id', store()->id);
    }

    public function chat()
    {
        return $this->morphMany(Chat::class, 'chat_model');
    }

    public function assignCourier($courier)
    {
        if ($courier && $courier->id !== $this->courier_id) {
            $this->update([
                'courier_id' => $courier->id,
            ]);

            // TODO: Создать событие назначения курьера к заказу
            // event(new OrderAssignedToCourier($this, $courier));
        }
    }
}
