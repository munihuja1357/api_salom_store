<?php

namespace App\Entity\Tenant\Firm\Store;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = 'discounts';

    protected $fillable = [
        'name',
        'discount',
        'started_at',
        'ended_at',
        'is_status',
        'created_by',
        'store_id',
    ];


    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }
}
