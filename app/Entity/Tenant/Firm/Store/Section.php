<?php

namespace App\Entity\Tenant\Firm\Store;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\HasMedia;

class Section extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $table = 'sections';

    protected $fillable = ['name', 'status', 'parent_id', 'created_by'];

    public const STATUS_ACTIVE = 'active';

    public const STATUS_CLOSED = 'closed';

    public const STATUS_DRAFT = 'draft';

    public static function statusesList(): array
    {
        return [
            self::STATUS_DRAFT => 'В корзине',
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_CLOSED => 'Закрытый',
        ];
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function stores()
    {
        return $this->belongsToMany(Store::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }
}
