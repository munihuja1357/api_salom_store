<?php

namespace App\Entity\Tenant\Firm\Store;

use Illuminate\Database\Eloquent\Model;

class BusinessDay extends Model
{
    protected $table = 'business_days';

    protected $fillable = ['store_id', 'day_of_week', 'open', 'periods'];

    public const STATUS_OPEN = 'open';
    public const STATUS_CLOSED = 'closed';

    public static function statusesList(): array
    {
        return [
            self::STATUS_OPEN => 'Открыто',
            self::STATUS_CLOSED => 'Закрыто',
        ];
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
