<?php

namespace App\Entity\Tenant\Firm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chat extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'chat_model_id',
        'chat_model_type',
        'status'
    ];
    public function chat_model()
    {
        return $this->morphTo();
    }
    public function message()
    {
        return $this->hasMany(Message::class);
    }
}
