<?php

namespace App\Entity\Tenant;

use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

/**
 * Class Tenant.
 */
class Tenant extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const STATUS_WAIT = 'wait';

    const STATUS_DEACTIVATED = 'deactivated';

    const STATUS_ACTIVE = 'active';

    protected $table = 'tenants';

    protected $fillable = [
        'first_name', 'last_name', 'phone_number', 'email', 'password', 'verify_token', 'status',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function register(
        string $firstName,
        string $lastName,
        string $phoneNumber,
        string $email,
        string $password): self
    {
        return static::create([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'phone_number' => $phoneNumber,
            'email' => $email,
            'verify_token' => Str::uuid(),
            'password' => Hash::make($password),
            'status' => self::STATUS_WAIT,
        ]);
    }

    public function isWait(): bool
    {
        return $this->status === self::STATUS_WAIT;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function verify(): void
    {
        if (!$this->isWait()) {
            throw new \DomainException('Пользователь уже проверен.');
        }

        $this->update([
            'status' => self::STATUS_ACTIVE,
            'verify_token' => null,
        ]);
    }

    public function findForPassport($username)
    {
        return $this->where('email', $username)->first();
    }
}
