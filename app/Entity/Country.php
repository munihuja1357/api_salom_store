<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    protected $fillable = ['iso', 'name'];
}
