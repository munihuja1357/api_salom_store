<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false;
}
