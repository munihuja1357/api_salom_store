<?php

namespace App\Services\Sms;

use GuzzleHttp\Client;

class OsonSms implements SmsSender
{
    private $login;

    private $hash;

    private $url;

    private Client $client;

    public function __construct($login, $hash, $url = 'http://api.osonsms.com/sendsms_v1.php')
    {
        if (empty($login) || empty($hash)) {
            throw new \InvalidArgumentException('Задайте в настройках данные SMS-шлюза');
        }

        $this->login = $login;
        $this->hash = $hash;
        $this->url = $url;
        $this->client = new Client();
    }

    public function send($number, $text): void
    {
        $operationId = uniqid();
        $hash = hash('sha256', $operationId.';'.$this->login.';'.'OsonSMS'.';'.$number.';'.$this->hash);

        $this->client->get($this->url, [
            'query' => [
                'from' => 'OsonSMS',
                'phone_number' => trim($number, '+'),
                'msg' => $text,
                'login' => $this->login,
                'str_hash' => $hash,
                'txn_id' => $operationId,
            ],
        ])->getBody()->getContents();
    }
}
