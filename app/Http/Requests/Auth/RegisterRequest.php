<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:25',
            'last_name' => 'required|string|max:25',
            'phone' => 'required|string',
            'email' => 'required|email|unique:tenants,email',
            'password' => 'required|string|min:8|max:20|confirmed:password_confirmation',
            'password_confirmation' => 'required|string|min:8|max:20',
            'term' => 'accepted',
        ];
    }
}
