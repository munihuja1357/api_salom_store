<?php

namespace App\Http\Requests\Firm\Customers;

use Illuminate\Foundation\Http\FormRequest;

class CartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'store_id' => 'nullable|integer|exists:stores,id',
            'is_template' => 'nullable|boolean',
        ];
    }
}
