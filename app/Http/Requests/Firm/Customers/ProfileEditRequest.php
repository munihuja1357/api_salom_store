<?php

namespace App\Http\Requests\Firm\Customers;

use Illuminate\Foundation\Http\FormRequest;

class ProfileEditRequest extends FormRequest
{
    public function rules()
    {
        return [
            'first_name'=>'nullable|string|max:255',
            'last_name'=>'nullable|string|max:255',
            'email'=>'nullable|email',
            'note'=>'nullable|string',
            'password'=>'nullable|string|min:8',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
