<?php

namespace App\Http\Requests\Firm\Customers;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'chat_id' => 'nullable|integer|exists:chats,id',
            'text' => 'required|string',
            'message_to_model_type' => 'nullable|string|max:255',
            'message_to_model_type_id' => 'nullable|integer|min:1',
        ];
    }
}
