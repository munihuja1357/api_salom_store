<?php

namespace App\Http\Requests\Firm\Customers\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'phone' => 'required|string',
            'code' => 'required|integer|digits:5',
        ];
    }

    public function bodyParameters()
    {
        return [
            'phone' => [
                'description' => 'Номер телефона',
            ],
            'code' => [
                'description' => 'Код верификации',
            ],
        ];
    }
}
