<?php

namespace App\Http\Requests\Firm\Customers;

use Illuminate\Foundation\Http\FormRequest;

class OrderDeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products' => 'nullable|array',
            'store_id' => 'nullable|integer|exists:stores,id',
            'delivery_method_id' => 'required|integer|exists:delivery_methods,id',
            'payment_method_id' => 'required|integer|exists:payment_methods,id',
            'delivery_date' => 'required|date',
            'address_id' => 'nullable|integer|exists:addresses,id',
            'first_name_delivery' => 'nullable|string|max:60',
            'last_name_delivery' => 'nullable|string|max:60',
            'address' => 'nullable|string|max:100',
            'city_id' => 'nullable|integer|exists:cities,id',
            'phone_delivery' => 'nullable|string|max:60',
            'address_secondary'=> 'nullable|string|max:100',
            'additional_phone'=> 'nullable|string|max:60',
            'company'=> 'nullable|string|max:60',
            'alias'=> 'nullable|string|max:255',
            'lat'=> 'nullable|numeric',
            'lng'=> 'nullable|numeric',
            'note'=> 'nullable|string',
            ];
    }
}
