<?php

namespace App\Http\Requests\Firm\Customers;

use App\Entity\Tenant\Firm\Customer\CartItem;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CartItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cart_id' => 'nullable|integer|exists:carts,id',
            'quantity' => 'required|integer|min:1',
            'product_id' => 'required|integer|exists:products,id',
            'price_list_id' => 'required|integer|exists:price_lists,id',
        ];
    }
}
