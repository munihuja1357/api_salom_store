<?php

namespace App\Http\Requests\Firm\Customers;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryChangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required|integer|exists:orders,id',
            'city_id' => 'nullable|integer|exists:cities,id',
            'delivery_method_id' => 'nullable|integer|exists:delivery_methods,id',
            'first_name_delivery' => 'nullable|string|max:60',
            'last_name_delivery' => 'nullable|string|max:60',
            'address' => 'nullable|string|max:100',
            'delivery_date' => 'nullable|date',
            'phone_delivery' => 'nullable|string|max:60',
            'address_secondary'=> 'nullable|string|max:100',
            'additional_phone'=> 'nullable|string|max:60',
            'company'=> 'nullable|string|max:60',
            'alias'=> 'nullable|string|max:255',
            'lat'=> 'nullable|numeric',
            'lng'=> 'nullable|numeric',
            'note'=> 'nullable|string',
        ];
    }
}
