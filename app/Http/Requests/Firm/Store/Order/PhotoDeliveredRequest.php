<?php

namespace App\Http\Requests\Firm\Store\Order;

use Illuminate\Foundation\Http\FormRequest;

class PhotoDeliveredRequest extends FormRequest
{
    public function rules()
    {
        return [
            'delivered_photo' => 'required|image',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
