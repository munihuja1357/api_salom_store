<?php

namespace App\Http\Requests\Firm\Store\Order;

use Illuminate\Foundation\Http\FormRequest;

class PhotoCheckOrderRequest extends FormRequest
{
    public function rules()
    {
        return [
            'check_photo' => 'required|image',
            'order_photo' => 'required|image',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
