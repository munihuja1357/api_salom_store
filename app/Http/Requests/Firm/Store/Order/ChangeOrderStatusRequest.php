<?php

namespace App\Http\Requests\Firm\Store\Order;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use App\Entity\Tenant\Firm\Store\Order\Order;

class ChangeOrderStatusRequest extends FormRequest
{
    public function rules()
    {
        return [
            'state' => [
                'required',
                Rule::in([
                    Order::STATUS_UNPROCESSED,
                    Order::STATUS_SHIPPED,
                    Order::STATUS_DELIVERED,
                    Order::STATUS_CANCELED,
                ]),
            ],
        ];
    }

    public function authorize()
    {
        return true;
    }
}
