<?php

namespace App\Http\Requests\Firm\Store\Courier;

use Illuminate\Validation\Rule;
use App\Entity\Tenant\Firm\Store\Courier;
use Illuminate\Foundation\Http\FormRequest;

class ToggleStatusRequest extends FormRequest
{
    public function rules()
    {
        return [
            'state' => [
                'required',
                Rule::in([
                    Courier::STATUS_BUSY,
                    Courier::STATUS_FREE,
                ]),
            ],
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function bodyParameters()
    {
        return [
            'state' => [
                'description' => '1 - Занять, <br /> 0 - Свободен <br />',
                'example' => '1',
            ],
        ];
    }
}
