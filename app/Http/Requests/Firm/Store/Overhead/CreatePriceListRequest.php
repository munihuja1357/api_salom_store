<?php

namespace App\Http\Requests\Firm\Store\Overhead;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreatePriceListRequest extends FormRequest
{
    public function rules()
    {
        $overhead = $this->route()->parameter('overhead');

        return [
            'product_id' => [
                'required',
                'integer',
                'exists:products,id',
                Rule::unique('price_lists')->where(function ($query) use ($overhead) {
                    $query->where('overhead_id', $overhead);
                }),
            ],
            'quantity' => 'required|integer',
            'price' => 'required|numeric',
            'old_price' => 'nullable',
            'discount_id' => 'nullable|integer|exists:discounts,id',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
