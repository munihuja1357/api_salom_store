<?php

namespace App\Http\Requests\Firm\Store\Overhead;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'number' => [
                'nullable',
                'integer',
                'digits_between:6,10',
                Rule::unique('overheads')->where(function ($query) {
                    $query->where('store_id', store()->id);
                }),
            ],
            'discount_id' => 'nullable|integer|exists:discounts,id',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
