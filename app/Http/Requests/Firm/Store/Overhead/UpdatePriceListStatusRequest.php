<?php

namespace App\Http\Requests\Firm\Store\Overhead;

use Illuminate\Validation\Rule;
use App\Entity\Tenant\Firm\Store\PriceList;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePriceListStatusRequest extends FormRequest
{
    public function rules()
    {
        return [
            'price_lists.*' => ['required', 'exists:price_lists,id'],
            'status' => [
                'required',
                Rule::in([
                    PriceList::STATUS_IN_STOCK,
                    PriceList::STATUS_OUT_STOCK,
                ]),
            ],
        ];
    }

    public function authorize()
    {
        return true;
    }
}
