<?php

namespace App\Http\Requests\Firm\Store;

use Illuminate\Foundation\Http\FormRequest;

class SearchProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'store_id' =>'nullable|integer|min:0|exists:stores,id',
            'start_price' =>'nullable|numeric|min:0',
            'end_price' => 'nullable|numeric|min:0',
            'quantity' => 'nullable|integer|min:1',
            'product_name' => 'nullable|string',
            'product_brand_name' => 'nullable|string|exists:manufactures,brand_name',
            'product_category_id' => 'nullable|string|exists:categories,id',
            'orderAscPrice' => 'nullable|boolean',
        ];
    }
}
