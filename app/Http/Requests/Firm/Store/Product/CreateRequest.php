<?php

namespace App\Http\Requests\Firm\Store\Product;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string',
            'barcode' => 'required|integer',
            'short_description' => 'required|string|max:500',
            'description' => 'nullable|string',
            'manufacture_id' => 'nullable|integer|exists:manufactures,id',
            'tags.*' => 'nullable|integer|exists:tags,id',
            'categories.*' => 'nullable|integer|exists:categories,id',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
