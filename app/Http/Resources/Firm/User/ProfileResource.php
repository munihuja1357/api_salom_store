<?php

namespace App\Http\Resources\Firm\User;

use Illuminate\Http\Request;
use App\Entity\Tenant\Firm\User;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin User
 * @property string first_name
 * @property string last_name
 * @property string fullName
 * @property string middle_name
 * @property string phone_number
 * @property string email
 * @property mixed gender
 * @property string avatar_url
 */
class ProfileResource extends JsonResource
{

    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'full_name' => $this->fullName,
            'phone' => $this->phone,
            'email' => $this->email,
            'avatar_url' => $this->avatar_url,
            'gender' => $this->gender,
            'role' => $this->roles()->first(),
        ];
    }
}
