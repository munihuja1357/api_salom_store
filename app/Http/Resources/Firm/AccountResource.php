<?php

namespace App\Http\Resources\Firm;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Entity\Tenant\Firm\Account;
use Illuminate\Http\Request;

/** @mixin Account */
class AccountResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type_id' => $this->type_id,
            'balance' => number_format($this->balance),
            'date_opened' => $this->data_opened,
            'date_closed' => $this->date_closed,
            'is_blocked' => $this->is_blocked,
        ];
    }
}
