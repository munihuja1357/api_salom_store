<?php

namespace App\Http\Resources\Firm;

use Illuminate\Http\Request;
use App\Entity\Tenant\Firm\Address;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Address
 */
class AddressResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'address' => $this->address,
            'address_secondary' => $this->address_secondary,
            'phone' => $this->phone,
            'additional_phone' => $this->additional_phone,
            'company' => $this->company,
            'alias' => $this->alias,
            'custom_fields' => $this->custom_fields,
        ];
    }
}
