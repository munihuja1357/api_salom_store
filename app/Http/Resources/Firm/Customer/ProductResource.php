<?php

namespace App\Http\Resources\Firm\Customer;

use App\Entity\Tenant\Firm\Store\PriceList;
use App\Http\Resources\Firm\MediaResource;
use App\Http\Resources\Firm\Store\ManufactureResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'manufacture' => ManufactureResource::make($this->manufacture),
            'status' => $this->status,
            'barcode' => $this->barcode,
            'short_description' => $this->short_description,
            'description' => $this->description,
            'creator' => $this->creator->first_name.' '.$this->creator->last_name,
            'created_at' => $this->created_at,
            'images'=>MediaResource::collection($this->getMedia('images.product'))
        ];
    }
}
