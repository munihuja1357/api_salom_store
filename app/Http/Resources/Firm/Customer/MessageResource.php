<?php

namespace App\Http\Resources\Firm\Customer;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'chat_id' => $this->chat_id,
            'message_from' => $this->message_from_model->first_name.' '.$this->message_from_model->last_name,
            'message_from_model_id' => $this->message_from_model_id,
            'message_from_model_type' => $this->message_from_model_type,
            'text' => $this->text,
            'is_seen' => $this->is_seen,
            'sort' => $this->sort,
            'replied' => $this->replied->first_name.' '.$this->replied->last_name,
            'created_at' => $this->created_at,
            'chat' => ChatResource::make($this->whenLoaded('chat')),
        ];
    }
}
