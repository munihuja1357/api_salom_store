<?php

namespace App\Http\Resources\Firm\Customer;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'phone' => $this->phone,
            'full_name' => $this->full_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'note' => $this->note,
            'avatar_url' => $this->avatar_url
        ];
    }
}
