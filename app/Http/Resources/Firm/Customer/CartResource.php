<?php

namespace App\Http\Resources\Firm\Customer;

use App\Entity\Tenant\Firm\Store\Store;
use App\Http\Resources\Firm\Store\StoreResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'store_id' => $this->store_id,
            'is_template' => $this->is_template,
            'store' => StoreResource::make($this->whenLoaded('store')),
            'cart_items' => CartItemResource::collection($this->whenLoaded('cart_items')),
        ];
    }
}
