<?php

namespace App\Http\Resources\Firm\Customer;

use App\Entity\Tenant\Firm\Customer\Cart;
use App\Http\Resources\Firm\Store\Overhead\PriceListResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CartItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cart_id' => $this->cart_id,
            'product_id' => $this->product_id,
            'price_list_id' => $this->price_list_id,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'cart' => CartResource::make($this->whenLoaded('cart')),
            'price_list' => PriceListResource::collection($this->whenLoaded('priceList')),
        ];
    }
}
