<?php

namespace App\Http\Resources\Firm\Store;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Firm\User\ProfileResource;
use Illuminate\Http\Request;
use App\Entity\Tenant\Firm\Store\Discount;

/** @mixin Discount
 * @property mixed discount
 * @property mixed name
 * @property mixed start_at
 * @property mixed ended_at
 * @property mixed status
 * @property mixed store_id
 */
class DiscountResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'discount' => $this->discount,
            'start' => $this->start_at,
            'end' => $this->ended_at,
            'status' => $this->status,
            'creator' => ProfileResource::make($this->whenLoaded('creator')),
            'store_id' => $this->store_id,
        ];
    }
}
