<?php

namespace App\Http\Resources\Firm\Store;

use Illuminate\Http\Request;
use App\Entity\Tenant\Firm\Store\Section;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Section */
class SectionResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'icon' => $this->getFirstMediaUrl('images.section'),
            'stores' => StoreResource::collection($this->whenLoaded('stores')),
            'parent_id' => $this->parent_id,
        ];
    }
}
