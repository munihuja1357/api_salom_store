<?php

namespace App\Http\Resources\Firm\Store;

use App\Http\Resources\Firm\Store\Overhead\OverheadResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Entity\Tenant\Firm\Store\Store;
use Illuminate\Http\Request;
use App\Http\Resources\Firm\User\ProfileResource;
use App\Http\Resources\Firm\AccountResource;

/**
 * @group Магазин
 *
 * @responseField id ID
 * @responseField name Название
 * @responseField address Адрес
 * @responseField logo Логотип
 * @responseField lat Широта на карте
 * @responseField lng Долгота на карте
 */
class StoreResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'address' => $this->address,
            'phone' => $this->phone,
            'additional_phone' => $this->additional_phone,
            'owner' => new ProfileResource($this->whenLoaded('owner')),
            'manager' => new ProfileResource($this->whenLoaded('manager')),
            'accounts' => AccountResource::collection($this->whenLoaded('accounts')),
            'logo' => $this->getFirstMediaUrl('images.store'),
            'overhead'=>OverheadResource::collection($this->whenLoaded('overheads')),
            'lat' => $this->lat,
            'lng' => $this->lng,
        ];
    }
}
