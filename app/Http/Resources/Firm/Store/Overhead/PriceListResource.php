<?php

namespace App\Http\Resources\Firm\Store\Overhead;

use App\Http\Resources\Firm\Store\DiscountResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Firm\User\ProfileResource;
use Illuminate\Http\Request;
use App\Entity\Tenant\Firm\Store\PriceList;
use App\Http\Resources\Firm\Store\ProductResource;

/** @mixin PriceList
 * @property mixed status
 * @property mixed quantity
 * @property mixed product_id
 * @property mixed overhead_id
 * @property mixed price
 * @property mixed discount_id
 * @property mixed old_price
 * @property mixed id
 * @property mixed product
 */
class PriceListResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'overhead_id' => $this->overhead_id,
            'product' => ProductResource::make($this->whenLoaded('product')),
            'quantity' => $this->quantity,
            'price' => $this->price,
            'old_price' => $this->old_price,
            'discount' => DiscountResource::make($this->discount),
            'creator' => ProfileResource::make($this->whenLoaded('creator')),
            'status' => $this->status,
        ];
    }
}
