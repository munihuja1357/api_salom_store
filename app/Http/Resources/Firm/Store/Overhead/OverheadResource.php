<?php

namespace App\Http\Resources\Firm\Store\Overhead;

use Illuminate\Http\Request;
use App\Entity\Tenant\Firm\Store\Overhead;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Firm\User\ProfileResource;
use App\Http\Resources\Firm\Store\DiscountResource;

/** @mixin Overhead
 * @property mixed id
 * @property mixed store_id
 * @property mixed discount
 * @property mixed status
 * @property mixed number
 * @property mixed created_at
 * @property mixed creator
 * @property mixed updated_at
 */
class OverheadResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'store_id' => $this->store_id,
            'discount_id' => DiscountResource::make($this->discount),
            'status' => $this->status,
            'number' => $this->number,
            'creator' => ProfileResource::make($this->whenLoaded('creator')),
            'price_list' => PriceListResource::collection($this->whenLoaded('priceList')),
            'created' => $this->created_at->format('d.m.Y H:i'),
            'updated' => $this->updated_at->format('d.m.Y H:i'),
        ];
    }
}
