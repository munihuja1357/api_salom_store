<?php

namespace App\Http\Resources\Firm\Store;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Firm\AccountResource;
use App\Entity\Tenant\Firm\Store\Courier;
use Illuminate\Http\Request;

/** @mixin Courier */
class CourierResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->user->first_name,
            'last_name' => $this->user->last_name,
            'full_name' => $this->user->fullName,
            'avatar_url' => $this->user->avatar_url,
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'total_orders' => $this->total_orders,
            'is_free' => $this->is_free,
            'blocked_until' => $this->blocked_until,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'accounts' => AccountResource::collection($this->whenLoaded('accounts')),
        ];
    }
}
