<?php

namespace App\Http\Resources\Firm\Store;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'parent_id'=>$this->parent_id,
            'comment'=>$this->comment,
            'customer_name'=>$this->customer->first_name.' '.$this->customer->last_name,
        ];
    }
}
