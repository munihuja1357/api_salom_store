<?php

namespace App\Http\Resources\Firm\Store;

use Illuminate\Http\Request;
use App\Entity\Tenant\Firm\Store\Product;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Product
 * @property mixed status
 * @property mixed name
 */
class ProductResource extends JsonResource
{
    /**
     * @var mixed
     */

    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'manufacture' => ManufactureResource::make($this->manufacture),
            'status' => $this->status,
            'image' => $this->getFirstMediaUrl('images.product')
        ];
    }
}
