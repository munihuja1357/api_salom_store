<?php

namespace App\Http\Resources\Firm\Store;

use Illuminate\Http\Request;
use App\Entity\Tenant\Firm\Store\Manufacture;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Manufacture */
class ManufactureResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'brand_name' => $this->brand_name,
            'address' => $this->address,
            'logo' => $this->getFirstMediaUrl('images.manufacture'),
            'status' => $this->status,
        ];
    }
}
