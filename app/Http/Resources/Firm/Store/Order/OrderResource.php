<?php

namespace App\Http\Resources\Firm\Store\Order;

use Illuminate\Http\Request;
use App\Http\Resources\Firm\AddressResource;
use App\Http\Resources\Firm\Store\StoreResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Firm\User\ProfileResource;
use App\Http\Resources\Firm\Store\CourierResource;
use App\Http\Resources\Firm\Store\CustomerResource;
use App\Http\Resources\Firm\Store\PaymentMethodResource;

/**
 * Заказ.
 */
class OrderResource extends JsonResource
{
    /*public function responseFields()
    {
        return [
            'id' => [
                'description' => 'ID Заказа',
            ],
            'first_name' => [
                'description' => 'Имя клиента',
            ],
            'last_name' => [
                'description' => 'Фамилия клиента',
            ],
            'phone' => [
                'description' => 'Номер телефона',
            ],
            'note' => [
                'description' => 'Заметка по заказу',
            ],
            'order_no' => [
                'description' => 'Номер заказа',
            ],
            'amount' => [
                'description' => 'Сумма заказа',
            ],
            'discount_percent' => [
                'description' => 'Скидка в процентах',
            ],
            'delivery_method_name' => [
                'description' => 'Название доставки',
            ],
            'discount_cost' => [
                'description' => 'Сумма скидки',
            ],
            'total' => [
                'description' => 'Общая сумма заказа с доставкой',
            ],
            'delivery_date' => [
                'description' => 'Дата доставки заказа',
            ],
        ];
    }*/

    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone' => $this->phone,
            'note' => $this->note,
            'order_no' => $this->order_no,
            'amount' => number_format($this->amount, 2),
            'discount_percent' => $this->discount_percent,
            'delivery_method_name' => $this->deliveryMethod->name ?? '',
            'discount_cost' => number_format($this->discount_cost),
            'total' => number_format($this->amount + $this->delivery_cost, 2),
            'delivery_date' => $this->delivery_date,
            'store' => new StoreResource($this->whenLoaded('store')),
            'customer' => new CustomerResource($this->whenLoaded('customer')),
            'courier' => new CourierResource($this->whenLoaded('courier')),
            'delivery_address' => new AddressResource($this->deliveryAddress),
            'payment_method' => new PaymentMethodResource($this->paymentMethod),
            'status' => new OrderStatusResource($this->status),
            'items' => OrderItemResource::collection($this->whenLoaded('items')),
            'manager' => new ProfileResource($this->whenLoaded('manager')),
            'cancel_reason_id' => $this->cancel_reason_id,
            'created' => $this->created_at->format('H:i'),
            'updated' => $this->updated_at->format('H:i'),
            'check_photo'=>$this->getFirstMediaUrl('check_photo'),
            'order_photo'=>$this->getFirstMediaUrl('order_photo'),
        ];
    }
}
