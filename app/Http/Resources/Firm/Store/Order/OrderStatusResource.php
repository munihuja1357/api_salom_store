<?php

namespace App\Http\Resources\Firm\Store\Order;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Entity\Tenant\Firm\Store\Order\OrderStatus;

/** @mixin OrderStatus
 */

class OrderStatusResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'color' => $this->color,
            'unremovable' => $this->unremovable,
            'sort_order' => $this->sort_order,
            'orders' => OrderShortResource::collection($this->whenLoaded('orders')),
        ];
    }
}
