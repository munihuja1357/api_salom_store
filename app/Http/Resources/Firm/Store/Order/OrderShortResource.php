<?php

namespace App\Http\Resources\Firm\Store\Order;

use App\Entity\Tenant\Firm\Store\Order\Order;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Order */
class OrderShortResource extends JsonResource
{
    /**     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_no' => $this->order_no,
            'amount' => number_format($this->amount, 2),
            'store' => collect([
                'name' => $this->store->name,
            ]),
            'created' => $this->created_at->format('H:i'),
        ];
    }
}
