<?php

namespace App\Http\Resources\Firm\Store\Order;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Firm\Store\Overhead\PriceListResource;
use Illuminate\Http\Request;
use App\Entity\Tenant\Firm\Store\Order\OrderItem;

/** @mixin OrderItem */
class OrderItemResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_name' => $this->product_name,
            'price' => number_format($this->price, 2),
            'quantity' => $this->quantity,
            'total' => number_format($this->total, 2),
            'order' => new OrderResource($this->whenLoaded('order')),
            'price_list' => new PriceListResource($this->whenLoaded('priceList')),
        ];
    }
}
