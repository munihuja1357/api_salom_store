<?php

namespace App\Http\Resources\Firm\Store;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Entity\Tenant\Firm\Store\Category;

/** @mixin Category */
class CategoryResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'is_adult' => $this->is_adult,
            'parent_id' => $this->parent_id,
            'products' => ProductResource::collection($this->whenLoaded('products')),
            'status' => $this->status,
        ];
    }
}
