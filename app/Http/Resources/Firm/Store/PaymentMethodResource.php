<?php

namespace App\Http\Resources\Firm\Store;

use Illuminate\Http\Request;
use App\Entity\Tenant\Firm\Store\PaymentMethod;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin PaymentMethod */
class PaymentMethodResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
