<?php

namespace App\Http\Controllers\Firm;

use App\Http\Controllers\Controller;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index()
    {
        return Inertia::render('firm/dashboard');
    }
}
