<?php

namespace App\Http\Controllers\Firm\Account;

use App\Http\Controllers\Controller;
use Inertia\Inertia;

class ProfileController extends Controller
{
    public function edit()
    {
        return Inertia::render('firm/account/profile/edit');
    }
}
