<?php

namespace App\Http\Controllers\Firm\Store;

use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Filters\Store\ProductFilter;
use Illuminate\Support\Facades\Auth;
use App\Entity\Tenant\Firm\Store\Tag;
use App\Entity\Tenant\Firm\Store\Product;
use App\Entity\Tenant\Firm\Store\Category;
use App\Entity\Tenant\Firm\Store\Overhead;
use App\Entity\Tenant\Firm\Store\PriceList;
use App\Entity\Tenant\Firm\Store\Manufacture;
use App\UseCases\Firm\Store\PriceListService;
use App\Http\Controllers\Firm\ModelController;
use App\Http\Resources\Firm\Store\ProductResource;
use App\Http\Requests\Firm\Store\Product\CreateRequest;
use App\Http\Resources\Firm\Store\Overhead\OverheadResource;
use App\Http\Resources\Firm\Store\Overhead\PriceListResource;
use App\Http\Requests\Firm\Store\Overhead\UpdatePriceListStatusRequest;
use App\Http\Requests\Firm\Store\Overhead\CreatePriceListRequest;

class PriceListController extends ModelController
{
    private PriceListService $service;

    /** @var array 'method' => 'policy' */
    protected array $guardedMethods = [
        'export' => 'export',
    ];

    protected function getModelClass(): string
    {
        return PriceList::class;
    }

    public function __construct(PriceListService $service)
    {
        return $this->service = $service;
    }

    public function index()
    {
        $overhead = Overhead::byStore()->active()->latest()->first();

        $priceList = PriceListResource::collection(
            PriceList::where('overhead_id', $overhead->id)
                ->with('product')
                ->paginate(10)
        );

        return Inertia::render('firm/store/price-list/list', [
            'overhead' => OverheadResource::make($overhead),
            'priceList' => $priceList,
            'statuses' => PriceList::statusesList(),
        ]);
    }

    public function create()
    {
        $categories = Category::active()->get()->toTree();
        $tags = Tag::all();
        $manufactures = Manufacture::active()->get();

        return Inertia::render('firm/store/product/create', [
            'categories' => $categories,
            'tags' => $tags,
            'manufactures' => $manufactures,
        ]);
    }

    public function store(Overhead $overhead, CreatePriceListRequest $request)
    {
        try {
            $this->service->create(
                $overhead->id,
                $request
            );
        } catch (\DomainException $e) {
            return back()->with('errors', $e->getMessage());
        }

        return back()->with('message', 'Наименование добавлена в накладную.');
    }

    // TODO: Вынести удаление наименования в сервис.
    public function destroy(Request $request)
    {
        try {
            PriceList::destroy(collect($request));
        } catch (\DomainException $e) {
            return back()->with('errors', $e->getMessage());
        }

        return back()->with('Выбранные наименование удалены.');
    }

    public function show($id)
    {
        //
    }

    public function updateStatuses(UpdatePriceListStatusRequest $request)
    {
        try {
            $this->service->updateStatuses($request);
        } catch (\DomainException $e) {
            return back()->with('errors', $e->getMessage());
        }

        return back()->with('message', 'Статусы обновлены');
    }
}
