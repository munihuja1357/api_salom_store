<?php

namespace App\Http\Controllers\Firm\Store\Courier;

use App\Http\Controllers\Controller;
use App\Http\Resources\Firm\Store\CourierResource;
use App\Entity\Tenant\Firm\Store\Courier;
use App\Http\Resources\Firm\Store\Order\OrderResource;
use App\Entity\Tenant\Firm\Store\Order\Order;

class CourierController extends Controller
{
    public function index()
    {
        //
    }

    public function getCourierById($id)
    {
        return CourierResource::make(
            Courier::find($id)->with('accounts', 'user')->first()
        );
    }

    public function getAllCouriers()
    {
        return CourierResource::collection(
          Courier::free()->get()
        );
    }

    public function getCourierUncompletedOrders($id)
    {
        return OrderResource::collection(
            Order::where('courier_id', $id)->uncompleted()->get()
        );
    }
}
