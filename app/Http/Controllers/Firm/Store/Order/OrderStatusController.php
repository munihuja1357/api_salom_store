<?php

namespace App\Http\Controllers\Firm\Store\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Entity\Tenant\Firm\Store\Order\OrderStatus;

class OrderStatusController extends Controller
{
    public function update(Request $request)
    {
        $orderStatus = OrderStatus::findOrFail($request->status_id);

        collect($request->input('orders'))->each(function ($order, $index) use ($orderStatus) {
            $order = Order::findOrFail($order);

            $order->update([
                'status_id' => $orderStatus->id,
            ]);
        });

        $orderStatus->refresh();

        return back();
    }
}
