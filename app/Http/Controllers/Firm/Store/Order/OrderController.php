<?php

namespace App\Http\Controllers\Firm\Store\Order;

use Inertia\Inertia;
use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Store\Courier;
use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Http\Resources\Firm\Store\Order\OrderResource;

class OrderController extends Controller
{
    public function index()
    {
        $orders = collect([]);

        $orders->put('active', OrderResource::collection(
            Order::with('courier')->uncompleted()->byStore()->get()
        ));
        $orders->put('delivered', OrderResource::collection(
            Order::with('courier')->delivered()->byStore()->get()
        ));

        return Inertia::render('firm/store/order/list', [
          'orders' => $orders,
        ]);
    }

    /*public function assignCourierToOrder($id, $courierId)
    {
        $order = Order::uncompleted()->findOrFail($id);
        $courier = Courier::notBlocked()->findOrFail($courierId);

        try {
            $order->assignCourier($courier);
        } catch (\DomainException $e) {
            return back()->with('errors', $e->getMessage());
        }

        return back()->with(['message' => 'Курьер назначен на заказ #'.$order->order_no]);
    }*/
}
