<?php

namespace App\Http\Controllers\Firm\Store\Overhead;

use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Entity\Tenant\Firm\Store\Discount;
use App\Entity\Tenant\Firm\Store\Overhead;
use App\Entity\Tenant\Firm\Store\PriceList;
use App\UseCases\Firm\Store\OverheadService;
use App\Http\Resources\Firm\Store\DiscountResource;
use App\Http\Requests\Firm\Store\Overhead\CreateRequest;
use App\Http\Resources\Firm\Store\Overhead\OverheadResource;
use App\Http\Resources\Firm\Store\Overhead\PriceListResource;

class OverheadController extends Controller
{
    private $service;

    public function __construct(OverheadService $service)
    {
        return $this->service = $service;
    }

    public function index()
    {
        $overheads = OverheadResource::collection(
            Overhead::where('store_id', store()->id)->orderBy('id', 'desc')->paginate(5)
        );

        $discounts = DiscountResource::collection(
            Discount::active()->get()
        );

        return Inertia::render('firm/store/overhead/list', [
            'overheads' => $overheads,
            'discounts' => $discounts,
        ]);
    }

    public function show($id)
    {
        $overhead = Overhead::with('priceList')->findOrFail($id);

        $priceList = PriceListResource::collection(
            PriceList::where('overhead_id', $overhead->id)
                ->with('product')
                ->paginate(10)
        );

        return Inertia::render('firm/store/price-list/list', [
            'overhead' => OverheadResource::make($overhead),
            'priceList' => $priceList,
            'statuses' => PriceList::statusesList(),
        ]);
    }

    public function create()
    {
        return Inertia::render('firm/store/overhead/create');
    }

    public function store(CreateRequest $request)
    {
        try {
            $overhead = $this->service->create(
                Auth::id(),
                $request
            );
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('store:overheads.show', $overhead->id);
    }

    public function destroy(Request $request)
    {
        try {
            Overhead::destroy(collect($request));
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return back()->with(['message' => 'Записи удалены']);
    }
}
