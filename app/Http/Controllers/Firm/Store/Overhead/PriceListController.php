<?php

namespace App\Http\Controllers\Firm\Store\Overhead;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Store\Overhead;
use App\Entity\Tenant\Firm\Store\PriceList;
use App\UseCases\Firm\Store\PriceListService;
use App\Http\Requests\Firm\Store\Overhead\CreatePriceListRequest;
use App\Http\Requests\Firm\Store\Overhead\UpdatePriceListStatusRequest;

class PriceListController extends Controller
{
    protected $service;

    public function __construct(PriceListService $service)
    {
        $this->service = $service;
    }

    public function store(Overhead $overhead, CreatePriceListRequest $request)
    {
        try {
            $this->service->create(
                $overhead->id,
                $request
            );
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return back()->with(['message' => 'Запись добавлена в накладную']);
    }

    public function updateStatuses(UpdatePriceListStatusRequest $request)
    {
        try {
            $this->service->updateStatuses($request);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return back()->with('message', 'Статусы обновлены');
    }

    public function destroy(Request $request)
    {
        try {
            PriceList::destroy(collect($request));
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return back()->with(['message' => 'Записи удалены']);
    }
}
