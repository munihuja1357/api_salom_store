<?php

namespace App\Http\Controllers\Firm\Settings;

use App\Entity\Tenant\Firm\User;
use App\Http\Controllers\Firm\ModelController;
use App\Http\Resources\Firm\User\ProfileResource;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class UserController extends ModelController
{
    public function getModelClass(): string
    {
        return User::class;
    }

    public function index()
    {
        return Inertia::render('firm/settings/users/list', [
            'roles' => Role::select('id', 'name')->get(),
            'users' => ProfileResource::collection(
                User::orderBy('first_name')->paginate(10)
            ),
        ]);
    }

    public function store(Request $request)
    {
        $role = Role::findOrFail($request->input('role'));

        $user = User::make([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'gender' => $request->input('gender'),
            'password' => bcrypt($request->input('password')),
        ]);

        $user->assignRole($role);

        session()->flash('message', 'Пользователь добавлен в систему');

        return back();
    }

    public function update(Request $request)
    {
        $user = User::findOrFail($request->id);

        if ($request->input('password')) {
            $user->update([
                'password' => bcrypt($request->input('password')),
            ]);
        }

        $user->update([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'gender' => $request->input('gender'),
        ]);

        if ($request->input('role')) {
            $user->syncRoles($request->input('role'));
        }

        session()->flash('message', 'Данные пользователя изменены.');

        return back();
    }
}
