<?php

namespace App\Http\Controllers\Firm\Manager;

use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Filters\Store\ProductFilter;
use Illuminate\Support\Facades\Auth;
use App\Entity\Tenant\Firm\Store\Tag;
use App\Entity\Tenant\Firm\Store\Product;
use App\Entity\Tenant\Firm\Store\Category;
use App\UseCases\Firm\Store\ProductService;
use App\Entity\Tenant\Firm\Store\Manufacture;
use App\Http\Controllers\Firm\ModelController;
use App\Http\Resources\Firm\Store\ProductResource;
use App\Http\Requests\Firm\Store\Product\CreateRequest;
use App\Entity\Tenant\Firm\Store\Overhead;
use App\Entity\Tenant\Firm\Store\PriceList;
use App\Http\Resources\Firm\Store\Overhead\PriceListResource;

class ProductController extends ModelController
{
    private ProductService $service;

    /** @var array 'method' => 'policy' */
    protected array $guardedMethods = [
        'export' => 'export',
    ];

    protected function getModelClass(): string
    {
        return Product::class;
    }

    public function __construct(ProductService $service)
    {
        return $this->service = $service;
    }

    public function index()
    {
        $products = ProductResource::collection(
            Product::orderBy('created_by', 'asc')->with('manufacture')->paginate(10)
        );

        return Inertia::render('firm/store/product/list', [
            'products' => $products,
        ]);
    }

    public function create()
    {
        $categories = Category::active()->get()->toTree();
        $tags = Tag::all();
        $manufactures = Manufacture::active()->get();

        return Inertia::render('firm/store/product/create', [
            'categories' => $categories,
            'tags' => $tags,
            'manufactures' => $manufactures,
        ]);
    }

    public function store(CreateRequest $request)
    {
        try {
            $this->service->create(
                Auth::id(),
                $request
            );
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('products.index');
    }

    public function destroy(Request $request)
    {
        try {
            Product::destroy(collect($request));
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('store:products.index');
    }

    public function show($id)
    {
        //
    }

    public function filter(ProductFilter $filter)
    {
        $products = Product::select(['id', 'name'])->filter($filter)->get();

        return ProductResource::collection($products);
    }

    public function search(SearchProductRequest $request)
    {
        $store_id = $request->input('store_id');
        $start_price = $request->input('start_price');
        $end_price = $request->input('end_price');
        $quantity = $request->input('quantity');
        $product_name = $request->input('product_name');
        $product_brand_name = $request->input('product_brand_name');
        $product_category_id = $request->input('product_category_id');
        $orderAsc = $request->input('orderAscPrice');

        $overhead = Overhead::where('store_id', $store_id)->active()->with(['priceList' => function ($priceList) use ($request) {
            $priceList->active()->filter($request);
        }]);

        $data = PriceList::stock()->with('product')
            ->whereHas('overhead', function ($q) use ($store_id) {
                $q->active();
                if (!empty($store_id)) $q->where('store_id', $store_id);
            })->whereHas('product', function ($q) use ($product_name, $product_category_id) {
                $q->sale()->whereHas('categories', function ($q2) use ($product_category_id) {
                    $q2->active();
                    if (!empty($product_category_id)) $q2->where('category_id', $product_category_id);
                });
                if (!empty($product_name)) $q->where('name', 'like', '%' . $product_name . '%');
            });
        if (!empty($product_brand_name)) {
            $data->whereHas('product.manufacture', function ($q) use ($product_brand_name) {
                $q->where('brand_name', $product_brand_name);
            });
        }
        if (!empty($start_price)) $data->where('price', '>', $start_price);
        if (!empty($end_price)) $data->where('price', '<=', $end_price);
        if (!empty($quantity)) $data->where('quantity', '>=', $quantity);
        if (!empty($orderAsc) && $orderAsc) $data->orderBy('price', 'DESC');
        else  $data->orderBy('price');
        $data = $data->get();
        return PriceListResource::collection($data);
    }
}
