<?php

namespace App\Http\Controllers\Firm\Manager\Order;

use Inertia\Inertia;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Entity\Tenant\Firm\Store\Courier;
use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Entity\Tenant\Firm\Store\Order\OrderStatus;
use App\Http\Resources\Firm\Store\Order\OrderStatusResource;

class OrderController extends Controller
{
    public function index()
    {
        $orderStatuses = OrderStatusResource::collection(
            OrderStatus::visible()
                ->with(['orders' => function ($query) {
                    $query->orderBy('created_at', 'desc');
                }, 'orders.store'])
                ->orderBy('sort_order')
                ->get()
        );

        return Inertia::render('firm/manager/order/list', [
            'orderStatuses' => $orderStatuses,
        ]);
    }

    public function assignCourierToOrder($id, $courierId)
    {
        $order = Order::uncompleted()->findOrFail($id);
        $courier = Courier::notBlocked()->findOrFail($courierId);

        try {
            $order->assignCourier($courier);
        } catch (\DomainException $e) {
            return back()->with('errors', $e->getMessage());
        }

        return back()->with(['message' => 'Курьер назначен на заказ #'.$order->order_no]);
    }
}
