<?php

namespace App\Http\Controllers\Firm\Manager\Store;

use App\Http\Controllers\Controller;
use Inertia\Inertia;
use App\Entity\Tenant\Firm\Store\Store;
use App\Http\Resources\Firm\Store\StoreResource;
use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Http\Resources\Firm\Store\Order\OrderResource;
use Illuminate\Http\Request;
use Auth;

class StoreController extends Controller
{
    public function index()
    {
        $stores = Store::paginate(20);

        return Inertia::render('firm/manager/store/list', [
            'stores' => StoreResource::collection($stores),
        ]);
    }

    public function show($id)
    {
        $store = Store::with('manager', 'owner', 'accounts')->findOrFail($id);
        $orders = Order::where('store_id', $store->id)->get();

        return Inertia::render('firm/manager/store/show', [
            'store' => new StoreResource($store),
            'orders' => fn() => OrderResource::collection($orders),
        ]);
    }

    public function create()
    {
        return Inertia::render('firm/manager/store/create');
    }

    public function store(Request $request)
    {
        try {
            $store = new Store();
            $store->name = $request['name'];
            $store->address = $request['address'];
            $store->phone = $request['phone'];
            $store->additional_phone = $request['additional_phone'];
            $store->lat = $request['lat'] ?? '';
            $store->lng = $request['lng'] ?? '';
            $store->owner_id = 2;
            $store->responsible_user_id = Auth::user()->id;
            $store->status = $request['status'];

            $store->saveOrFail();

            $store->addMedia($request->file('logo'))->toMediaCollection('images.store');
        } catch (\DomainException $e) {
            return back()->with([
                'error' => $e->getMessage()
            ]);
        }

        return back()->with([
            'message' => 'Магазин успешно создан.'
        ]);
    }

    public function getStats($id)
    {
        //
    }
}
