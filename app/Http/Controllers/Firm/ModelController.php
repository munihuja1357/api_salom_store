<?php

namespace App\Http\Controllers\Firm;

use App\Http\Controllers\Controller;

abstract class ModelController extends Controller
{
    /** @var array 'method' => 'policy' */
    protected array $guardedMethods = [];

    protected array $methodsWithoutModels = [];

    abstract protected function getModelClass(): string;

    public function __construct()
    {
        $this->authorizeResource($this->getModelClass());
    }

    protected function resourceAbilityMap()
    {
        $base = parent::resourceAbilityMap();

        return array_merge($base, $this->guardedMethods);
    }

    protected function resourceMethodsWithoutModels()
    {
        $base = parent::resourceMethodsWithoutModels();

        return array_merge($base, $this->methodsWithoutModels);
    }
}
