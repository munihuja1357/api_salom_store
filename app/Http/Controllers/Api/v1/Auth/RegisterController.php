<?php

namespace App\Http\Controllers\Api\v1\Auth;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Entity\Tenant\Tenant;
use App\UseCases\Auth\RegisterService;

class RegisterController extends Controller
{
    private $service;

    public function __construct(RegisterService $service)
    {
        $this->middleware('guest');
        $this->service = $service;
    }

    // RegisterRequest redirects to / when validation fails
    public function register(RegisterRequest $request)
    {
        $this->service->register($request);

        return response()->json([
            'message' => 'На вашу почту отправлено письмо с ссылкой для активации. Нажмите на неё для активации аккаунта.',
        ], Response::HTTP_CREATED);
    }

    public function verify($token)
    {
        if (!$user = Tenant::where('verify_token', $token)->first()) {
            return response()->json(['message' => 'К сожалению, ваша ссылка не может быть идентифицирована.'], 401);
        }

        try {
            $this->service->verify($user->id);

            return response()->json(['message' => 'Ваш адрес электронной почты подтвержден. Теперь вы можете войти в систему.'], 201);
        } catch (\DomainException $e) {
            return response()->json(['message' => $e->getMessage()], 401);
        }
    }
}
