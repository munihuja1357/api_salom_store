<?php

namespace App\Http\Controllers\Api\v1\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Utilities\ProxyRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use App\Entity\Tenant\Tenant;

/**
 * Class LoginController.
 */
class LoginController extends Controller
{
    protected ProxyRequest $proxy;

    public function __construct(ProxyRequest $proxy)
    {
        $this->proxy = $proxy;
    }

    /**
     * @param LoginRequest $request
     *
     * @return Application|ResponseFactory|Response
     */
    public function login(LoginRequest $request)
    {
        $tenant = Tenant::where('email', $request['email'])->first();

        abort_unless($tenant, 404, 'This combination does not exists 1.');
        abort_unless(
            \Hash::check(request('password'), $tenant->password),
            403,
            'This combination does not exists 2.'
        );

        $response = $this->proxy
            ->grantPasswordToken([
                'grant_type' => 'password',
                'client_id' => $request['client_id'],
                'client_secret' => $request['client_secret'],
                'username' => $request['email'],
                'password' => $request['password'],
                'scope' => '*',
            ]);

        return response([
            'token' => $response->access_token,
            'expiresIn' => $response->expires_in,
            'message' => 'Вы вошли в систему',
        ], 200);
    }

    public function refreshToken()
    {
        $resp = $this->proxy->refreshAccessToken();

        return response([
            'token' => $resp->access_token,
            'expiresIn' => $resp->expires_in,
            'message' => 'Token has been refreshed.',
        ], 200);
    }
}
