<?php

namespace App\Http\Controllers\Api\v1\Firm;

use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\User;
use App\Http\Resources\Firm\User\ProfileResource;

class UserController extends Controller
{
    public function index()
    {
        $users = User::limit(20)->get();

        return ProfileResource::collection($users);
    }
}
