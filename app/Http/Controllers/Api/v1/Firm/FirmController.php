<?php

namespace App\Http\Controllers\Api\v1\Firm;

use App\Http\Controllers\Controller;
use App\Http\Requests\Firm\CreateRequest;
use Illuminate\Http\Response;
use App\UseCases\Firm\FirmService;

class FirmController extends Controller
{
    private $firm;

    public function __construct(FirmService $service)
    {
        $this->firm = $service;
    }

    public function store(CreateRequest $request)
    {
        $this->firm->create(
            $request->user()->id,
            $request['name'],
            $request['domain']
        );

        return response()->json([
            'message' => 'Фирма создана',
        ], Response::HTTP_CREATED);
    }
}
