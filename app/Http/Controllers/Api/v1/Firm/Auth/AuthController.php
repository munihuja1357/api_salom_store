<?php

namespace App\Http\Controllers\Api\v1\Firm\Auth;

use Illuminate\Http\Response;
use App\Utilities\ProxyRequest;
use App\Entity\Tenant\Firm\User;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;

/**
 * @group Аутентификация
 *
 * API для аутентификации в системе
 */
class AuthController extends Controller
{
    protected ProxyRequest $proxy;

    public function __construct(ProxyRequest $proxy)
    {
        $this->proxy = $proxy;
    }

    /**
     * Вход в систему.
     *
     * @param LoginRequest $request
     *
     * <aside class="notice">refresh_token будет записан в куки.</aside>
     *
     * @responseField access_token string Токен доступа
     * @responseField expiresIn int Срок действия access_token
     *
     * @response 200 {
     *  "access_token": "MTQ0NjJkZmQ5OTM2NDE1ZTZjNGZmZjI3",
     *  "expires_in": 3600,
     *  return "message"
     * }
     *
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request['email'])->first();

        abort_unless($user, 404, 'Пользователь с таким email-ом не найден');
        abort_unless(
            \Hash::check(request('password'), $user->password),
            403,
            'Неправильный E-mail или пароль.'
        );

        $response = $this->proxy
            ->grantPasswordToken([
                'grant_type' => 'password',
                'client_id' => $request['client_id'],
                'client_secret' => $request['client_secret'],
                'username' => $request['email'],
                'password' => $request['password'],
                'scope' => '*',
            ]);

        return response()->json([
            'access_token' => $response->access_token,
            'expires_in' => $response->expires_in,
            'message' => 'Вы вошли в систему',
        ], 200);
    }

    /**
     * Обновить Access Token.
     *
     * @header client_id 1
     * @header client_secret secret
     */
    public function refreshToken()
    {
        $resp = $this->proxy->refreshAccessToken();

        return response()->json([
            'token' => $resp->access_token,
            'expiresIn' => $resp->expires_in,
            'message' => 'Токен был обновлен.',
        ], 200);
    }

    /**
     * Выход из системы.
     */
    public function logout()
    {
        $token = request()->user()->token();
        $token->delete();

        // remove the httponly cookie
        cookie()->queue(cookie()->forget('refresh_token'));

        return response()->json([
            'message' => 'Вы успешно вышли из системы',
        ], 200);
    }
}
