<?php

namespace App\Http\Controllers\Api\v1\Firm\Store;

use App\Http\Controllers\Controller;
use App\Filters\Store\ProductFilter;
use App\Entity\Tenant\Firm\Store\Product;
use App\Http\Resources\Firm\Store\ProductResource;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    // TODO: Создать отдельный ресурс для отправки коллекции модели.
    public function index(ProductFilter $filter)
    {
        return ProductResource::collection(
            Product::select(['id', 'name'])->filter($filter)->get()
        );
    }

    public function byCategoryId($id)
    {
        $idProduct=DB::table('category_product')->where('category_id',$id)->pluck('product_id')->toArray();
        return ProductResource::collection(
            Product::sale()->whereIn('id',$idProduct)->get()
        );
    }
}
