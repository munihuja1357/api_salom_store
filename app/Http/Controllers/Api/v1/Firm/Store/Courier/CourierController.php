<?php

namespace App\Http\Controllers\Api\v1\Firm\Store\Courier;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Store\Courier;
use App\Http\Resources\Firm\AccountResource;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use App\Http\Resources\Firm\Store\CourierResource;
use App\Http\Requests\Firm\Store\Courier\ToggleStatusRequest;
use App\Http\Resources\Firm\Store\Order\OrderResource;
use App\Entity\Tenant\Firm\Store\Order\Order;

/**
 * @group Курьер
 */
class CourierController extends Controller
{
    public function index()
    {
        return CourierResource::collection(
            Courier::free()->notBlocked()->limit(5)->get()
        );
    }

    public function show($id)
    {
        $courier = Courier::with('accounts')->findOrfail($id);

        return new CourierResource($courier);
    }

    /**
     * Информация о курьере.
     *
     * @apiResource App\Http\Resources\Firm\Store\CourierResource
     * @apiResourceModel App\Entity\Tenant\Firm\Store\Courier
     */
    public function getProfile()
    {
        return CourierResource::make(
            Courier::find(courier()->id)->with('accounts', 'user')->first()
        );
    }

    /**
     * Сменить статус курьера.
     *
     * @param ToggleStatusRequest $status
     *
     * @response {
     *  "message": "Ваш статус изменён."
     * }
     * @return Application|ResponseFactory|Response
     */
    public function toggleStatus(ToggleStatusRequest $status)
    {
        courier()->query()->update(['is_free' => $status->state]);

        return response([
            'message' => 'Ваш статус изменён.',
        ], 202);
    }

    /**
     * @group Счета
     *
     * Получить список счетов
     *
     * @apiResourceCollection App\Http\Resources\Firm\AccountResource
     * @apiResourceModel App\Entity\Tenant\Firm\Account
     */
    public function getAccounts()
    {
        return AccountResource::collection(
            courier()->accounts
        );
    }

    public function getUncompletedOrders($id)
    {
        return OrderResource::collection(
            Order::where('courier_id', $id)->uncompleted()->get()
        );
    }
}
