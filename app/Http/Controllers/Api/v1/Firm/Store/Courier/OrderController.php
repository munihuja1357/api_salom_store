<?php

namespace App\Http\Controllers\Api\v1\Firm\Store\Courier;

use App\Entity\Tenant\Firm\Store\Order\OrderItem;
use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Store\Courier;
use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Http\Requests\Firm\Store\Order\PhotoCheckOrderRequest;
use App\Http\Resources\Firm\Store\Order\OrderItemResource;
use App\Http\Resources\Firm\Store\Order\OrderResource;
use App\Filters\Store\OrderFilter;

/**
 * @group Заказы
 */
class OrderController extends Controller
{
    /**
     * Получить список заказов.
     *
     * @responseField id ID Заказа
     * @responseField first_name Имя клиента
     * @responseField last_name Фамилия клиента
     * @responseField phone Номер телефона
     * @responseField note Заметка по заказу
     * @responseField order_no Номер заказа
     * @responseField amount Сумма заказа
     * @responseField discount_percent Скидка в процентах
     * @responseField delivery_method_name Название доставки
     * @responseField delivery_method_name Название доставки
     * @responseField discount_cost Сумма скидки
     * @responseField total Общая сумма заказа с доставкой
     * @responseField delivery_date Дата доставки заказа
     * @responseField store Магазин
     * @responseField store.id Магазин: ID
     * @responseField store.name Магазин: Название
     * @responseField store.address Магазин: Адрес
     * @responseField store.logo Магазин: Логотип
     * @responseField store.lat Магазин: Широта на карте
     * @responseField store.lng Магазин: Долгота на карте
     *
     * @apiResourceCollection App\Http\Resources\Firm\Store\Order\OrderResource
     * @apiResourceModel App\Entity\Tenant\Firm\Store\Order\Order
     */
    public function index()
    {
        return OrderResource::collection(
            Order::where('status_id', 1)->get()
        );
    }

    /**
     * История доставок.
     *
     * @apiResourceCollection App\Http\Resources\Firm\Store\Order\OrderResource
     * @apiResourceModel App\Entity\Tenant\Firm\Store\Order\Order
     */
    public function getHistory()
    {
        return OrderResource::collection(
          Order::where('courier_id', courier()->id)
              ->whereNotNull('delivered_at')->get()
        );
    }

    /**
     * Взять заказ.
     *
     * @urlParam id integer required ID Заказа
     *
     * @response {
     *  'message': 'Вы назначены на заказ #номерЗаказа'
     * }
     */
    public function take($id)
    {
        abort_unless(!courier()->blocked_until >= now(),
            403,
            __('message.account_blocked')
        );
        abort_unless(courier()->is_free, 403, __('message.have_order'));

        try {
            Order::where('id', $id)
                ->whereNull('courier_id')
                ->update([
                    'courier_id' => courier()->id,
                ]);
        } catch (\DomainException $e) {
            return response()->json([
                'error' => $e,
            ]);
        }

        return response()->json([
            'message' => __('message.order_delivery'),
        ], 200);
    }


    public function orderItems($id)
    {
        return OrderItemResource::collection(
            OrderItem::where('order_id', $id)->get()
        );
    }
    public function photoCheckOrder(PhotoCheckOrderRequest $request,$id)
    {
        $order = Order::findOrFail($id);
        $order->addMedia($request->input('check_photo'))->toMediaCollection('check_photo');
        $order->addMedia($request->input('order_photo'))->toMediaCollection('order_photo');
        $order->save();
        return response()->json(['message' => __('message.photo_saved')]);
    }

    public function paymentTrue($id)
    {
        $order = Order::findOrFail($id);
        $order->payment_status=1;
        $order->save();
        return response()->json(['message' => __('message.updated')]);
    }
}
