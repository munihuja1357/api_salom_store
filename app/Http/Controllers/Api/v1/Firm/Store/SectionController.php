<?php

namespace App\Http\Controllers\Api\v1\Firm\Store;

use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Store\Section;
use App\Http\Resources\Firm\Store\SectionResource;

/**
 * @group Раздел.
 * */
class SectionController extends Controller
{
    /**
     * Список активных раздел.
     *
     * @responseField id ID
     * @responseField name Название раздела.
     * @responseField icon Иконка раздела.
     * @responseField stores[] Магазины которые входят в раздел.
     * @responseField parent_id ID родителя раздела.
     *
     * @apiResourceCollection App\Http\Resources\Firm\Store\SectionResource
     * @apiResourceModel App\Entity\Tenant\Firm\Store\Section
     * */
    public function index()
    {
        return SectionResource::collection(
            Section::active()->get()
        );
    }

    /**
     * Получить раздел по ID.
     *
     * @urlParam id required ID раздела.
     *
     * @responseField id ID
     * @responseField name Название раздела.
     * @responseField icon Иконка раздела.
     * @responseField stores[] Магазины которые входят в раздел.
     * @responseField parent_id ID родителя раздела.
     *
     * @apiResource App\Http\Resources\Firm\Store\SectionResource
     * @apiResourceModel App\Entity\Tenant\Firm\Store\Section
     * */
    public function show($id)
    {
        return SectionResource::make(
            Section::findOrFail($id)->active()->with(['stores'=>function ($q){
                $q->active();
            }])->firstOrFail()
        );
    }
}
