<?php

namespace App\Http\Controllers\Api\v1\Firm\Store;

use App\Entity\Tenant\Firm\Store\Store;
use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Store\Section;
use App\Http\Resources\Firm\Store\StoreResource;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function index()
    {
        return StoreResource::collection(
            Store::active()->get()
        );
    }

    public function getBySection($section_id)
    {
        return StoreResource::collection(
            Store::whereHas('section', function ($q) use ($section_id) {
                $q->active()->where('section_id', $section_id);
            })->active()->get()
        );
    }

    public function getByCategory($category_id)
    {
        return StoreResource::collection(
            Store::whereHas('categories', function ($q) use ($category_id) {
                $q->active()->where('category_id', $category_id);
            })->get()
        );
    }

    public function show($id)
    {
        return StoreResource::make(
            Store::with('overheads.priceList.product')
                ->whereHas('overheads', function ($q) {
                    $q->active()->whereHas('priceList', function ($q2) {
                        $q2->stock()->whereHas('product', function ($q3) {
                            $q3->sale();
                        });
                    });
                })->active()->findOrFail($id)
        );
    }
    public function showInfo($id)
    {
        return StoreResource::make(
            Store::active()->findOrFail($id)
        );
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'section_id'=>'nullable|integer|exists:sections,id'
        ]);
        $section_id=$request->input('section_id');
        $data=Store::where('name', 'like', '%' . $request->input('name') . '%')->active();
        if(!empty($section_id))$data->whereHas('section',function ($q) use($section_id){
            $q->where('section_id',$section_id);
        });
        return StoreResource::collection(
            $data->get()
        );
    }
}
