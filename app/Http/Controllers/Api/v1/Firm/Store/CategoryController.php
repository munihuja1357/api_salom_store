<?php

namespace App\Http\Controllers\Api\v1\Firm\Store;

use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Store\Category;
use App\Http\Resources\Firm\Store\CategoryResource;

class CategoryController extends Controller
{
    public function index()
    {
        return CategoryResource::collection(
            Category::active()->get()
        );
    }
    public function show($id)
    {
        return CategoryResource::make(
            Category::active()->with(['products'=>function($q){
                $q->sale()->whereHas('priceList',function ($q2){
                    $q2->stock()->whereHas('overhead', function ($q4) {
                        $q4->active()->whereHas('store',function ($q3) {
                            $q3->active();
                        });
                    });
                });
            }])->findOrFail($id)
        );
    }

    public function showByStore($store_id)
    {
        return CategoryResource::make(
            Category::active()->with(['stories'=>function($q) use($store_id){
                    $q->active()->where('store_id',$store_id);
                }])->firstOrFail()
        );
    }
    public function mainCategories()
    {
        return CategoryResource::collection(
            Category::main()->get()
        );
    }
}
