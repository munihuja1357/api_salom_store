<?php

namespace App\Http\Controllers\Api\v1\Firm\Store;

use App\Http\Controllers\Controller;
use App\UseCases\Firm\Store\PriceListService;
use App\Http\Requests\Firm\Store\Overhead\UpdatePriceListStatusRequest;

class PriceListController extends Controller
{
    private PriceListService $service;

    public function __construct(PriceListService $service)
    {
        return $this->service = $service;
    }

    public function updateStatuses(UpdatePriceListStatusRequest $request)
    {
        try {
            $this->service->updateStatuses($request);
        } catch (\DomainException $e) {
            return response()->json('error', $e->getMessage());
        }

        return response()->json([
            'message' => 'Статусы обновлены',
        ], 202);
    }
}
