<?php

namespace App\Http\Controllers\Api\v1\Firm\Store\Order;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Http\Resources\Firm\Store\Order\OrderResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use App\Http\Requests\Firm\Store\Order\ChangeOrderStatusRequest;
use App\Entity\Tenant\Firm\Store\Courier;
use App\Filters\Store\OrderFilter;

/**
 * @group Магазин
 */
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param OrderFilter $filters
     *
     * @return OrderResource|AnonymousResourceCollection
     */
    public function index(OrderFilter $filters)
    {
        return OrderResource::collection(
            Order::filter($filters)->paginate(20)
        );
    }

    /**
     * Назначение курьера.
     *
     * @urlParam id integer required ID Заказа
     * @bodyParam courier_id integer required ID Курьера
     *
     * @response {
     *  'message': 'Курьер назначен на заказ #номерЗаказа'
     * }
     *
     * @return JsonResponse
     */
    public function assignCourier($id, Request $request)
    {
        $order = Order::findOrFail($id);
        $courier = Courier::notBlocked()->findOrFail($request->courier_id);

        try {
            $order->assignCourier($courier);
        } catch (\DomainException $e) {
            return response()->json([
                'error' => 'Произошла ошибка. Повторите попытку позже.',
            ], 500);
        }

        return response()->json([
            'message' => 'Курьер назначен на заказ #'.$order->order_no,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Получить заказ по ID.
     *
     * @param $id int ID заказа
     *
     * @return OrderResource|AnonymousResourceCollection
     * @apiResourceCollection App\Http\Resources\Firm\Store\Order\OrderResource
     * @apiResourceModel App\Entity\Tenant\Firm\Store\Order\Order
     */
    public function show($id)
    {
        return OrderResource::make(
            Order::with('courier', 'items', 'customer', 'store')
                ->findOrFail($id)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Order   $order
     *
     * @return void
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Order $order
     *
     * @return Response
     */
    public function destroy(Order $order)
    {
        //
    }

    /**
     * Подтверждение заказа.
     *
     * Подтверждение наличие пунктов заказа у магазина.
     *
     * @urlParam id integer ID заказа
     *
     * @param ChangeOrderStatusRequest $request
     *
     * @return JsonResponse
     */
    public function changeStatus($id, ChangeOrderStatusRequest $request)
    {
        $order = Order::where('store_id', store()->id)->findOrFail($id);

        try {
            $order->update([
                'status_id' => $request->state,
            ]);
        } catch (\DomainException $e) {
            return response()->json(['message' => $e->getMessage()], 401);
        }

        return response()->json([
            'message' => 'Статус заказа успешно обновлён.',
        ], 200);
    }
}
