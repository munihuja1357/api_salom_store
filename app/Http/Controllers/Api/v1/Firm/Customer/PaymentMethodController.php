<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Store\PaymentMethod;
use App\Http\Controllers\Controller;
use App\Http\Resources\Firm\Store\PaymentMethodResource;
use Illuminate\Http\Request;

    class PaymentMethodController extends Controller
{

    public function __invoke()
    {
        return PaymentMethodResource::collection(
            PaymentMethod::active()->select('id','name')->get()
        );
    }
}
