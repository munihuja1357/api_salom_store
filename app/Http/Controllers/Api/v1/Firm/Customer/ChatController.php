<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Chat;
use App\Http\Controllers\Controller;
use App\Http\Requests\Firm\Customers\ChatRequest;
use App\Http\Resources\Firm\Customer\ChatResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function index()
    {
        return ChatResource::collection(
            Chat::whereHas('chat_model',function ($q){
                $q->where('id',Auth::user()->id);
            })->get()
        );
    }

    public function show($id)
    {
        return ChatResource::make(
            Chat::whereHas('chat_model',function ($q){
                $q->where('id',Auth::user()->id);
            })->where('id',$id)->with('message')->firstOrFail()
        );
    }

    public function store(ChatRequest $request)
    {
        $chat=new Chat();
        $chat->name=$request->input('name');
        $chat->chat_model_type=Auth::user()->classModel();
        $chat->chat_model_type=Auth::user()->id;
        $chat->save();
        return $chat;
    }

    public function delete($id)
    {
        return Chat::destroy($id);
    }
}
