<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\Firm\Customers\MessageRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function store(MessageRequest $request)
    {
        $sort=1;
        $sorted=Message::where('chat_id',$request->input('chat_id'))->max('sort');
        if($sorted&&$sorted>0)$sort=$sorted+1;
        $message=new Message();
        $message->message_from_model_type=Auth::user()->classModel();
        $message->message_from_model_id=Auth::user()->id;
        $message->message_to_model_id=$request->input('message_to_model_type_id');
        $message->message_to_model_type=$request->input('message_to_model_type');
        $message->text=$request->input('text');
        $message->chat_id=$request->input('chat_id');
        $message->sort=$sort;
        $message->save();
        return $message;
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'text' => 'required|string',
        ]);
        $message=Message::findOrFail($id);
        $message->text=$request->input('text');
        $message->save();
        return $message;
    }

    public function updateIsSeen($id)
    {
        $message=Message::findOrFail($id);
        $message->is_seen=true;
        $message->save();
        return $message;
    }

    public function delete($id)
    {
        return Message::destroy($id);
    }
}
