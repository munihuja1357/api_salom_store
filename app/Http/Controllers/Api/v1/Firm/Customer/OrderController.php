<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Address;
use App\Entity\Tenant\Firm\Customer\Cart;
use App\Entity\Tenant\Firm\Customer\CartItem;
use App\Entity\Tenant\Firm\Store\DeliveryMethod;
use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Entity\Tenant\Firm\Store\Order\OrderItem;
use App\Entity\Tenant\Firm\Store\PriceList;
use App\Entity\Tenant\Firm\Store\Store;
use App\Http\Controllers\Controller;
use App\Http\Requests\Firm\Customers\DeliveryChangeRequest;
use App\Http\Requests\Firm\Customers\OrderDeliveryRequest;
use App\Http\Resources\Firm\Store\Order\OrderResource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class OrderController extends Controller
{
    public function index()
    {
        return OrderResource::collection(
            Order::where('customer_id', Auth::user()->id)->orderBy('id', 'desc')->get()
        );
    }

    public function show($id)
    {
        return OrderResource::make(
            Order::with('items')->where('id', $id)->where('customer_id', Auth::user()->id)->firstOrFail()
        );
    }

    public function storeCart(OrderDeliveryRequest $request, $cart_id)
    {
        $request->merge(['store_id' => Cart::findOrFail($cart_id)->store_id]);
        $cart = CartItem::select('price_list_id', 'quantity')->active()->where('cart_id', $cart_id)->firstOrFail();
        $products = [];
        foreach ($cart as $item) {
            array_push($products, ['id' => $item->price_list_id, 'quantity' => $item->quantity]);
        }
        $request->merge(['products' => $products]);
        return $this->store($request);
    }

    public function store(OrderDeliveryRequest $request)
    {
        if (!$request->has('products')) {
            return response()->json(['errors' => ['products' => __('validation.required', 'products')]], 422);
        }
        if (!$request->has('store_id')) {
            return response()->json(['errors' => ['store_id' => __('validation.required', 'store_id')]], 422);
        }
        $products = $request->input('products');
        $delivery_cost = 15;
        $store_id = $request->input('store_id');
        $delivery_date = $request->input('delivery_date');
        $delivery_method_id = $request->input('delivery_method_id');
        $payment_method_id = $request->input('payment_method_id');
        $note = $request->input('note');
        $delivery_address_id = $request->input('address_id');
        DB::beginTransaction();
        try {
            $amount = 0;
            $sum_discount = 0;
            if (isset($products)) {
                foreach ($products as $item) {
                    $fn = PriceList::active()->sale()->where('id', $item['id'])->orderBy('created_at', 'desc')->first();
                    if (!$fn) {
                        return response()->json(['price_list_id' => $item['id'], 'errors' => ['products' => __('validation.exists', 'products')]], 422);
                    }
                    $price = $fn->price;
                    if($fn->discount_id!==null) {
                        if ($fn->discount->to_date < Carbon::now()) {
                            $price = $price - ($price * $fn->discount->percent) / 100;
                            $sum_discount = $sum_discount + ($price * $fn->discount->percent) / 100;
                        }
                    }else{
                        if ($fn->overhead->discount->to_date < Carbon::now()) {
                            $price = $price - ($price * $fn->overhead->discount->percent) / 100;
                            $sum_discount = $sum_discount + ($price * $fn->overhead->discount->percent) / 100;
                        }
                    }
                    if (!isset($products['quantity']) || $fn->quantity < $products['quantity']) {
                        return response()->json(['price_list_id' => $item['id'], 'errors' => ['quantity' => __('validation.exists', 'quantity')]], 422);
                    }
                    $amount = $amount + $price * $products['quantity'];
                }
            }
            $delivery_method_name = $this->getDeliveryName($delivery_method_id);
            if (empty($delivery_address_id)) $delivery_address_id = $this->checkAddress($request);
            $order = new Order();
            $order->store_id = $store_id;
            $order->custom_fields = [];
            $order->note = $note;
            $order->amount = $amount;
            $order->discount_percent = ($sum_discount * 100) / $price;
            $order->delivery_date = $delivery_date;
            $order->delivery_cost = $delivery_cost;
            $order->delivery_method_name = $delivery_method_name;
            $order->delivery_method_id = $delivery_method_id;
            $order->payment_method_id = $payment_method_id;
            $order->customer_id = Auth::user()->id;
            $order->status_id = Order::STATUS_UNPROCESSED;
            $order->delivery_address_id = $delivery_address_id;
            $order->save();
            if (isset($products)) {
                foreach ($products as $item) {
                    $fn = PriceList::active()->sale()->where('id', $item['id'])->orderBy('created_at', 'desc')->first();
                    $price = $fn->price;
                    if ($fn->discount->to_date < Carbon::now()) {
                        $price = $price - ($price * $fn->discount->percent) / 100;
                    }
                    OrderItem::create([
                        'order_id' => $order->id,
                        'price_list_id' => $fn->id,
                        'product_name' => $fn->product->name,
                        'price' => $fn->price,
                        'quantity' => $products['quantity'],
                        'total' => $price * $products['quantity'],
                    ]);
                }
            }
            DB::commit();
            return OrderResource::make(
                Order::findOrFail($order->id)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => __('message.alert_create')], 500);
        }

    }

    public function update(DeliveryChangeRequest $request)
    {
        $delivery_date = $request->input('delivery_date');
        $delivery_method_id = $request->input('delivery_method_id');
        $note = $request->input('note', '');
        $order_id = $request->input('order_id');
        DB::beginTransaction();
        try {
            $order = Order::find($order_id);
            $order->note = $note;
            if (!empty($delivery_date)) $order->delivery_date = $delivery_date;
            if (!empty($delivery_method_id)) {
                $delivery_method_name = $this->getDeliveryName($delivery_method_id);
                $order->delivery_method_name = $delivery_method_name;
                $order->delivery_method_id = $delivery_method_id;
            }
            $order->save();
            $this->storeAddress($request, $order->delivery_address_id);
            DB::commit();
            return OrderResource::make(
                Order::findOrFail($order->id)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => __('message.alert_update')], 500);
        }
    }

    private function getDeliveryName($delivery_id)
    {
        $delivery_method_name = DeliveryMethod::find($delivery_id);
        return $delivery_method_name->name;
    }

    private function checkAddress($request)
    {
        if (!($request->has('city_id') || $request->has('address') || $request->has('first_name_delivery') || $request->has('phone_delivery'))) {
            return response()->json(['errors' => [
                'city_id' => __('validation.required', 'city_id'),
                'first_name' => __('validation.required', 'first_name'),
                'phone' => __('validation.required', 'phone'),
                'address' => __('validation.required', 'address')
            ]], 422);
        }
        $last_name = $request->input('last_name_delivery');
        $delivery_address = Address::where('city_id', $request->input('city_id'))
            ->where('address', $request->input('address'))
            ->where('first_name', $request->input('first_name_delivery'))
            ->where('phone', $request->input('phone_delivery'));
        if (!empty($last_name)) $delivery_address->where('last_name', $last_name);
        $delivery_address = $delivery_address->first();
        if ($delivery_address)
            return $delivery_address->id;
        else
            return $this->storeAddress($request);
    }

    private function storeAddress($request, $id = false)
    {
        $city = $request->input('city_id');
        $first_name_delivery = $request->input('first_name_delivery');
        $last_name_delivery = $request->input('last_name_delivery');
        $address = $request->input('address');
        $phone_delivery = $request->input('phone_delivery');
        $address_secondary = $request->input('address_secondary');
        $additional_phone = $request->input('additional_phone');
        $company = $request->input('company');
        $alias = $request->input('alias');
        $lat = $request->input('lat');
        $lng = $request->input('lng');
        if ($id === false) {
            $delivery_address = new Address();
        } else {
            $delivery_address = Address::findOrFail($id);
        }
        if (!empty($city)) $delivery_address->city_id = $city;
        if (!empty($first_name_delivery)) $delivery_address->first_name = $first_name_delivery;
        if (!empty($last_name_delivery)) $delivery_address->last_name = $last_name_delivery;
        if (!empty($address)) $delivery_address->address = $address;
        if (!empty($phone_delivery)) $delivery_address->phone = $phone_delivery;
        if (!empty($address_secondary)) $delivery_address->address_secondary = $address_secondary;
        if (!empty($additional_phone)) $delivery_address->additional_phone = $additional_phone;
        if (!empty($company)) $delivery_address->company = $company;
        if (!empty($alias)) $delivery_address->alias = $alias;
        if (!empty($lat)) $delivery_address->lat = $lat;
        if (!empty($lng)) $delivery_address->lng = $lng;
        $delivery_address->save();
        return $delivery_address->id;
    }
    public function workDay($store_id,$delivery_date)
    {
        $weekStoreWork=Store::findOrFail($store_id)->work_day;
        $exclude=$weekStoreWork['exclude'];
        $work=false;
        if(array_search(date('Y-m-d',strtotime($delivery_date)),$exclude)!==false){
            $work=isset($weekStoreWork['week'][Carbon::parse($delivery_date)->weekday()])?true:false;
        }
        return $work;
    }
}
