<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Store\PriceList;
use App\Http\Controllers\Controller;
use App\Http\Requests\Firm\Store\SearchProductRequest;
use App\Http\Resources\Firm\Store\Overhead\PriceListResource;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function search(SearchProductRequest $request)
    {
        $store_id = $request->input('store_id');
        $start_price = $request->input('start_price');
        $end_price = $request->input('end_price');
        $quantity = $request->input('quantity');
        $product_name = $request->input('product_name');
        $product_brand_name = $request->input('product_brand_name');
        $product_category_id = $request->input('product_category_id');
        $orderAsc = $request->input('orderAscPrice');
        $data= PriceList::stock()->with('product')
            ->whereHas('overhead', function ($q) use ($store_id) {
                $q->active();
                if (!empty($store_id)) $q->where('store_id',  $store_id);
            })->whereHas('product', function ($q) use ($product_name,$product_category_id) {
                $q->sale()->whereHas('categories', function ($q2) use ($product_category_id) {
                    $q2->active();
                    if (!empty($product_category_id)) $q2->where('category_id', $product_category_id);
                });
                if (!empty($product_name)) $q->where('name', 'like', '%' . $product_name . '%');
            });
        if (!empty($product_brand_name)) {
            $data->whereHas('product.manufacture', function ($q) use ($product_brand_name) {
                $q->where('brand_name', $product_brand_name);
            });
        }
        if (!empty($start_price)) $data->where('price', '>', $start_price);
        if (!empty($end_price)) $data->where('price', '<=', $end_price);
        if (!empty($quantity)) $data->where('quantity', '>=', $quantity);
        if (!empty($orderAsc)&&$orderAsc) $data->orderBy('price', 'DESC');
        else  $data->orderBy('price');
        $data=$data->get();
        return  PriceListResource::collection($data);
    }
}
