<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Customer\Customer;
use App\UseCases\Firm\Customers\Auth\PhoneService;
use App\UseCases\Firm\Customers\Auth\RegisterService;

/**
 * @group Аутентификация клиента
 *
 * API для аутентификации в системе клиенту
 */
class VerifyPhoneNumberController extends Controller
{
    /**
     * @var RegisterService
     */
    private RegisterService $registerService;

    /**
     * @var PhoneService
     */
    private PhoneService $phoneService;

    public function __construct(PhoneService $phone, RegisterService $register)
    {
        $this->registerService = $register;
        $this->phoneService = $phone;
    }

    /**
     * Отправка кода подтверждения.
     *
     * Код подтверждения будет отправлен на номер телефона клиента.
     * Если до этого он не был зарегистрирован в системе, то для него создается новый пользователь.
     *
     * @bodyParam phone string required Номер телефона
     *
     * @response code_expiration Время действия кода подтверждения.
     *
     * @response 200 {
     * "message": "Код подтверждения отправлен на номер телефона"
     * "code_expiration: 5,
     * }
     */
    public function __invoke(Request $request)
    {
        $customer = Customer::where('phone', $request['phone'])->first();

        if (!$customer) {
            $this->registerService->register($request['phone']);
        } else {
            $this->phoneService->request($customer->id); //for sending SMS code
        }

        return response()->json([
            'message' => 'Код подтверждения отправлен на номер телефона',
            'code_expiration' => 5,
        ]);
    }
}
