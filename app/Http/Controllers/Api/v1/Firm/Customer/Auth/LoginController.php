<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer\Auth;

use Illuminate\Http\Response;
use App\Utilities\ProxyRequest;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Entity\Tenant\Firm\Customer\Customer;
use App\UseCases\Firm\Customers\Auth\PhoneService;
use App\Http\Requests\Firm\Customers\Auth\LoginRequest;

/**
 * @group Аутентификация клиента
 *
 * API для аутентификации в системе клиенту
 */
class LoginController extends Controller
{
    protected ProxyRequest $proxy;

    private PhoneService $phone;

    public function __construct(ProxyRequest $proxy, PhoneService $phone)
    {
        $this->proxy = $proxy;
        $this->phone = $phone;
    }

    /**
     * Вход в систему.
     *
     * @param LoginRequest $request
     *
     * <aside class="notice">refresh_token будет записан в куки.</aside>
     *
     * @return JsonResponse
     * @responseField access_token string Токен доступа
     * @responseField expiresIn int Срок действия access_token
     *
     * @response 200 {
     *  "access_token": "MTQ0NjJkZmQ5OTM2NDE1ZTZjNGZmZjI3",
     *  "expires_in": 3600,
     *  return "message"
     * }
     */
    public function login(LoginRequest $request)
    {
        $customer = Customer::where('phone', $request['phone'])->first();
        abort_unless($customer, 404, 'Неправильный номер телефона');
        $this->phone->verify($customer->id, $request);

        $resp = $this->proxy
            ->grantPasswordToken([
                'grant_type' => 'password',
                'client_id' => $request['client_id'],
                'client_secret' => $request['client_secret'],
                'username' => $request['phone'],
                'password' => $request['code'],
                'scope' => '',
            ]);

        return response()->json([
            'token' => $resp->access_token,
            'expiresIn' => $resp->expires_in,
            'message' => 'Вы вошли в систему',
        ], 200);
    }

    /**
     * Обновление AccessToken-а.
     *
     * @authenticated
     * */
    public function refreshToken()
    {
        $resp = $this->proxy->refreshAccessToken();

        return response([
            'token' => $resp->access_token,
            'expiresIn' => $resp->expires_in,
            'message' => 'Токен был обновлен.',
        ], 200);
    }

    /**
     * Выход из системы.
     *
     * @authenticated
     */
    public function __invoke()
    {
        $token = request()->user()->token();
        $token->delete();

        // remove the httponly cookie
        cookie()->queue(cookie()->forget('refresh_token'));

        return response()->json([
            'message' => 'Вы успешно вышли из системы.',
        ], 200);
    }
}
