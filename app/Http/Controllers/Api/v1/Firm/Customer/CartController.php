<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Customer\Cart;
use App\Http\Controllers\Controller;
use App\Http\Requests\Firm\Customers\CartRequest;
use App\Http\Resources\Firm\Customer\CartResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index()
    {
        return CartResource::collection(
            Cart::where('customer_id',Auth::user()->id)->get()
        );
    }

    public function show($id)
    {
        return CartResource::make(
            Cart::where('customer_id',Auth::user()->id)->where('id',$id)
                ->with('store','cart_items')->firstOrFail()
        );
    }

    public function store(CartRequest $request)
    {
        if(!$request->has('store_id')){
            return response()->json([
                "message"=> "The given data was invalid.",
            'errors'=>['store_id'=>__('validation.required',['attribute'=>'store_id'])]],422);
        }
        $cart=new Cart();
        $cart->customer_id=Auth::user()->id;
        $this->upd_store($request,$cart);
        return $cart;
    }

    public function update(CartRequest $request,$id)
    {
        $cart=Cart::where('customer_id',Auth::user()->id)->where('id',$id)->firstOrFail();
        return $this->upd_store($request,$cart);
    }

    public function update_template(CartRequest $request,$id)
    {
        $cart=Cart::where('customer_id',Auth::user()->id)->where('id',$id)->firstOrFail();
        return $this->upd_store($request,$cart);
    }

    private function upd_store(CartRequest $request,$cart)
    {
        if($request->has('name'))$cart->name=$request->input('name');
        if($request->has('store_id'))$cart->store_id=$request->input('store_id');
        if($request->has('is_template'))$cart->is_template=$request->input('is_template');
        return  $cart->save();
    }

    public function delete($id)
    {
        $cart=Cart::where('customer_id',Auth::user()->id)->where('id',$id)->firstOrFail();
        return $cart->delete();
    }
}
