<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Customer\Cart;
use App\Entity\Tenant\Firm\Customer\CartItem;
use App\Entity\Tenant\Firm\Store\Overhead;
use App\Http\Controllers\Controller;
use App\Http\Requests\Firm\Customers\CartItemRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartItemController extends Controller
{
    public function store(CartItemRequest $request)
    {
        $price_list_id=$request->input('price_list_id');
        $product_id=$request->input('product_id');
        $quantity=$request->input('quantity');
        $store_id=Overhead::active()->whereHas('priceList',function ($q) use($price_list_id,$product_id){
            $q->stock()->where('id',$price_list_id)->where('product_id',$product_id)
                ->whereHas('product', function ($q2) use ($product_id) {
                    $q2->sale()->where('id',$product_id);
                });
        })->firstOrFail();
        $cart_item=new CartItem();
        if(!$request->has('cart_id')){
            $cart=new Cart();
            $cart->store_id=$store_id->store_id;
            $cart->customer_id=Auth::user()->id;
            $cart->save();
            $cart_item->cart_id=$cart->id;
        }else{
            $cart_item_select=CartItem::where('cart_id',$request->input('cart_id'))
                ->where('price_list_id',$price_list_id)->active()->first();
            if ($cart_item_select){
                return response()->json(['message'=>__('message.cart_item_selected')],422);
            }
            $cart_item->cart_id=$request->input('cart_id');
        }
        $cart_item->product_id=$product_id;
        $cart_item->price_list_id=$price_list_id;
        $cart_item->status=CartItem::STATUS_ACTIVE;
        $cart_item->quantity=$quantity;
        $cart_item->save();
        return $cart_item;

    }
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'quantity' => 'required|integer|min:1',
        ]);
        $cart_item=CartItem::where('id',$id)->active()->whereHas('cart',function ($q){
            $q->where('customer_id', Auth::user()->id);
        })->firstOrFail();
        $cart_item->quantity=$request->input('quantity');
        return $cart_item->save();
    }

    public function delete($id)
    {
        $cart_item=CartItem::where('id',$id)->whereHas('cart',function ($q){
            $q->where('customer_id', Auth::user()->id);
        })->firstOrFail();
        $cart_item->status=CartItem::STATUS_CLOSED;
        return $cart_item->save();
    }
}
