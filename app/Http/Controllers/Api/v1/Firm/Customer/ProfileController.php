<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Customer\Customer;
use App\Http\Requests\Firm\Customers\LatLngRequest;
use App\Http\Requests\Firm\Customers\ProfileEditRequest;
use App\Http\Resources\Firm\Customer\ProfileResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param $id
     *
     * @return ProfileResource
     */
    public function show(Request $request)
    {
        return new ProfileResource($request->user());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     *
     * @return ProfileResource
     */
    public function update(ProfileEditRequest $request)
    {
        $customer = Customer::findOrFail(Auth::user()->id);
        if($request->has('first_name'))$customer->first_name=$request->input('first_name');
        if($request->has('last_name'))$customer->last_name=$request->input('last_name');
        if($request->has('email'))$customer->email=$request->input('email');
        if($request->has('note'))$customer->note=$request->input('note');
        if($request->has('password'))$customer->password=bcrypt($request->input('password'));
        $customer->save();
        return ProfileResource::make($customer);
    }

    public function storeLatLng(LatLngRequest $request)
    {
        $customer=Customer::findOrFail(Auth::user()->id);
        $customer->lat=$request->input('lat');
        $customer->lng=$request->input('lng');
        if($customer->save()){
            return response()->json(['message'=>__('message.updated')]);
        }else{
            return response()->json(['message'=>__('message.alert_update')],500);
        }
    }
}
