<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Store\DeliveryMethod;
use App\Http\Controllers\Controller;
use App\Http\Resources\Firm\Customer\DeliveryMethodResource;
use Illuminate\Http\Request;

class DeliveryMethodController extends Controller
{
    public function __invoke()
    {
        return DeliveryMethodResource::collection(
            DeliveryMethod::active()->orderBy('order_sort')->get()
        );
    }
}
