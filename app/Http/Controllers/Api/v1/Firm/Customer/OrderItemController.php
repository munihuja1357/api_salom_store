<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Store\Order\OrderItem;
use App\Http\Controllers\Controller;
use App\Http\Resources\Firm\Store\Order\OrderItemResource;
use Illuminate\Support\Facades\Auth;

class OrderItemController extends Controller
{
    public function index($order_id)
    {
        return OrderItemResource::collection(
            OrderItem::with('priceList')->where('order_id',$order_id)
                ->whereHas('order',function ($q) {
                    $q->where('customer_id', Auth::user()->id);
                })->get()
        );
    }
}
