<?php

namespace App\Http\Controllers\Api\v1\Firm\Customer;

use App\Entity\Tenant\Firm\Store\CommentProduct;
use App\Entity\Tenant\Firm\Store\PriceList;
use App\Entity\Tenant\Firm\Store\Product;
use App\Http\Controllers\Controller;
use App\Http\Requests\Firm\Store\CommentRequest;
use App\Http\Resources\Firm\Store\CommentProductResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentProductController extends Controller
{
    public function index($id,$store_id)
    {
        return CommentProductResource::collection(
            CommentProduct::where('product_id',$id)->where('store_id',$store_id)->orderBy('parent_id')->get()
        );
    }

    public function store(CommentRequest $request, $id,$store_id)
    {
        PriceList::where('product_id',$id)->stock()->whereHas('product',function ($q){
            $q->sale();
        })->whereHas('overhead',function ($q) use ($store_id){
            $q->active()->where('store_id',$store_id);
        })->firstOrFail();
        if($request->input('parent_id')!=0) {
            CommentProduct::where('id',$request->input('parent_id'))->where('product_id',$id)
                ->where('store_id',$store_id)->where('customer_id',Auth::user()->id)->firstOrFail();
        }
        $comment=new CommentProduct();
        $comment->product_id=$id;
        $comment->store_id=$store_id;
        $comment->parent_id=$request->input('parent_id');
        $comment->comment=$request->input('comment');
        $comment->customer_id=Auth::user()->id;
        $comment->save();
        return CommentProductResource::make($comment);
    }

    public function destroy($id)
    {
        return CommentProduct::destroy($id);
    }
}
