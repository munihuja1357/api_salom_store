<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Inertia\Inertia;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function show()
    {
        return Inertia::render('firm/auth/login');
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->hasAnyRole(['Manager', 'Owner'])) {
            return redirect()->route('manager:dashboard');
        }

        return redirect()->route('store:dashboard');
    }
}
