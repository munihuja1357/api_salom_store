<?php

namespace App\Policies\Tenant\Firm;

use App\Entity\Tenant\Firm\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy extends ModelPolicy
{
    use HandlesAuthorization;

    protected string $alias = 'users';

    protected function getModelClass() :string
    {
        return User::class;
    }
}
