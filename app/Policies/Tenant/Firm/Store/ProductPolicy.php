<?php

namespace App\Policies\Tenant\Firm\Store;

use Str;
use App\Entity\Tenant\Firm\User;
use App\Entity\Tenant\Firm\Store\Product;
use App\Policies\Tenant\Firm\ModelPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy extends ModelPolicy
{
    use HandlesAuthorization;

    protected string $alias = 'products';

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function search(User $user)
    {
        return $user->can('search-'.$this->alias);
    }
}
