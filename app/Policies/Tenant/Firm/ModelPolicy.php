<?php

namespace App\Policies\Tenant\Firm;

use App\Entity\Tenant\Firm\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Access\HandlesAuthorization;

abstract class ModelPolicy
{
    use HandlesAuthorization;

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected string $alias;

    abstract protected function getModelClass(): string;

    public function viewAny(User $user)
    {
        return $user->can('view-any-'.$this->alias);
    }

    public function view(User $user, Model $model)
    {
        if ($user->can('view-'.$this->alias)) {
            return true;
        }

        if ($user->can('view-self-'.$this->alias)) {
            return $this->isOwner($user, $model);
        }

        return false;
    }

    public function create(User $user)
    {
        return $user->can('create-'.$this->alias);
    }

    public function update(User $user, Model $model)
    {
        if ($user->can('update-'.$this->alias)) {
            return true;
        }

        if ($user->can('update-self-'.$this->alias)) {
            return $this->isOwner($user, $model);
        }

        return false;
    }

    public function delete(User $user, Model $model)
    {
        if ($user->can('delete-'.$this->alias)) {
            return true;
        }

        if ($user->can('delete-self-'.$this->alias)) {
            return $this->isOwner($user, $model);
        }

        return false;
    }

    private function isOwner(User $user, Model $model): bool
    {
        if (!empty($user) && method_exists($model, 'user')) {
            return $user->getKey() === $model->getRelation('user')->getKey();
        }

        return false;
    }
}
