<?php

namespace App\UseCases\Cpanel;

class Domain extends Cpanel
{
    public function addSubDomain()
    {
        $this->cpanel->execute_action(
            2,
            'SubDomain',
            'addsubdomain',
            $this->cpanel->getUsername(),
            [
                'domain' => request('domain'),
                'rootdomain' => 'salom.mobi',
                'dir' => '/salom.mobi/public',
                'disallowdot' => 1,
            ]
        );
    }
}
