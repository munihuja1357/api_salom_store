<?php

namespace App\UseCases\Cpanel;

use Stancl\Tenancy\Contracts\TenantWithDatabase;

class Db extends Cpanel
{
    public function createDb(TenantWithDatabase $tenant)
    {
        $this->cpanel->execute_action(2,
            'MysqlFE',
            'createdb',
            $this->cpanel->getUsername(),
            [
                'db' => $tenant->database->getName(),
            ]
        );
    }

    public function createDbUser(TenantWithDatabase $tenant)
    {
        $this->cpanel->execute_action(
            2,
            'MysqlFE',
            'createdbuser',
            [
                'dbuser' => $tenant->database->getUsername(),
                'password' => $tenant->database->getPassword(),
            ]
        );
    }

    public function setDbUser(TenantWithDatabase $tenant)
    {
        $this->cpanel->execute_action(2,
            'MysqlFE',
            'setdbuserprivileges',
            $this->cpanel->getUsername(),
            [
                'privileges' => 'ALL PRIVILEGES',
                'db' => $tenant->database->getName(),
                'dbuser' => $tenant->database->getUsername(),
            ]
        );
    }

    public function setup()
    {
        $this->createDb();
        $this->createDbUser();
        $this->setDbUser();
    }
}
