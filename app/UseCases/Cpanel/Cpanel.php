<?php

namespace App\UseCases\Cpanel;

class Cpanel
{
    protected $cpanel;

    public function __construct()
    {
        $this->cpanel = new \Gufy\CpanelPhp\Cpanel([
            'host' => 'https://scp69.hosting.reg.ru:2083', // ip or domain complete with its protocol and port
            'username' => config('services.cpanel.username'), // username of your server, it usually root.
            'auth_type' => 'password', // set 'hash' or 'password'
            'password' => config('services.cpanel.password'), // long hash or your user's password
        ]);
    }


}
