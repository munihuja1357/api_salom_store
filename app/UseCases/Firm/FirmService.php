<?php

namespace App\UseCases\Firm;

use Illuminate\Support\Str;
use App\Entity\Tenant\Firm\Firm;
use App\UseCases\Cpanel\Domain;

class FirmService
{
    protected $domain;

    public function __construct(Domain $domain)
    {
        $this->domain = $domain;
    }

    public function create(
        int $tenantId,
        string $name,
        string $domain): void
    {
        $dbSlug = 'firm_'.Str::snake($domain.random_int(10000, 99999));

        $firm = Firm::create([
            'name' => $name,
            'tenant_id' => $tenantId,
            'is_main' => true,
            'status' => Firm::STATUS_UNPAID,
            'tenancy_db_name' => $dbSlug,
            'tenancy_db_username' => $dbSlug,
            'tenancy_db_password' => Str::random(12),
        ]);

        $firm->domains()->create(['domain' => $domain]);
    }
}
