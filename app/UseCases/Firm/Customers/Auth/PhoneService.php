<?php

namespace App\UseCases\Firm\Customers\Auth;

use App\Services\Sms\SmsSender;
use App\Http\Requests\Firm\Customers\Auth\LoginRequest;
use App\Entity\Tenant\Firm\Customer\Customer;

class PhoneService
{
    private SmsSender $sms;

    public function __construct(SmsSender $sms)
    {
        $this->sms = $sms;
    }

    public function request($id)
    {
        $customer = $this->getCustomer($id);

        $token = $customer->requestPhoneVerification();
        $this->sms->send($customer->phone, 'Ваш код для входа в SalomMobi: '.$token);

        return $token;
    }

    public function verify($id, LoginRequest $request)
    {
        $customer = $this->getCustomer($id);
        $customer->verifyPhone($request['code']);
    }

    private function getCustomer($id): Customer
    {
        return Customer::findOrFail($id);
    }
}
