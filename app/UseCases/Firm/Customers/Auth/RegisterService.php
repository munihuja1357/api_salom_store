<?php

namespace App\UseCases\Firm\Customers\Auth;

use App\Utilities\ProxyRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Events\Dispatcher;
use App\Http\Requests\Firm\Customers\Auth\RegisterRequest;
use App\Entity\Tenant\Firm\Customer\Customer;
use App\Events\Customer\CustomerRegistered;

class RegisterService
{
    private Dispatcher $dispatcher;

    private PhoneService $phoneService;

    private ProxyRequest $proxy;

    public function __construct(Dispatcher $dispatcher, PhoneService $phone, ProxyRequest $proxy)
    {
        $this->dispatcher = $dispatcher;
        $this->phoneService = $phone;
        $this->proxy = $proxy;
    }

    public function register(string $phone)
    {
        $customer = Customer::create([
            'phone' => $phone,
            'status' => Customer::STATUS_NOT_VERIFIED,
        ]);

        event(new CustomerRegistered($customer));
    }
}
