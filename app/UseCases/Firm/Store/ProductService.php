<?php

namespace App\UseCases\Firm\Store;

use App\Entity\Tenant\Firm\User;
use Illuminate\Support\Facades\DB;
use App\Entity\Tenant\Firm\Store\Product;
use App\Http\Requests\Firm\Store\Product\CreateRequest;
use App\Http\Requests\Firm\Store\Overhead\UpdatePriceListStatusRequest;

class ProductService
{
    public function create($userId, CreateRequest $request)
    {
        $user = User::findOrFail($userId);

        return DB::transaction(function () use ($request, $user) {
            $product = Product::make([
                'name' => $request['name'],
                'barcode' => $request['barcode'],
                'short_description' => $request['short_description'],
                'description' => $request['description'],
                'manufacture_id' => $request['manufacture_id'],
                'status' => Product::STATUS_MODERATION,
            ]);

            $product->creator()->associate($user);

            $product->saveOrFail();

            $product->categories()->attach($request['categories']);
            $product->tags()->attach($request['tags']);

            if ($request->hasFile('images')) {
                $product->addMultipleMediaFromRequest(['images'])
                    ->each(function ($fileAdder) {
                        $fileAdder->toMediaCollection('images.product');
                    });
            }

            return $product;
        });
    }
}
