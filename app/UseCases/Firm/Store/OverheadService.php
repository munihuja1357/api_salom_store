<?php

namespace App\UseCases\Firm\Store;

use App\Entity\Tenant\Firm\User;
use Illuminate\Support\Facades\DB;
use App\Entity\Tenant\Firm\Store\Overhead;
use App\Http\Requests\Firm\Store\Overhead\CreateRequest;

class OverheadService
{
    public function create($userId, CreateRequest $request)
    {
        $user = User::findOrFail($userId);

        return DB::transaction(function () use ($request, $user) {
            $overhead = Overhead::make([
                'number' => $request['number'],
                'store_id' => $request['store_id'],
                'discount_id' => $request['discount_id'],
                'status' => Overhead::STATUS_IN_ACTIVE,
            ]);
            $overhead->creator()->associate($user);
            $overhead->store()->associate(store()->id);

            $overhead->saveOrFail();

            return $overhead;
        });
    }
}
