<?php

namespace App\UseCases\Firm\Store;

use Auth;
use App\Entity\Tenant\Firm\Store\PriceList;
use App\Http\Requests\Firm\Store\Overhead\UpdatePriceListStatusRequest;

class PriceListService
{
    public function create($overheadId, $request)
    {
        $user = Auth::user();

        $priceList = PriceList::make([
            'overhead_id' => $overheadId,
            'product_id' => $request['product_id'],
            'quantity' => $request['quantity'],
            'old_price' => $request['old_price'],
            'price' => $request['price'],
            'discount_id' => $request['discount_id'] ?? null,
            'status' => PriceList::STATUS_IN_STOCK,
        ]);

        $priceList->creator()->associate($user);
        $priceList->saveOrFail();

        return $priceList;
    }

    // TODO Исправить баг со сменой статуса
    public function updateStatuses(UpdatePriceListStatusRequest $request)
    {
        PriceList::whereIn('id', $request)
            ->update([
                'status' => $request->status,
            ]);
    }
}
