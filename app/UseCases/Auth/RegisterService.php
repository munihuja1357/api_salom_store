<?php

namespace App\UseCases\Auth;

use App\Mail\Auth\VerifyMail;
use Illuminate\Contracts\Mail\Mailer;
use App\Http\Requests\Auth\RegisterRequest;
use Illuminate\Contracts\Events\Dispatcher;
use App\Entity\Tenant\Tenant;

class RegisterService
{
    private $mailer;

    private $dispatcher;

    public function __construct(Dispatcher $dispatcher, Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->dispatcher = $dispatcher;
    }

    public function register(RegisterRequest $request): void
    {
        $tenant = Tenant::register(
            $request['first_name'],
            $request['last_name'],
            $request['phone_number'],
            $request['email'],
            $request['password']
        );

        $this->mailer->to($tenant->email)->send(new VerifyMail($tenant));
    }

    public function verify($id): void
    {
        $tenant = Tenant::findOrFail($id);
        $tenant->verify();
    }
}
