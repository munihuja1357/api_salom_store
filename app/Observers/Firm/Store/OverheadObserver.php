<?php

namespace App\Observers\Firm\Store;

use App\Entity\Tenant\Firm\Store\Overhead;
use App\Entity\Tenant\Firm\Store\Order\Order;

class OverheadObserver
{
    /**
     * Handle the overhead "created" event.
     *
     * @param Overhead $overhead
     */
    public function creating(Overhead $overhead)
    {
        if (empty($overhead->number)) {
            $overhead->number = Overhead::where('store_id', $overhead->store_id)->max('number') + 1;
        }
    }

    /**
     * Handle the overhead "updated" event.
     *
     * @param Overhead $overhead
     */
    public function updated(Overhead $overhead)
    {
        //
    }

    /**
     * Handle the overhead "deleted" event.
     *
     * @param Overhead $overhead
     */
    public function deleted(Overhead $overhead)
    {
        //
    }

    /**
     * Handle the overhead "restored" event.
     *
     * @param Overhead $overhead
     */
    public function restored(Overhead $overhead)
    {
        //
    }

    /**
     * Handle the overhead "force deleted" event.
     *
     * @param Overhead $overhead
     */
    public function forceDeleted(Overhead $overhead)
    {
        //
    }
}
