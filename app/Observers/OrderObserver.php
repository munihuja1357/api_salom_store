<?php

namespace App\Observers;

use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Entity\Tenant\Firm\Store\Overhead;

class OrderObserver
{
    /**
     * Handle the order "creating" event.
     *
     * @param Order $order
     */
    public function creating(Order $overhead)
    {
      if (empty($order->order_no)) {
            $order->order_no = Order::where('store_id', $order->store_id)->max('order_no') + 1;
        }
    }

    /**
     * Handle the order "updated" event.
     *
     * @param Order $order
     */
    public function updated(Order $order)
    {
        //
    }

    /**
     * Handle the order "deleted" event.
     *
     * @param Order $order
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the order "restored" event.
     *
     * @param Order $order
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the order "force deleted" event.
     *
     * @param Order $order
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
