<?php

namespace App\Listeners\Customer;

use App\Events\Customer\CustomerRegistered;
use App\UseCases\Firm\Customers\Auth\PhoneService;

class SendVerificationSmsNotification
{
    /**
     * @var PhoneService
     */
    private PhoneService $phone;

    public function __construct(PhoneService $phone)
    {
        $this->phone = $phone;
    }

    public function handle(CustomerRegistered $event)
    {
        $this->phone->request($event->customer->id); //for sending SMS code
    }
}
