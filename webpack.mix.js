const mix = require('laravel-mix');
const path = require('path');
const config = require('./webpack.config')

mix.webpackConfig(config)

mix.js('resources/js/app.js', 'public/js')
    .vue();

mix.sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false,
        postCss: [
            require('tailwindcss')
        ],
    })
    .webpackConfig({
        output: {
            chunkFilename: 'js/[name].js?id=[chunkhash]',
            path: path.resolve(__dirname, './public')
        },
    })
    .babelConfig({
        plugins: ['@babel/plugin-syntax-dynamic-import'],
    })
    .sourceMaps();

if (mix.inProduction()) {
    mix.version();
}
