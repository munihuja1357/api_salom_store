<?php
return [
    'success' => 'Success',
    'added' => 'Added',
    'updated' => 'Updated',
    'deleted' => 'Deleted',
    'status' => 'status',
    'alert' => 'Error',
    'alert_create' => 'An error occurred while creating a new record.',
    'alert_update' => 'An error occurred while updating record.',
    'no_records_found' => 'No Records Found.',
    'success_logout' => 'You have been successfully logged out.',
    'account_blocked' => 'Your account is blocked.',
    'have_order' => 'You already have a valid order.',
    'order_delivery' => 'You are now assigned to deliver this order.',
    'photo_saved' => 'Photos are saved.',
    'code_send_to_phone' => 'Confirmation code sent to phone number.',
];
