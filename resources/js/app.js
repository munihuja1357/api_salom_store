require('./bootstrap');

import Vue from 'vue';
import { Errors } from "@/utils/form";
import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaProgress } from '@inertiajs/progress';

Vue.component('v-icon', require('./components/inline-svg/index.js').default);

/**
 * Register Plugins.
 */
require('./plugins');

/**
 * Register Vue Prototypes.
 */
require('./prototypes');

/**
 * Register Vue Directives.
 */
require('./directives');

/**
 * Register Route Mixin.
 */

Vue.mixin({ methods: { route } });

/**
* Progress bar
* */

InertiaProgress.init({
    // The delay after which the progress bar will
    // appear during navigation, in milliseconds.
    delay: 250,

    // The color of the progress bar.
    color: '#9061f9',

    // Whether to include the default NProgress styles.
    includeCSS: true,

    // Whether the NProgress spinner will be shown.
    showSpinner: false,
})


/**
 * Initialize Vue application instance
 */
let app = document.getElementById('app');
let title = document.head.querySelector('meta[name="title"]').content;

new Vue({
    metaInfo() {
        return {
            title: 'Loading…',
            titleTemplate: '%s - ' + title
        }
    },

    render: h => h(InertiaApp, {
        props: {
            initialPage: JSON.parse(app.dataset.page),
            resolveComponent: name => import(`@/views/${name}`).then(module => module.default),
            transformProps: props => {
                return {
                    ...props,
                    errors: new Errors(props.errors),
                }
            },
        },
    }),
}).$mount(app);
