import Vue from 'vue'
import { InertiaApp, plugin } from "@inertiajs/inertia-vue";
import VueMeta from 'vue-meta';
import PortalVue from 'portal-vue';
import YmapPlugin from 'vue-yandex-maps';

const plugins = [
    plugin,
    PortalVue,
    VueMeta,
];

const settings = {
    apiKey: '23f6e59c-df53-4250-ae19-f3e7510b1757',
    lang: 'ru_RU',
    coordorder: 'latlong',
    version: '2.1'
}

Vue.use(YmapPlugin, settings);

plugins.forEach(plugin => {
    Vue.use(plugin);
});

export {
    InertiaApp
}
