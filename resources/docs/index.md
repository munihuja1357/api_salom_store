---
title: CRM Documentation

language_tabs:
- bash
- javascript

includes:
- "./prepend.md"
- "./authentication.md"
- "./groups/*"
- "./errors.md"
- "./append.md"

logo: 

toc_footers:
- <a href="./collection.json">Посмотреть коллекцию Postman</a>

---

# Вступление



Эта документация призвана предоставить всю информацию, необходимую для работы с нашим API.

<aside>По мере прокрутки вы увидите примеры кода для работы с API на разных языках программирования в темной области справа (или как часть контента на мобильном устройстве).
Вы можете переключить используемый язык с помощью вкладок в правом верхнем углу (или в меню навигации в левом верхнем углу на мобильном устройстве). </aside>

<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
<script>
    var baseUrl = "salom.crm.loc";
</script>
<script src="js/tryitout-2.4.2.js"></script>

> Base URL

```yaml
salom.crm.loc
```
