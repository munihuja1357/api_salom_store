# Счета


## Получить список счетов




> Пример запроса:

```bash
curl -X GET \
    -G "salom.crm.loc/api/couriers/accounts" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/couriers/accounts"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 85,
        "name": "iste ut",
        "type_id": 1,
        "balance": "83",
        "date_opened": null,
        "date_closed": null,
        "is_blocked": false
    },
    {
        "id": 86,
        "name": "deleniti quo",
        "type_id": 1,
        "balance": "135",
        "date_opened": null,
        "date_closed": null,
        "is_blocked": false
    }
]
```
<div id="execution-results-GETapi-couriers-accounts" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-GETapi-couriers-accounts"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-couriers-accounts"></code></pre>
</div>
<div id="execution-error-GETapi-couriers-accounts" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-GETapi-couriers-accounts"></code></pre>
</div>
<form id="form-GETapi-couriers-accounts" data-method="GET" data-path="api/couriers/accounts" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-couriers-accounts', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-couriers-accounts" onclick="tryItOut('GETapi-couriers-accounts');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-couriers-accounts" onclick="cancelTryOut('GETapi-couriers-accounts');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-couriers-accounts" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/couriers/accounts</code></b>
</p>
</form>



