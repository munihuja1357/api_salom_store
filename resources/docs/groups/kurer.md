# Курьер


## Информация о курьере.




> Пример запроса:

```bash
curl -X GET \
    -G "salom.crm.loc/api/couriers/profile" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/couriers/profile"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "id": 90,
    "first_name": "Mariah",
    "last_name": "Volkman",
    "full_name": "Mariah Volkman",
    "avatar_url": "salom.crm.loc\/img\/avatar.jpg",
    "phone": "1-462-898-5826 x51822",
    "email": "zackery84@example.org",
    "total_orders": 11,
    "is_free": 1,
    "blocked_until": null,
    "lat": 40.211775,
    "lng": 69.654779
}
```
<div id="execution-results-GETapi-couriers-profile" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-GETapi-couriers-profile"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-couriers-profile"></code></pre>
</div>
<div id="execution-error-GETapi-couriers-profile" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-GETapi-couriers-profile"></code></pre>
</div>
<form id="form-GETapi-couriers-profile" data-method="GET" data-path="api/couriers/profile" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-couriers-profile', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-couriers-profile" onclick="tryItOut('GETapi-couriers-profile');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-couriers-profile" onclick="cancelTryOut('GETapi-couriers-profile');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-couriers-profile" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/couriers/profile</code></b>
</p>
</form>


## Сменить статус курьера.




> Пример запроса:

```bash
curl -X PUT \
    "salom.crm.loc/api/couriers/status" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"state":"1"}'

```

```javascript
const url = new URL(
    "salom.crm.loc/api/couriers/status"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "state": "1"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (200):

```json
{
    "message": "Ваш статус изменён."
}
```
<div id="execution-results-PUTapi-couriers-status" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-PUTapi-couriers-status"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-couriers-status"></code></pre>
</div>
<div id="execution-error-PUTapi-couriers-status" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-PUTapi-couriers-status"></code></pre>
</div>
<form id="form-PUTapi-couriers-status" data-method="PUT" data-path="api/couriers/status" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-couriers-status', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-couriers-status" onclick="tryItOut('PUTapi-couriers-status');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-couriers-status" onclick="cancelTryOut('PUTapi-couriers-status');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-couriers-status" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/couriers/status</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры тела запроса</b></h4>
<p>
<b><code>state</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="state" data-endpoint="PUTapi-couriers-status" data-component="body" required  hidden>
<br>
1 - Занять, <br /> 0 - Свободен <br /> The value must be one of <code>0</code> or <code>1</code>.</p>

</form>



