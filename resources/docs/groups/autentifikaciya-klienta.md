# Аутентификация клиента

API для аутентификации в системе клиенту

## Отправка кода подтверждения.


Код подтверждения будет отправлен на номер телефона клиента.
Если до этого он не был зарегистрирован в системе, то для него создается новый пользователь.

> Пример запроса:

```bash
curl -X POST \
    "salom.crm.loc/api/customers/auth/verify" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"phone":"sequi"}'

```

```javascript
const url = new URL(
    "salom.crm.loc/api/customers/auth/verify"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "phone": "sequi"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (200):

```json

code_expiration Время действия кода подтверждения.
```
> Example response (200):

```json

{
"message": "Код подтверждения отправлен на номер телефона"
"code_expiration: 5,
}
```
<div id="execution-results-POSTapi-customers-auth-verify" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-POSTapi-customers-auth-verify"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-customers-auth-verify"></code></pre>
</div>
<div id="execution-error-POSTapi-customers-auth-verify" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-POSTapi-customers-auth-verify"></code></pre>
</div>
<form id="form-POSTapi-customers-auth-verify" data-method="POST" data-path="api/customers/auth/verify" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-customers-auth-verify', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-customers-auth-verify" onclick="tryItOut('POSTapi-customers-auth-verify');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-customers-auth-verify" onclick="cancelTryOut('POSTapi-customers-auth-verify');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-customers-auth-verify" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/customers/auth/verify</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры тела запроса</b></h4>
<p>
<b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="phone" data-endpoint="POSTapi-customers-auth-verify" data-component="body" required  hidden>
<br>
Номер телефона</p>

</form>


## Вход в систему.




> Пример запроса:

```bash
curl -X POST \
    "salom.crm.loc/api/customers/auth/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"phone":"eum","code":12}'

```

```javascript
const url = new URL(
    "salom.crm.loc/api/customers/auth/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "phone": "eum",
    "code": 12
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (200):

```json

{
 "access_token": "MTQ0NjJkZmQ5OTM2NDE1ZTZjNGZmZjI3",
 "expires_in": 3600,
 return "message"
}
```
<div id="execution-results-POSTapi-customers-auth-login" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-POSTapi-customers-auth-login"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-customers-auth-login"></code></pre>
</div>
<div id="execution-error-POSTapi-customers-auth-login" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-POSTapi-customers-auth-login"></code></pre>
</div>
<form id="form-POSTapi-customers-auth-login" data-method="POST" data-path="api/customers/auth/login" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-customers-auth-login', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-customers-auth-login" onclick="tryItOut('POSTapi-customers-auth-login');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-customers-auth-login" onclick="cancelTryOut('POSTapi-customers-auth-login');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-customers-auth-login" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/customers/auth/login</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры тела запроса</b></h4>
<p>
<b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="phone" data-endpoint="POSTapi-customers-auth-login" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>code</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="code" data-endpoint="POSTapi-customers-auth-login" data-component="body" required  hidden>
<br>
</p>

</form>

### Ответ
<h4 class="fancy-heading-panel"><b>Поля ответа</b></h4>
<p>
<b><code>access_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Токен доступа</p>
<p>
<b><code>expiresIn</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<br>
Срок действия access_token</p>

## Обновление AccessToken-а.

<small class="badge badge-darkred">требует аутентификации</small>



> Пример запроса:

```bash
curl -X POST \
    "salom.crm.loc/api/customers/auth/refresh-token" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/customers/auth/refresh-token"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTapi-customers-auth-refresh-token" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-POSTapi-customers-auth-refresh-token"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-customers-auth-refresh-token"></code></pre>
</div>
<div id="execution-error-POSTapi-customers-auth-refresh-token" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-POSTapi-customers-auth-refresh-token"></code></pre>
</div>
<form id="form-POSTapi-customers-auth-refresh-token" data-method="POST" data-path="api/customers/auth/refresh-token" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-customers-auth-refresh-token', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-customers-auth-refresh-token" onclick="tryItOut('POSTapi-customers-auth-refresh-token');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-customers-auth-refresh-token" onclick="cancelTryOut('POSTapi-customers-auth-refresh-token');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-customers-auth-refresh-token" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/customers/auth/refresh-token</code></b>
</p>
<p>
<label id="auth-POSTapi-customers-auth-refresh-token" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-customers-auth-refresh-token" data-component="header"></label>
</p>
</form>


## Выход из системы.

<small class="badge badge-darkred">требует аутентификации</small>



> Пример запроса:

```bash
curl -X POST \
    "salom.crm.loc/api/customers/auth/logout" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/customers/auth/logout"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTapi-customers-auth-logout" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-POSTapi-customers-auth-logout"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-customers-auth-logout"></code></pre>
</div>
<div id="execution-error-POSTapi-customers-auth-logout" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-POSTapi-customers-auth-logout"></code></pre>
</div>
<form id="form-POSTapi-customers-auth-logout" data-method="POST" data-path="api/customers/auth/logout" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-customers-auth-logout', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-customers-auth-logout" onclick="tryItOut('POSTapi-customers-auth-logout');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-customers-auth-logout" onclick="cancelTryOut('POSTapi-customers-auth-logout');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-customers-auth-logout" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/customers/auth/logout</code></b>
</p>
<p>
<label id="auth-POSTapi-customers-auth-logout" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-customers-auth-logout" data-component="header"></label>
</p>
</form>



