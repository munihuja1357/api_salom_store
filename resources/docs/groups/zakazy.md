# Заказы


## Получить список заказов.




> Пример запроса:

```bash
curl -X GET \
    -G "salom.crm.loc/api/couriers/orders" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/couriers/orders"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 171,
        "first_name": "Cleo",
        "last_name": "Parisian",
        "phone": "+1 (607) 752-7997",
        "note": "Doloremque quidem sed ullam fugiat libero iste eum.",
        "order_no": 404895,
        "amount": "10,259.55",
        "discount_percent": 4,
        "delivery_method_name": "",
        "discount_cost": "0",
        "total": "10,274.22",
        "delivery_date": null,
        "delivery_address": {
            "id": 221,
            "first_name": "Brandi",
            "last_name": "Gottlieb",
            "address": "80146 Romaguera Bridge Suite 860\nPort Sophia, MO 13432",
            "address_secondary": "847 Steuber Trace Apt. 208\nNew Alexa, MS 47313-7863",
            "phone": "+1 (361) 968-9786",
            "additional_phone": "+1-543-810-7261",
            "company": "Hoeger PLC",
            "alias": "Дом",
            "custom_fields": null
        },
        "payment_method": null,
        "status": 2,
        "cancel_reason_id": null,
        "created": "09:51",
        "updated": "09:51"
    },
    {
        "id": 172,
        "first_name": "Caleb",
        "last_name": "Cartwright",
        "phone": "1-416-548-9594 x72471",
        "note": "Nisi aut enim ex alias et. Aut quam velit nemo ducimus mollitia totam. Quis incidunt eum amet.",
        "order_no": 609379,
        "amount": "6,530.54",
        "discount_percent": 12,
        "delivery_method_name": "",
        "discount_cost": "0",
        "total": "6,536.55",
        "delivery_date": null,
        "delivery_address": {
            "id": 222,
            "first_name": "Danielle",
            "last_name": "Stehr",
            "address": "3567 Moses Dale\nEast Erick, PA 32038",
            "address_secondary": "736 Maybelle Walk Apt. 789\nKeelingmouth, IN 29374",
            "phone": "743.930.2337",
            "additional_phone": "258.224.7310 x518",
            "company": "Jast-Herzog",
            "alias": "Дом",
            "custom_fields": null
        },
        "payment_method": null,
        "status": 1,
        "cancel_reason_id": null,
        "created": "09:51",
        "updated": "09:51"
    }
]
```
<div id="execution-results-GETapi-couriers-orders" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-GETapi-couriers-orders"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-couriers-orders"></code></pre>
</div>
<div id="execution-error-GETapi-couriers-orders" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-GETapi-couriers-orders"></code></pre>
</div>
<form id="form-GETapi-couriers-orders" data-method="GET" data-path="api/couriers/orders" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-couriers-orders', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-couriers-orders" onclick="tryItOut('GETapi-couriers-orders');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-couriers-orders" onclick="cancelTryOut('GETapi-couriers-orders');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-couriers-orders" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/couriers/orders</code></b>
</p>
</form>

### Ответ
<h4 class="fancy-heading-panel"><b>Поля ответа</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<br>
ID Заказа</p>
<p>
<b><code>first_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Имя клиента</p>
<p>
<b><code>last_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Фамилия клиента</p>
<p>
<b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Номер телефона</p>
<p>
<b><code>note</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Заметка по заказу</p>
<p>
<b><code>order_no</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<br>
Номер заказа</p>
<p>
<b><code>amount</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Сумма заказа</p>
<p>
<b><code>discount_percent</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<br>
Скидка в процентах</p>
<p>
<b><code>delivery_method_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Название доставки</p>
<p>
<b><code>discount_cost</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Сумма скидки</p>
<p>
<b><code>total</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Общая сумма заказа с доставкой</p>
<p>
<b><code>delivery_date</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Дата доставки заказа</p>
<p>
<b><code>store</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Магазин</p>
<p>
<b><code>store.id</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Магазин: ID</p>
<p>
<b><code>store.name</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Магазин: Название</p>
<p>
<b><code>store.address</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Магазин: Адрес</p>
<p>
<b><code>store.logo</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Магазин: Логотип</p>
<p>
<b><code>store.lat</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Магазин: Широта на карте</p>
<p>
<b><code>store.lng</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Магазин: Долгота на карте</p>

## История доставок.




> Пример запроса:

```bash
curl -X GET \
    -G "salom.crm.loc/api/couriers/orders/history" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/couriers/orders/history"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 173,
        "first_name": "Maureen",
        "last_name": "Boyle",
        "phone": "506.523.8606 x1006",
        "note": "Necessitatibus dolores ad quia.",
        "order_no": 795247,
        "amount": "10,998.10",
        "discount_percent": 9,
        "delivery_method_name": "",
        "discount_cost": "0",
        "total": "11,004.78",
        "delivery_date": null,
        "delivery_address": {
            "id": 223,
            "first_name": "Elinor",
            "last_name": "Price",
            "address": "3393 Boyer Centers\nWest Rosalynland, TN 27177",
            "address_secondary": "73196 Jeffery Mountains Apt. 471\nWehnermouth, DE 65903",
            "phone": "(753) 804-0413",
            "additional_phone": "(923) 541-4958",
            "company": "McGlynn LLC",
            "alias": "Дом",
            "custom_fields": null
        },
        "payment_method": null,
        "status": 2,
        "cancel_reason_id": null,
        "created": "09:51",
        "updated": "09:51"
    },
    {
        "id": 174,
        "first_name": "Garnett",
        "last_name": "Glover",
        "phone": "(485) 867-0346",
        "note": "Pariatur sapiente quasi minus quia architecto. Minima quidem sit accusamus iure.",
        "order_no": 356558,
        "amount": "19,148.37",
        "discount_percent": 10,
        "delivery_method_name": "",
        "discount_cost": "0",
        "total": "19,160.64",
        "delivery_date": null,
        "delivery_address": {
            "id": 224,
            "first_name": "Maci",
            "last_name": "Emmerich",
            "address": "3300 Prohaska Pike Suite 414\nReynoldsville, AL 29839",
            "address_secondary": "15230 Arlie Junction Apt. 932\nKatarinafort, KY 32451",
            "phone": "213.279.7355 x051",
            "additional_phone": "(472) 626-1168 x758",
            "company": "Mayert-Borer",
            "alias": "Дом",
            "custom_fields": null
        },
        "payment_method": null,
        "status": 3,
        "cancel_reason_id": null,
        "created": "09:51",
        "updated": "09:51"
    }
]
```
<div id="execution-results-GETapi-couriers-orders-history" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-GETapi-couriers-orders-history"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-couriers-orders-history"></code></pre>
</div>
<div id="execution-error-GETapi-couriers-orders-history" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-GETapi-couriers-orders-history"></code></pre>
</div>
<form id="form-GETapi-couriers-orders-history" data-method="GET" data-path="api/couriers/orders/history" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-couriers-orders-history', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-couriers-orders-history" onclick="tryItOut('GETapi-couriers-orders-history');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-couriers-orders-history" onclick="cancelTryOut('GETapi-couriers-orders-history');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-couriers-orders-history" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/couriers/orders/history</code></b>
</p>
</form>


## Взять заказ.




> Пример запроса:

```bash
curl -X PUT \
    "salom.crm.loc/api/couriers/orders/17/take" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/couriers/orders/17/take"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json

{
 'message': 'Вы назначены на заказ #номерЗаказа'
}
```
<div id="execution-results-PUTapi-couriers-orders--id--take" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-PUTapi-couriers-orders--id--take"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-couriers-orders--id--take"></code></pre>
</div>
<div id="execution-error-PUTapi-couriers-orders--id--take" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-PUTapi-couriers-orders--id--take"></code></pre>
</div>
<form id="form-PUTapi-couriers-orders--id--take" data-method="PUT" data-path="api/couriers/orders/{id}/take" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-couriers-orders--id--take', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-couriers-orders--id--take" onclick="tryItOut('PUTapi-couriers-orders--id--take');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-couriers-orders--id--take" onclick="cancelTryOut('PUTapi-couriers-orders--id--take');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-couriers-orders--id--take" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/couriers/orders/{id}/take</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры URL</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="id" data-endpoint="PUTapi-couriers-orders--id--take" data-component="url" required  hidden>
<br>
ID Заказа</p>
</form>



