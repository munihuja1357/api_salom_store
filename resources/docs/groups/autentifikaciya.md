# Аутентификация

API для аутентификации в системе

## Вход в систему.




> Пример запроса:

```bash
curl -X POST \
    "salom.crm.loc/api/auth/login" \
    -H "client_id: client_secret_id" \
    -H "client_secret: client_secret" \
    -H "Content-Type: application/json" \
    -d '{"email":"farrell.charley@example.net","password":"password"}'

```

```javascript
const url = new URL(
    "salom.crm.loc/api/auth/login"
);

let headers = {
    "client_id": "client_secret_id",
    "client_secret": "client_secret",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "farrell.charley@example.net",
    "password": "password"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (200):

```json

{
 "access_token": "MTQ0NjJkZmQ5OTM2NDE1ZTZjNGZmZjI3",
 "expires_in": 3600,
 return "message"
}
```
<div id="execution-results-POSTapi-auth-login" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-POSTapi-auth-login"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-login"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-login" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-login"></code></pre>
</div>
<form id="form-POSTapi-auth-login" data-method="POST" data-path="api/auth/login" data-authed="0" data-hasfiles="0" data-headers='{"client_id":"client_secret_id","client_secret":"client_secret","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-login', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-login" onclick="tryItOut('POSTapi-auth-login');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-login" onclick="cancelTryOut('POSTapi-auth-login');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-login" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/login</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры тела запроса</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-auth-login" data-component="body" required  hidden>
<br>
Поле value должно быть действительным электронным адресом.</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="password" data-endpoint="POSTapi-auth-login" data-component="body" required  hidden>
<br>
Должен содержать от 8 до 20 символов.</p>

</form>

### Ответ
<h4 class="fancy-heading-panel"><b>Поля ответа</b></h4>
<p>
<b><code>access_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Токен доступа</p>
<p>
<b><code>expiresIn</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<br>
Срок действия access_token</p>

## Обновить Access Token.




> Пример запроса:

```bash
curl -X POST \
    "salom.crm.loc/api/auth/refresh-token" \
    -H "client_id: 1" \
    -H "client_secret: secret"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/auth/refresh-token"
);

let headers = {
    "client_id": "1",
    "client_secret": "secret",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTapi-auth-refresh-token" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-POSTapi-auth-refresh-token"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-refresh-token"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-refresh-token" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-refresh-token"></code></pre>
</div>
<form id="form-POSTapi-auth-refresh-token" data-method="POST" data-path="api/auth/refresh-token" data-authed="0" data-hasfiles="0" data-headers='{"client_id":"1","client_secret":"secret"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-refresh-token', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-refresh-token" onclick="tryItOut('POSTapi-auth-refresh-token');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-refresh-token" onclick="cancelTryOut('POSTapi-auth-refresh-token');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-refresh-token" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/refresh-token</code></b>
</p>
</form>


## Выход из системы.




> Пример запроса:

```bash
curl -X POST \
    "salom.crm.loc/api/auth/logout" \
    -H "client_id: client_secret_id" \
    -H "client_secret: client_secret"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/auth/logout"
);

let headers = {
    "client_id": "client_secret_id",
    "client_secret": "client_secret",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTapi-auth-logout" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-POSTapi-auth-logout"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-logout"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-logout" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-logout"></code></pre>
</div>
<form id="form-POSTapi-auth-logout" data-method="POST" data-path="api/auth/logout" data-authed="0" data-hasfiles="0" data-headers='{"client_id":"client_secret_id","client_secret":"client_secret"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-logout', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-logout" onclick="tryItOut('POSTapi-auth-logout');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-logout" onclick="cancelTryOut('POSTapi-auth-logout');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-logout" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/logout</code></b>
</p>
</form>



