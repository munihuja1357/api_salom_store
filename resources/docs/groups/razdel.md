# Раздел.


## Список активных раздел.




> Пример запроса:

```bash
curl -X GET \
    -G "salom.crm.loc/api/sections" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/sections"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 1,
        "name": "Test",
        "icon": "",
        "parent_id": 0
    },
    {
        "id": 1,
        "name": "Test",
        "icon": "",
        "parent_id": 0
    }
]
```
<div id="execution-results-GETapi-sections" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-GETapi-sections"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-sections"></code></pre>
</div>
<div id="execution-error-GETapi-sections" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-GETapi-sections"></code></pre>
</div>
<form id="form-GETapi-sections" data-method="GET" data-path="api/sections" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-sections', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-sections" onclick="tryItOut('GETapi-sections');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-sections" onclick="cancelTryOut('GETapi-sections');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-sections" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/sections</code></b>
</p>
</form>

### Ответ
<h4 class="fancy-heading-panel"><b>Поля ответа</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<br>
ID</p>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Название раздела.</p>
<p>
<b><code>icon</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Иконка раздела.</p>
<p>
<b><code>stores[]</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Магазины которые входят в раздел.</p>
<p>
<b><code>parent_id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<br>
ID родителя раздела.</p>

## Получить раздел по ID.




> Пример запроса:

```bash
curl -X GET \
    -G "salom.crm.loc/api/sections/non" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/sections/non"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 1,
        "name": "Test",
        "icon": "",
        "parent_id": 0
    },
    {
        "id": 1,
        "name": "Test",
        "icon": "",
        "parent_id": 0
    }
]
```
<div id="execution-results-GETapi-sections--id-" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-GETapi-sections--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-sections--id-"></code></pre>
</div>
<div id="execution-error-GETapi-sections--id-" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-GETapi-sections--id-"></code></pre>
</div>
<form id="form-GETapi-sections--id-" data-method="GET" data-path="api/sections/{id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-sections--id-', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-sections--id-" onclick="tryItOut('GETapi-sections--id-');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-sections--id-" onclick="cancelTryOut('GETapi-sections--id-');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-sections--id-" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/sections/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры URL</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="GETapi-sections--id-" data-component="url" required  hidden>
<br>
ID раздела.</p>
</form>

### Ответ
<h4 class="fancy-heading-panel"><b>Поля ответа</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<br>
ID</p>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Название раздела.</p>
<p>
<b><code>icon</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Иконка раздела.</p>
<p>
<b><code>stores[]</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Магазины которые входят в раздел.</p>
<p>
<b><code>parent_id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<br>
ID родителя раздела.</p>


