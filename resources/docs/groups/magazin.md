# Магазин


## Display a listing of the resource.




> Пример запроса:

```bash
curl -X GET \
    -G "salom.crm.loc/api/store/orders" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/store/orders"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500):

```json
{
    "message": "SQLSTATE[42S02]: Base table or view not found: 1146 Table 'firm_salom98523.domains' doesn't exist (SQL: select * from `domains` where `domain` = localhost limit 1)",
    "exception": "Illuminate\\Database\\QueryException",
    "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
    "line": 671,
    "trace": [
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
            "line": 631,
            "function": "runQueryCallback",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
            "line": 339,
            "function": "run",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2270,
            "function": "select",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2258,
            "function": "runSelect",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2753,
            "function": "Illuminate\\Database\\Query\\{closure}",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2259,
            "function": "onceWithColumns",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Eloquent\/Builder.php",
            "line": 548,
            "function": "get",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Eloquent\/Builder.php",
            "line": 532,
            "function": "getModels",
            "class": "Illuminate\\Database\\Eloquent\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Concerns\/BuildsQueries.php",
            "line": 143,
            "function": "get",
            "class": "Illuminate\\Database\\Eloquent\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Resolvers\/DomainTenantResolver.php",
            "line": 32,
            "function": "first",
            "class": "Illuminate\\Database\\Eloquent\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Resolvers\/Contracts\/CachedTenantResolver.php",
            "line": 34,
            "function": "resolveWithoutCache",
            "class": "Stancl\\Tenancy\\Resolvers\\DomainTenantResolver",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Middleware\/IdentificationMiddleware.php",
            "line": 26,
            "function": "resolve",
            "class": "Stancl\\Tenancy\\Resolvers\\Contracts\\CachedTenantResolver",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Middleware\/InitializeTenancyByDomain.php",
            "line": 38,
            "function": "initializeTenancy",
            "class": "Stancl\\Tenancy\\Middleware\\IdentificationMiddleware",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Stancl\\Tenancy\\Middleware\\InitializeTenancyByDomain",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 687,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 662,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 628,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 617,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 165,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 60,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 63,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 140,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 109,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
            "line": 37,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 596,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 134,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Concerns\/CallsCommands.php",
            "line": 56,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Concerns\/CallsCommands.php",
            "line": 28,
            "function": "runCommand",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Commands\/Run.php",
            "line": 55,
            "function": "call",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Tenancy.php",
            "line": 160,
            "function": "Stancl\\Tenancy\\Commands\\{closure}",
            "class": "Stancl\\Tenancy\\Commands\\Run",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Commands\/Run.php",
            "line": 56,
            "function": "runForMultiple",
            "class": "Stancl\\Tenancy\\Tenancy",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Stancl\\Tenancy\\Commands\\Run",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
            "line": 37,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 596,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 134,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/symfony\/console\/Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/symfony\/console\/Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/symfony\/console\/Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 93,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-GETapi-store-orders" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-GETapi-store-orders"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-store-orders"></code></pre>
</div>
<div id="execution-error-GETapi-store-orders" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-GETapi-store-orders"></code></pre>
</div>
<form id="form-GETapi-store-orders" data-method="GET" data-path="api/store/orders" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-store-orders', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-store-orders" onclick="tryItOut('GETapi-store-orders');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-store-orders" onclick="cancelTryOut('GETapi-store-orders');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-store-orders" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/store/orders</code></b>
</p>
</form>


## Получить заказ по ID.




> Пример запроса:

```bash
curl -X GET \
    -G "salom.crm.loc/api/store/orders/eum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/store/orders/eum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 175,
        "first_name": "Leora",
        "last_name": "Buckridge",
        "phone": "1-445-903-5945 x0109",
        "note": "Aliquam ut rerum rerum doloribus ea. Quibusdam amet vel sapiente. Sit molestiae ad autem aut consequatur.",
        "order_no": 985881,
        "amount": "8,020.41",
        "discount_percent": 16,
        "delivery_method_name": "",
        "discount_cost": "0",
        "total": "8,029.29",
        "delivery_date": null,
        "delivery_address": {
            "id": 225,
            "first_name": "Zackary",
            "last_name": "Simonis",
            "address": "802 Orlo Estate Apt. 606\nEast Demarioberg, NC 45506",
            "address_secondary": "593 Blaise Plains Suite 827\nSouth Shadberg, ND 07786",
            "phone": "1-670-949-3708 x16249",
            "additional_phone": "(304) 991-4289 x2477",
            "company": "Doyle-Nitzsche",
            "alias": "Офис",
            "custom_fields": null
        },
        "payment_method": null,
        "status": 2,
        "cancel_reason_id": null,
        "created": "09:51",
        "updated": "09:51"
    },
    {
        "id": 176,
        "first_name": "Javonte",
        "last_name": "Mayert",
        "phone": "(276) 914-0998",
        "note": "Ipsa autem quam amet ut. Dolor sit quibusdam aliquam hic itaque.",
        "order_no": 102578,
        "amount": "4,778.42",
        "discount_percent": 12,
        "delivery_method_name": "",
        "discount_cost": "0",
        "total": "4,788.82",
        "delivery_date": null,
        "delivery_address": {
            "id": 226,
            "first_name": "Nathan",
            "last_name": "Torp",
            "address": "391 Marietta Burg Apt. 016\nSchuppeport, IN 99541",
            "address_secondary": "476 Viva Lock\nPort Eldred, PA 40082-4579",
            "phone": "+1 (912) 245-6806",
            "additional_phone": "(798) 255-7516",
            "company": "Bogan-Schamberger",
            "alias": "Дом",
            "custom_fields": null
        },
        "payment_method": null,
        "status": 2,
        "cancel_reason_id": null,
        "created": "09:51",
        "updated": "09:51"
    }
]
```
<div id="execution-results-GETapi-store-orders--id-" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-GETapi-store-orders--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-store-orders--id-"></code></pre>
</div>
<div id="execution-error-GETapi-store-orders--id-" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-GETapi-store-orders--id-"></code></pre>
</div>
<form id="form-GETapi-store-orders--id-" data-method="GET" data-path="api/store/orders/{id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-store-orders--id-', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-store-orders--id-" onclick="tryItOut('GETapi-store-orders--id-');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-store-orders--id-" onclick="cancelTryOut('GETapi-store-orders--id-');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-store-orders--id-" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/store/orders/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры URL</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="GETapi-store-orders--id-" data-component="url" required  hidden>
<br>
</p>
</form>


## Подтверждение заказа.


Подтверждение наличие пунктов заказа у магазина.

> Пример запроса:

```bash
curl -X PUT \
    "salom.crm.loc/api/store/orders/14/status" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"state":"2"}'

```

```javascript
const url = new URL(
    "salom.crm.loc/api/store/orders/14/status"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "state": "2"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-PUTapi-store-orders--id--status" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-PUTapi-store-orders--id--status"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-store-orders--id--status"></code></pre>
</div>
<div id="execution-error-PUTapi-store-orders--id--status" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-PUTapi-store-orders--id--status"></code></pre>
</div>
<form id="form-PUTapi-store-orders--id--status" data-method="PUT" data-path="api/store/orders/{id}/status" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-store-orders--id--status', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-store-orders--id--status" onclick="tryItOut('PUTapi-store-orders--id--status');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-store-orders--id--status" onclick="cancelTryOut('PUTapi-store-orders--id--status');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-store-orders--id--status" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/store/orders/{id}/status</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры URL</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="id" data-endpoint="PUTapi-store-orders--id--status" data-component="url"  hidden>
<br>
ID заказа</p>
<h4 class="fancy-heading-panel"><b>Параметры тела запроса</b></h4>
<p>
<b><code>state</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="state" data-endpoint="PUTapi-store-orders--id--status" data-component="body" required  hidden>
<br>
The value must be one of <code>1</code>, <code>2</code>, <code>3</code>, or <code>4</code>.</p>

</form>


## Display a listing of the resource.




> Пример запроса:

```bash
curl -X GET \
    -G "salom.crm.loc/api/orders" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/orders"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500):

```json
{
    "message": "SQLSTATE[42S02]: Base table or view not found: 1146 Table 'firm_salom98523.domains' doesn't exist (SQL: select * from `domains` where `domain` = localhost limit 1)",
    "exception": "Illuminate\\Database\\QueryException",
    "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
    "line": 671,
    "trace": [
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
            "line": 631,
            "function": "runQueryCallback",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
            "line": 339,
            "function": "run",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2270,
            "function": "select",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2258,
            "function": "runSelect",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2753,
            "function": "Illuminate\\Database\\Query\\{closure}",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2259,
            "function": "onceWithColumns",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Eloquent\/Builder.php",
            "line": 548,
            "function": "get",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Eloquent\/Builder.php",
            "line": 532,
            "function": "getModels",
            "class": "Illuminate\\Database\\Eloquent\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Concerns\/BuildsQueries.php",
            "line": 143,
            "function": "get",
            "class": "Illuminate\\Database\\Eloquent\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Resolvers\/DomainTenantResolver.php",
            "line": 32,
            "function": "first",
            "class": "Illuminate\\Database\\Eloquent\\Builder",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Resolvers\/Contracts\/CachedTenantResolver.php",
            "line": 34,
            "function": "resolveWithoutCache",
            "class": "Stancl\\Tenancy\\Resolvers\\DomainTenantResolver",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Middleware\/IdentificationMiddleware.php",
            "line": 26,
            "function": "resolve",
            "class": "Stancl\\Tenancy\\Resolvers\\Contracts\\CachedTenantResolver",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Middleware\/InitializeTenancyByDomain.php",
            "line": 38,
            "function": "initializeTenancy",
            "class": "Stancl\\Tenancy\\Middleware\\IdentificationMiddleware",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Stancl\\Tenancy\\Middleware\\InitializeTenancyByDomain",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 687,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 662,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 628,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 617,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 165,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 60,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 63,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 140,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 109,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
            "line": 37,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 596,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 134,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Concerns\/CallsCommands.php",
            "line": 56,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Concerns\/CallsCommands.php",
            "line": 28,
            "function": "runCommand",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Commands\/Run.php",
            "line": 55,
            "function": "call",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Tenancy.php",
            "line": 160,
            "function": "Stancl\\Tenancy\\Commands\\{closure}",
            "class": "Stancl\\Tenancy\\Commands\\Run",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/stancl\/tenancy\/src\/Commands\/Run.php",
            "line": 56,
            "function": "runForMultiple",
            "class": "Stancl\\Tenancy\\Tenancy",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Stancl\\Tenancy\\Commands\\Run",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
            "line": 37,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 596,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 134,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/symfony\/console\/Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/symfony\/console\/Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/symfony\/console\/Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 93,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/goodmartian\/Projects\/crm\/backend\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-GETapi-orders" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-GETapi-orders"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-orders"></code></pre>
</div>
<div id="execution-error-GETapi-orders" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-GETapi-orders"></code></pre>
</div>
<form id="form-GETapi-orders" data-method="GET" data-path="api/orders" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-orders', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-orders" onclick="tryItOut('GETapi-orders');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-orders" onclick="cancelTryOut('GETapi-orders');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-orders" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/orders</code></b>
</p>
</form>


## Получить заказ по ID.




> Пример запроса:

```bash
curl -X GET \
    -G "salom.crm.loc/api/orders/voluptatem" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "salom.crm.loc/api/orders/voluptatem"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 177,
        "first_name": "Chauncey",
        "last_name": "Schiller",
        "phone": "+19932787957",
        "note": "Quasi aut inventore itaque nihil qui doloremque.",
        "order_no": 914083,
        "amount": "3,223.14",
        "discount_percent": 18,
        "delivery_method_name": "",
        "discount_cost": "0",
        "total": "3,228.31",
        "delivery_date": null,
        "delivery_address": {
            "id": 227,
            "first_name": "Elizabeth",
            "last_name": "Borer",
            "address": "1442 Clark Shore\nNorth Cassidychester, WA 41870",
            "address_secondary": "892 Orville Fort\nProsaccoborough, VA 46932-0330",
            "phone": "+1-731-278-7598",
            "additional_phone": "948.343.1673 x26032",
            "company": "Metz, Fisher and McDermott",
            "alias": "Квартира",
            "custom_fields": null
        },
        "payment_method": null,
        "status": 4,
        "cancel_reason_id": null,
        "created": "09:51",
        "updated": "09:51"
    },
    {
        "id": 178,
        "first_name": "Mariam",
        "last_name": "Grady",
        "phone": "539-530-0652",
        "note": "Voluptatem fugit sunt neque. Suscipit assumenda voluptatem ut occaecati suscipit.",
        "order_no": 989419,
        "amount": "8,761.72",
        "discount_percent": 20,
        "delivery_method_name": "",
        "discount_cost": "0",
        "total": "8,767.98",
        "delivery_date": null,
        "delivery_address": {
            "id": 228,
            "first_name": "Freddie",
            "last_name": "Purdy",
            "address": "630 Conn Gardens Apt. 093\nPhilipbury, VA 20573",
            "address_secondary": "7722 Maida Station\nPort Andersonborough, AR 46266",
            "phone": "792.833.9283 x1514",
            "additional_phone": "1-710-288-0814",
            "company": "Franecki, Altenwerth and Runolfsdottir",
            "alias": "Офис",
            "custom_fields": null
        },
        "payment_method": null,
        "status": 4,
        "cancel_reason_id": null,
        "created": "09:51",
        "updated": "09:51"
    }
]
```
<div id="execution-results-GETapi-orders--id-" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-GETapi-orders--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-orders--id-"></code></pre>
</div>
<div id="execution-error-GETapi-orders--id-" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-GETapi-orders--id-"></code></pre>
</div>
<form id="form-GETapi-orders--id-" data-method="GET" data-path="api/orders/{id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-orders--id-', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-orders--id-" onclick="tryItOut('GETapi-orders--id-');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-orders--id-" onclick="cancelTryOut('GETapi-orders--id-');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-orders--id-" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/orders/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры URL</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="GETapi-orders--id-" data-component="url" required  hidden>
<br>
</p>
</form>


## Подтверждение заказа.


Подтверждение наличие пунктов заказа у магазина.

> Пример запроса:

```bash
curl -X PUT \
    "salom.crm.loc/api/orders/19/status" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"state":"4"}'

```

```javascript
const url = new URL(
    "salom.crm.loc/api/orders/19/status"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "state": "4"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-PUTapi-orders--id--status" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-PUTapi-orders--id--status"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-orders--id--status"></code></pre>
</div>
<div id="execution-error-PUTapi-orders--id--status" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-PUTapi-orders--id--status"></code></pre>
</div>
<form id="form-PUTapi-orders--id--status" data-method="PUT" data-path="api/orders/{id}/status" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-orders--id--status', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-orders--id--status" onclick="tryItOut('PUTapi-orders--id--status');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-orders--id--status" onclick="cancelTryOut('PUTapi-orders--id--status');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-orders--id--status" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/orders/{id}/status</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры URL</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="id" data-endpoint="PUTapi-orders--id--status" data-component="url"  hidden>
<br>
ID заказа</p>
<h4 class="fancy-heading-panel"><b>Параметры тела запроса</b></h4>
<p>
<b><code>state</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="state" data-endpoint="PUTapi-orders--id--status" data-component="body" required  hidden>
<br>
The value must be one of <code>1</code>, <code>2</code>, <code>3</code>, or <code>4</code>.</p>

</form>


## Назначение курьера.




> Пример запроса:

```bash
curl -X PUT \
    "salom.crm.loc/api/orders/16/courier/assign" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"courier_id":7}'

```

```javascript
const url = new URL(
    "salom.crm.loc/api/orders/16/courier/assign"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "courier_id": 7
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (200):

```json

{
 'message': 'Курьер назначен на заказ #номерЗаказа'
}
```
<div id="execution-results-PUTapi-orders--id--courier-assign" hidden>
    <blockquote>Полученный ответ<span id="execution-response-status-PUTapi-orders--id--courier-assign"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-orders--id--courier-assign"></code></pre>
</div>
<div id="execution-error-PUTapi-orders--id--courier-assign" hidden>
    <blockquote>Запрос завершился ошибкой:</blockquote>
    <pre><code id="execution-error-message-PUTapi-orders--id--courier-assign"></code></pre>
</div>
<form id="form-PUTapi-orders--id--courier-assign" data-method="PUT" data-path="api/orders/{id}/courier/assign" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-orders--id--courier-assign', this);">
<h3>
    Запрос&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-orders--id--courier-assign" onclick="tryItOut('PUTapi-orders--id--courier-assign');">Попробуйте ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-orders--id--courier-assign" onclick="cancelTryOut('PUTapi-orders--id--courier-assign');" hidden>Отменить</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-orders--id--courier-assign" hidden>Отправить запрос 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/orders/{id}/courier/assign</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Параметры URL</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="id" data-endpoint="PUTapi-orders--id--courier-assign" data-component="url" required  hidden>
<br>
ID Заказа</p>
<h4 class="fancy-heading-panel"><b>Параметры тела запроса</b></h4>
<p>
<b><code>courier_id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="courier_id" data-endpoint="PUTapi-orders--id--courier-assign" data-component="body" required  hidden>
<br>
ID Курьера</p>

</form>



