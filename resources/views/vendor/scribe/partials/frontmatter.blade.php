title: {{ $settings['title'] }}

language_tabs:
@foreach($settings['languages'] as $language)
- {{ $language }}
@endforeach

includes:
- "./prepend.md"
- "./authentication.md"
- "./groups/*"
- "./errors.md"
- "./append.md"

logo: {{ $settings['logo'] ?? false }}

toc_footers:
@if($showPostmanCollectionButton)
- <a href="{{ $postmanCollectionLink }}">Посмотреть коллекцию Postman</a>
@endif
@if($showOpenAPISpecButton)
- <a href="{{ $openAPISpecLink }}">Просмотреть спецификацию OpenAPI (Swagger)</a>
@endif
