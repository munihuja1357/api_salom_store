# Проверка подлинности запросов

@if(!$isAuthed)
Этот API не аутентифицирован.
@else
{!! $authDescription !!}

{!! $extraAuthInfo !!}
@endif
