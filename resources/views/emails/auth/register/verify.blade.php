@component('mail::message')
# Email Confirmation

Please refer to the following link:

@component('mail::button', ['url' => route('api.register.verify', ['token' => $tenant->verify_token])])
Verify Email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
