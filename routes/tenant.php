<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;
use App\Http\Controllers\Api\v1\Firm\Store\Courier\CourierController;

/*
|--------------------------------------------------------------------------
| Firm Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::group([
    'as' => 'api.',
    'prefix' => 'api/firm',
    'namespace' => 'Api\v1',
    'middleware' => [
        InitializeTenancyByDomain::class,
        PreventAccessFromCentralDomains::class,
    ],
], function () {
    Route::group([
        'prefix' => 'sections',
    ], function () {
        Route::get('', 'Firm\Store\SectionController@index');
        Route::get('{section_id}/stores', 'Firm\Store\SectionController@stores');
    });

    Route::group([
        'namespace' => 'Firm\Auth',
        'prefix' => 'auth',
    ], function () {
        Route::post('login', 'LoginController@login')->name('login');

        Route::group([
            'middleware' => 'auth:api-firm-user',
        ], function () {
            Route::post('refresh-token', 'LoginController@refreshToken')->name('refreshToken');
            Route::post('logout', 'LogoutController@logout')->name('logout');
        });
    });

    Route::group([
        'prefix' => 'customers',
        'namespace' => 'Firm\Customer',
    ], function () {
        Route::group([
            'namespace' => 'Auth',
            'prefix' => 'auth',
        ], function () {
            Route::post('register', 'RegisterController')->name('register');
            Route::post('verify/sms', 'VerifyPhoneNumberController')->name('sms_verify');
            Route::post('login', 'LoginController@login')->name('login');

            Route::group([
                'middleware' => 'auth:api-customers',
            ], function () {
                Route::post('refresh-token', 'LoginController@refreshToken')->name('refreshToken');
                Route::post('logout', 'LogoutController@logout')->name('logout');
            });
        });
    });

    /*Route::group([
        'namespace' => 'Firm\Store',
        'middleware' => 'auth:api-firm-user',
    ], function ($store) {
        $store->group(['prefix' => 'orders', 'namespace' => 'Order'], function ($orders) {
            $orders->resource('/', 'OrderController');
            $orders->put('{id}/take', 'OrderController@take');
        });

        $store->group(['prefix' => 'couriers'], function ($couriers) {
            $couriers->get('profile/info', [CourierController::class, 'getProfileInfo']);
            $couriers->put('status', [CourierController::class, 'toggleStatus']);
        });


    });*/
});
