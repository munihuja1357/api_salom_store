<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*Auth::routes(['verify' => true]); // for verification email*/
/*
Route::group(['as' => 'api.', 'namespace' => 'Api\v1'],
    function () {

        Route::group([
            'middleware' => 'auth:api-tenants'
        ], function () {
            Route::group([
                'prefix' => 'auth',
                'namespace' => 'Auth',
            ], function ()
            {
                Route::post('logout', 'LogoutController@logout')->name('logout');
                Route::post('refresh-token', 'LoginController@refreshToken')->name('refreshToken');
            });

            Route::group([
                'prefix' => 'firms',
                'namespace' => 'Firm'
            ], function () {
                Route::post('', 'FirmController@store');
            });
        });

        Route::group([
            'namespace' => 'Auth',
            'prefix' => 'auth',
            'as' => 'register.'
        ], function () {
                Route::post('login', 'LoginController@login');
                Route::post('register', 'RegisterController@register');
                Route::get('verify/{token}', 'RegisterController@verify')->name('verify');
        });
});*/
