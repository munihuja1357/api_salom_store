<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\Firm\Store;
use App\Http\Controllers\Api\v1\Firm\Customer;
use App\Http\Controllers\Api\v1\Firm\Store\Order;
use App\Http\Controllers\Api\v1\Firm\Store\Courier;
use App\Http\Controllers\Api\v1\Firm\Auth\AuthController;
use App\Http\Controllers\Api\v1\Firm\Store\Courier\CourierController;
use App\Http\Controllers\Api\v1\Firm\UserController;

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');

    /*Route::group([
        'middleware' => 'auth:api-firm-user',
    ], function () {
        Route::post('refresh-token', [AuthController::class, 'refreshToken'])->name('refreshToken');
        Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    });*/
});

Route::group([
    'middleware' => [
        'auth:api-firm-user,firm-user',
    ],
], function () {

    Route::group([
        'as' => 'couriers.',
        'prefix' => 'couriers'
    ], function () {
        Route::get('/', [CourierController::class, 'index'])->name('index');
        Route::get('{id}', [CourierController::class, 'show'])->name('show');
        Route::get('profile', [CourierController::class, 'getProfile']);
        Route::put('status', [CourierController::class, 'toggleStatus']);
        Route::get('accounts', [CourierController::class, 'getAccounts']);

        Route::group([
            'as' => 'orders.',
            'prefix' => 'orders'
        ], function () {
            Route::get('/', [Courier\OrderController::class, 'index']);
            Route::get('history', [Courier\OrderController::class, 'getHistory']);
            Route::put('{id}/take', [Courier\OrderController::class, 'take'])->where('id','[0-9]+');
            Route::put('{id}/photo_check_order', [Courier\OrderController::class, 'photoCheckOrder'])->where('id','[0-9]+');
            Route::get('{id}/payment_true', [Courier\OrderController::class, 'paymentTrue'])->where('id','[0-9]+');
            Route::get('{id}/items_order', [Courier\OrderController::class, 'orderItems'])->where('id','[0-9]+');
        });

        Route::get('{id}/orders/uncompleted', [CourierController::class, 'getUncompletedOrders'])->name('orders.uncompleted');

    });

    Route::group([
        'as' => 'store.',
        'prefix' => 'store',
    ], function () {
        Route::group([
            'as' => 'orders.',
            'prefix' => 'orders',
        ], function () {
            Route::get('/', [Order\OrderController::class, 'index'])->name('index');
            Route::get('{id}', [Order\OrderController::class, 'show'])->name('show');
            Route::put('{id}/status', [Order\OrderController::class, 'changeStatus'])->name('changeStatus');
        });
    });

    Route::group([
        'as' => 'orders.',
        'prefix' => 'orders',
    ], function () {
        Route::get('/', [Order\OrderController::class, 'index'])->name('index');
        Route::get('{id}', [Order\OrderController::class, 'show'])->name('show');
        Route::put('{id}/status', [Order\OrderController::class, 'changeStatus'])->name('changeStatus');
        Route::put('{id}/courier/assign', [Order\OrderController::class, 'assignCourier'])->name('assign_courier');
    });

    Route::post('price-list/status', [Store\PriceListController::class, 'updateStatuses'])->name('price_list.update_statuses');
    Route::get('products', [Store\ProductController::class, 'index'])->name('products.index');

    Route::get('users', [UserController::class, 'index'])->name('users.list');
});

Route::post('customers/auth/verify', Customer\Auth\VerifyPhoneNumberController::class)->name('verify');
Route::post('customers/auth/login', [Customer\Auth\LoginController::class, 'login'])->name('login');

Route::group([
    'as' => 'customers.',
    'prefix' => 'customers',
    'middleware' => 'auth:api-customers',
], function () {
    Route::prefix('auth')->group(function () {
        Route::post('refresh-token', [Customer\Auth\LoginController::class, 'refreshToken'])->name('refreshToken');
        Route::post('logout', Customer\Auth\LoginController::class)->name('logout');
    });

    Route::get('profile', [Customer\ProfileController::class, 'show']);
    Route::post('profile/update', [Customer\ProfileController::class, 'update']);
    Route::post('location', [Customer\ProfileController::class, 'storeLatLng']);
    Route::get('delivery-methods', Customer\DeliveryMethodController::class);
    Route::get('payment-methods', Customer\PaymentMethodController::class);

    Route::group(['prefix' => 'cart'], function ($cart) {
        $cart->get('/', [Customer\CartController::class,'index']);
        $cart->get('/show/{id}', [Customer\CartController::class,'show'])->where('id','[0-9]+');
        $cart->post('/store', [Customer\CartController::class,'store']);
        $cart->post('/update/{id}', [Customer\CartController::class,'update'])->where('id','[0-9]+');
        $cart->post('/update-template/{id}', [Customer\CartController::class,'update_template'])->where('id','[0-9]+');
        $cart->get('/delete/{id}', [Customer\CartController::class,'delete'])->where('id','[0-9]+');

        $cart->post('/store-item', [Customer\CartItemController::class,'store']);
        $cart->post('/update-item/{id}', [Customer\CartItemController::class,'update'])->where('id','[0-9]+');
        $cart->get('/delete-item/{id}', [Customer\CartItemController::class,'delete'])->where('id','[0-9]+');
    });

    Route::group(['prefix' => 'chat'], function ($chat) {
        $chat->get('/', [Customer\ChatController::class,'index']);
        $chat->get('/show/{id}', [Customer\ChatController::class,'show'])->where('id','[0-9]+');
        $chat->post('/store', [Customer\ChatController::class,'store']);
        $chat->get('/delete/{id}', [Customer\ChatController::class,'delete'])->where('id','[0-9]+');

        $chat->post('/message/store', [Customer\MessageController::class,'store']);
        $chat->post('/message/update/{id}', [Customer\MessageController::class,'update'])->where('id','[0-9]+');
        $chat->post('/message/update-is-seen/{id}', [Customer\MessageController::class,'updateIsSeen'])->where('id','[0-9]+');
        $chat->get('/message/delete/{id}', [Customer\MessageController::class,'delete'])->where('id','[0-9]+');
    });

    Route::group(['prefix' => 'order'], function ($order) {
        $order->get('/', [Customer\OrderController::class,'index']);
        $order->get('{id}', [Customer\OrderController::class,'show'])->where('id','[0-9]+');
        $order->get('/{order_id}/order-items', [Customer\OrderItemController::class,'index'])->where('order_id','[0-9]+');
        $order->post('store', [Customer\OrderController::class,'store']);
        $order->post('store-cart/{cart_id}', [Customer\OrderController::class,'storeCart'])->where('cart_id','[0-9]+');
        $order->post('update/{id}', [Customer\OrderController::class,'update'])->where('id','[0-9]+');
        });

    Route::group(['prefix' => 'products'], function ($products) {
        $products->get('/', [Customer\ProductController::class,'getWithImages']);
        $products->get('/category/{id_category}', [Customer\ProductController::class,'getWithImagesByCategoryId'])->where('id_category','[0-9]+');
        $products->get('/{id}', [Customer\ProductController::class,'getWithImagesById'])->where('id','[0-9]+');
        $products->post('/search', [Customer\ProductController::class,'search']);

        $products->get('{id}/{store_id}/comments', [Customer\CommentProductController::class,'index'])->where('id','[0-9]+')->where('store_id','[0-9]+');
        $products->post('{id}/{store_id}/comments', [Customer\CommentProductController::class,'store'])->where('id','[0-9]+')->where('store_id','[0-9]+');
        $products->post('/comments/delete/{id}',[Customer\ CommentProductController::class,'destroy'])->where('id','[0-9]+');
    });
});

Route::group([
    'prefix' => 'sections',
], function () {
    Route::get('', [Store\SectionController::class, 'index']);
    Route::get('{id}', [Store\SectionController::class, 'show']);
});

Route::group(['prefix' => 'categories',
    'namespace' => 'Store'], function ($categories) {
    $categories->get('/', [Store\CategoryController::class,'index']);
    $categories->get('/show/{id}', [Store\CategoryController::class,'show'])->where('id','[0-9]+');
    $categories->get('/by-store/{store_id}', [Store\CategoryController::class,'showByStore'])->where('store_id','[0-9]+');
    $categories->get('/parent', [Store\CategoryController::class,'mainCategories']);
});

Route::group(['prefix' => 'stories',
    'namespace' => 'Store'], function ($stories) {
    $stories->get('/', [Store\StoreController::class,'index']);
    $stories->get('/by-section/{section_id}', [Store\StoreController::class,'getBySection'])->where('section_id','[0-9]+');
    $stories->get('/by-category/{category_id}', [Store\StoreController::class,'getByCategory'])->where('category_id','[0-9]+');
    $stories->get('/show/{id}', [Store\StoreController::class,'show'])->where('id','[0-9]+');
    $stories->get('/show-info/{id}', [Store\StoreController::class,'showInfo'])->where('id','[0-9]+');
    $stories->post('/search', [Store\StoreController::class,'search']);
});
Route::group(['prefix' => 'products'], function ($products) {
    $products->get('/', [Store\ProductController::class,'index']);
    $products->get('/category/{id}', [Store\ProductController::class,'byCategoryId']);
});
