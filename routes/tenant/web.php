<?php

declare(strict_types=1);

use App\Http\Controllers\Firm\Store;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Firm\Manager;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Firm\DashboardController;

Route::get('login', [LoginController::class, 'show'])->name('login');
Route::post('login', [LoginController::class, 'login']);

Route::group([
    'as' => 'manager:',
    'prefix' => 'manager',
    'middleware' => 'auth:firm-user',
], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

    Route::group([
        'as' => 'orders.',
        'prefix' => 'orders',
    ], function () {
        Route::get('/', [Manager\Order\OrderController::class, 'index'])->name('index');
        Route::post('status/{status_id}', [Manager\Order\OrderStatusController::class, 'update'])->name('status.update');
    });

    Route::group([
        'as' => 'stores.',
        'prefix' => 'store'
    ], function () {
        Route::get('/', [Manager\Store\StoreController::class, 'index'])->name('index');
        Route::get('create', [Manager\Store\StoreController::class, 'create'])->name('create');
        Route::post('store', [Manager\Store\StoreController::class, 'store'])->name('store');
        Route::get('{id}', [Manager\Store\StoreController::class, 'show'])->name('show');

        Route::get('{id}/orders', [Manager\Store\StoreController::class, 'getOrders'])->name('orders.index');
    });
});

Route::group([
    'as' => 'store:',
    'middleware' => 'auth:firm-user',
], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('orders', [Store\Order\OrderController::class, 'index'])->name('orders');

    /*Route::get('products/filter', [PriceListController::class, 'filter'])->name('products.filter');
    Route::resource('products', PriceListController::class);
    Route::post('products/destroy', [PriceListController::class, 'destroy'])->name('products.destroy');*/

    Route::resource('overheads', Store\Overhead\OverheadController::class);
    Route::post('overheads/destroy', [Store\Overhead\OverheadController::class, 'destroy'])->name('overheads.destroy');
    Route::post('overheads/{overhead}/price-list', [Store\PriceListController::class, 'store'])->name('overheads.price_list.store');

    Route::group([
        'as' => 'price_list.',
        'prefix' => 'price-list',
    ], function () {
        Route::get('/', [Store\PriceListController::class, 'index'])->name('index');
        Route::post('destroy', [Store\PriceListController::class, 'destroy'])->name('destroy');
        Route::post('status', [Store\PriceListController::class, 'updateStatuses'])->name('update_statuses');
    });

    Route::group([
        'as' => 'orders.',
        'prefix' => 'orders',
    ], function () {
        Route::get('/', [Store\Order\OrderController::class, 'index'])->name('index');
    });
});

Route::get('account/profile', function () {
})->name('profile.edit');
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

/*
Route::group([
    'middleware' => 'auth:firm-user',
], function () {
    Route::get('/', 'Firm\DashboardController@index')->name('dashboard');

    Route::resource('products', 'Firm\Store\ProductController');
    Route::post('products/destroy', 'Firm\Store\ProductController@destroy')->name('products.destroy');

    Route::resource('price-lists', 'Firm\Store\Overhead\PriceListController')->except('index');
    Route::post('price-lists/destroy', 'Firm\Store\Overhead\PriceListController@destroy')->name('price-lists.destroy');

    Route::get('settings/users', 'Firm\Settings\UserController@index')->name('users.index');
    Route::post('settings/users', 'Firm\Settings\UserController@store')->name('users.store');
    Route::put('settings/users/{user}', 'Firm\Settings\UserController@update')->name('users.update');

    Route::get('couriers/all', [Store\Courier\CourierController::class, 'getAllCouriers'])->name('couriers.all');
    Route::get('couriers/{id}', [Store\Courier\CourierController::class, 'getCourierById'])->name('courier.get');
    Route::get('couriers/{id}/orders/uncompleted', [Store\Courier\CourierController::class, 'getCourierUncompletedOrders'])->name('courier.orders.uncompleted');
    Route::put('orders/{id}/assign-courier/{courier_id}', [Store\Order\OrderController::class, 'assignCourierToOrder'])->name('orders.assign_courier');
});*/
