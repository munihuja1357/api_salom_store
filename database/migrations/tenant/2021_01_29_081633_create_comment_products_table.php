<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('product_id')->nullable()->constrained('products');
            $table->unsignedBigInteger('parent_id')->default(0);
            $table->string('comment',1000)->index();
            $table->foreignId('customer_id')->nullable()->constrained('customers');
            $table->foreignId('store_id')->constrained();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment_products');
    }
}
