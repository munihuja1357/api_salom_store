<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name')->nullable();
            $table->date('birth_date')->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->string('position')->nullable();
            $table->string('phone_number')->unique();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->foreignId('contact_type_id')->nullable()->constrained('contact_types');
            $table->foreignId('source_id')->nullable()->constrained('traffic_sources');
            $table->foreignId('user_responsible_id')->constrained('users');
            $table->foreignId('created_by')->constrained('users');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
