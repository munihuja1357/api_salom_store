<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryMethods extends Migration
{
    public function up()
    {
        Schema::create('delivery_methods', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->decimal('cost', 15, 4);
            $table->integer('order_sort');
            $table->boolean('is_active');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_methods');
    }
}
