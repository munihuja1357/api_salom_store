<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropCategoryStoreTable extends Migration
{
    public function up()
    {
        Schema::drop('category_store');
    }

    public function down()
    {
        Schema::table('category_store', function (Blueprint $table) {
            //
        });
    }
}
