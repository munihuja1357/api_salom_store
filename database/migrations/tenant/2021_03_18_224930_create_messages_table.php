<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
    - chat_id
    - from_model_id
    - from_model_type
    - to_model_id
    - to_model_type
    - text
    - date
    -   replied_id - nullable
    -   deleted_at
    -   is_seen
    -   sort
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('chat_id')->constrained();
            $table->unsignedBigInteger('message_from_model_id');
            $table->string('message_from_model_type',255);
            $table->unsignedBigInteger('message_to_model_id');
            $table->string('message_to_model_type',255);
            $table->text('text');
            $table->boolean('is_seen');
            $table->integer('sort');
            $table->unsignedBigInteger('replied_id')->nullable()->constrained('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
