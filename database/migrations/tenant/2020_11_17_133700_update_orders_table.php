<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrdersTable extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_cancel_reason_id_foreign');
            $table->dropIndex('orders_cancel_reason_id_foreign');
            $table->unsignedBigInteger('cancel_reason_id')->change();
        });
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->index('orders_cancel_reason_id_foreign');
        });
    }
}
