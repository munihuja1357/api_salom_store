<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->nullable()->unique();
            $table->text('note')->nullable(); // замечание о клиенте
            $table->string('phone')->unique();
            $table->boolean('phone_verified')->default(false);
            $table->string('verification_code')->nullable();
            $table->timestamp('verification_code_expire')->nullable();
            $table->integer('total_orders')->default(0);
            $table->decimal('total_spent', 15, 4)->default(0); // общая потраченная сумма на заказы
            $table->string('password')->nullable();
            $table->string('status', 16);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
