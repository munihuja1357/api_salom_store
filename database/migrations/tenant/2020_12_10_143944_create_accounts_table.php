<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('type_id');
            $table->decimal('balance', 15, 4);
            $table->timestamp('date_opened');
            $table->timestamp('date_closed')->nullable();
            $table->boolean('is_blocked')->default(false);
            $table->unsignedBigInteger('owner_id');
            $table->string('owner_type');
        });

        Schema::create('account_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
