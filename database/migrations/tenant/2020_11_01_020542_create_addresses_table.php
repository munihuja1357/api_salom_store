<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alias')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('company')->nullable();
            $table->foreignId('city_id')->constrained('cities');
            $table->string('address', 100)->nullable();
            $table->string('address_secondary', 100)->nullable();
            $table->string('phone', 60)->nullable();
            $table->string('additional_phone', 60)->nullable();
            $table->jsonb('custom_fields')->nullable();
            $table->boolean('is_default')->default(false);
            $table->unsignedBigInteger('owner_id');
            $table->string('owner_type');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
