<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFirmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('firms', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name');
            $table->string('address')->nullable();
            $table->foreignId('city_id')->nullable()->constrained('cities');
            $table->foreignId('tenant_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('is_main')->default(false);
            $table->string('status', 16);
            $table->string('tenancy_db_name')->unique();
            $table->string('tenancy_db_username')->unique();
            $table->string('tenancy_db_password');
            $table->timestamps();
            $table->json('data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('firms');
    }
}

/*
    billings
        firm_id
        currency
        method
        amount
        billing_info
        date_of_payment
        status

 */
