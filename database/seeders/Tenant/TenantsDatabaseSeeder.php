<?php

namespace Database\Seeders\Tenant;

use Illuminate\Database\Seeder;

class TenantsDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsSeeder::class);
        $this->call(AccountTypesSeeder::class);
        $this->call(OrderStatusSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(PassportClientTableSeeder::class);
        $this->call(FakerSeeder::class);
    }
}
