<?php

namespace Database\Seeders\Tenant;

use Illuminate\Database\Seeder;
use App\Entity\Tenant\Firm\Store\Order\OrderStatus;
use App\Entity\Tenant\Firm\User;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::create([
            'name' => 'Необработанные',
            'is_unremovable' => true,
            'sort_order' => 0,
            'created_by' => 1,
        ]);

        OrderStatus::create([
            'name' => 'Доставляются',
            'is_unremovable' => true,
            'sort_order' => 2,
            'created_by' => 1,
        ]);

        OrderStatus::create([
            'name' => 'Доставлен',
            'is_unremovable' => true,
            'sort_order' => 3,
            'created_by' => 1,
        ]);

        OrderStatus::create([
            'name' => 'Отменён',
            'is_unremovable' => true,
            'is_hidden' => true,
            'sort_order' => 4,
            'created_by' => 1,
        ]);
    }
}
