<?php

namespace Database\Seeders\Tenant;

use Illuminate\Database\Seeder;
use App\Entity\Tenant\Firm\AccountType;

class AccountTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AccountType::create([
            'name' => 'Основной счет',
        ]);

        AccountType::create([
            'name' => 'Бонусный счет',
        ]);
    }
}
