<?php

namespace Database\Seeders\Tenant;

use Illuminate\Database\Seeder;
use Laravel\Passport\ClientRepository;

class PassportClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new ClientRepository();

        $client->createPasswordGrantClient(null, 'Default client', 'http://salom.crm.loc', 'firm-user');

        $client->createPasswordGrantClient(null, 'Salom Delivery APP Client', 'http://salom.crm.loc', 'customers');
    }
}
