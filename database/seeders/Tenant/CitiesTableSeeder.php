<?php

namespace Database\Seeders\Tenant;

use App\Entity\City;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::create([
            'name' => 'Худжанд',
        ]);
    }
}
