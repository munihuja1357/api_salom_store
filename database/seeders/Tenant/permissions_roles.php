<?php

return [
    'Owner' => [
        'product' => [
            'view',
            'view-any',
            'create',
            'update',
            'delete',
            'search',
        ],
    ],
    'Dispatcher' => [
        'product' => [
            'view',
            'view-any',
            'update',
            'create',
            'delete',
            'search',
        ],
        'user' => [
            'view',
            'view-any',
            'update',
            'create',
            'delete',
            'search',
        ],
        'store' => [
            'view',
            'view-any',
            'update',
            'create',
            'delete',
            'search',
        ],
        'overhead' => [
            'view',
            'view-any',
            'update',
            'create',
            'delete',
            'search',
        ],
        'order' => [
            'view',
            'view-any',
            'update',
            'create',
            'delete',
            'search',
        ],
    ],
    'Store' => [
        'product' => [
            'view',
            'update-self',
            'create',
            'delete',
            'search',
        ],
        'store' => [
            'view',
            'update-self',
            'create',
            'delete',
            'search',
        ],
        'overhead' => [
            'view',
            'update-self',
            'create',
            'delete',
            'search',
        ],
        'order' => [
            'view',
            'update-self',
            'create',
            'delete',
            'search',
        ],
    ],
];
