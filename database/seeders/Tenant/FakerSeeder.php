<?php

namespace Database\Seeders\Tenant;

use Illuminate\Database\Seeder;
use App\Entity\Tenant\Firm\City;
use App\Entity\Tenant\Firm\Customer\Customer;
use App\Entity\Tenant\Firm\User;
use App\Entity\Tenant\Firm\Address;
use App\Entity\Tenant\Firm\Store\Store;
use App\Entity\Tenant\Firm\Store\Manufacture;
use App\Entity\Tenant\Firm\Store\Product;
use App\Entity\Tenant\Firm\Store\Discount;
use App\Entity\Tenant\Firm\Store\Overhead;
use App\Entity\Tenant\Firm\Store\PriceList;
use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Entity\Tenant\Firm\Store\Order\OrderItem;
use App\Entity\Region;
use App\Entity\Tenant\Firm\Store\Courier;
use App\Entity\Tenant\Firm\Account;

class FakerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(City::class, 10)->create();
        // factory(User::class, 5)->create();
        factory(Customer::class, 20)->create();
        factory(Address::class, 50)->create();
        factory(Store::class, 10)->create();
        factory(Courier::class, 10)->create();
        factory(Account::class)->create();
        factory(Manufacture::class, 10)->create();
        factory(Product::class, 50)->create();
        factory(Discount::class, 10)->create();
        factory(Overhead::class, 15)->create();
        factory(PriceList::class, 50)->create();
        factory(Region::class, 15)->create();
        factory(Order::class, 20)->create();
        factory(OrderItem::class, 20)->create();
    }
}
