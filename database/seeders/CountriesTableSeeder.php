<?php

namespace Database\Seeders;

use App\Entity\City;
use App\Entity\Region;
use App\Entity\Country;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = Country::create([
            'iso' => 'TJK',
            'name' => 'Таджикистан',
        ]);

        $region = Region::create([
            'name' => 'Согд',
            'country_id' => $country->id,
        ]);

        City::create([
           'name' => 'Худжанд',
           'region_id' => $region->id,
        ]);
    }
}
