<?php

namespace Database\Seeders;

use App\Entity\Tenant\Tenant;
use Illuminate\Database\Seeder;
use App\Entity\Tenant\Firm\Firm;
use Database\Seeders\Tenant\PermissionsSeeder;

class TenantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsSeeder::class);
        $tenant = Tenant::create([
            'first_name' => 'SHEROZ',
            'last_name' => 'Juraev',
            'phone_number' => '446456554465',
            'email' => 'credocoder@gmail.com',
            'password' => bcrypt('12345678'),
        ]);

        $firm = Firm::create([
            'name' => 'Salom',
            'address' => 'Some address',
            'city_id' => 1,
            'tenant_id' => $tenant->id,
            'is_main' => 1,
            'is_active' => 1,
            'slug' => 'salom',
            'tenancy_db_name' => 'salom'.rand(1000, 9999),
            'tenancy_db_username' => 'tenant'.rand(1000, 9999),
            'tenancy_db_password' => bcrypt(rand(1000, 9999)),
        ]);

        $firm->domains()->create(['domain' => $firm->slug.'.crm.loc']);
    }
}
