<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\User;
use App\Entity\Tenant\Firm\Store\Store;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entity\Tenant\Firm\Store\Discount::class, function (Faker $faker) {
    return [
        'store_id' => factory(Store::class),
        'name' => $faker->name,
        'percent' => rand(1, 99),
        'from_date' => now(),
        'to_date' => $faker->dateTimeBetween('now', '+180 days'),
        'created_by' => factory(User::class),
        'is_active' => rand(0, 1),
    ];
});
