<?php

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\Account;
use App\Entity\Tenant\Firm\User;
use App\Entity\Tenant\Firm\Customer\Customer;
use App\Entity\Tenant\Firm\Store\Courier;
use App\Entity\Tenant\Firm\Store\Store;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Account::class, function (Faker $faker) {
    $ownerTables = [
        User::class,
        Customer::class,
        Courier::class,
        Store::class,
    ];
    $ownerTableType = $faker->randomElement($ownerTables);
    $ownerTable = factory($ownerTableType)->create();

    return [
        'name' => $faker->words(2, true),
        'type_id' => 1,
        'balance' => $faker->randomFloat(2, 10, 150),
        'date_opened' => now(),
        'is_blocked' => false,
        'owner_id' => $ownerTable->id,
        'owner_type' => $ownerTable->getMorphClass(),
    ];
});
