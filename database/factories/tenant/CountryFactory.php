<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entity\Country;
use Faker\Generator as Faker;

$factory->define(Country::class, function (Faker $faker) {
    return [
        'iso' => $faker->unique()->countryISOAlpha3,
        'name' => $faker->unique()->country,
    ];
});
