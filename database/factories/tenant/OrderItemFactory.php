<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\Store\Order\OrderItem;
use App\Entity\Tenant\Firm\Store\PriceList;
use App\Entity\Tenant\Firm\Store\Order\Order;

/*
 *

  `order_id`,
  ``,
  ``,
  `price`,
  ``,
  ``,
  ``,
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'product_name' => $faker->word,
        'price' => $faker->randomFloat(2, 0.1, 2000),
        'total' => $faker->randomFloat(2, 5, 15),
        'price_list_id' => factory(PriceList::class),
        'order_id' => factory(Order::class),
        'quantity' => rand(1, 4),
    ];
});
