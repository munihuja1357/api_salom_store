<?php

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\Store\Section;
use App\Entity\Tenant\Firm\User;
use Illuminate\Database\Eloquent\Factory;

/* @var Factory $factory */
$factory->define(Section::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'status' => Section::STATUS_ACTIVE,
        'parent_id' => factory(Section::class, 3),
        'created_by' => factory(User::class, 2),
    ];
});
