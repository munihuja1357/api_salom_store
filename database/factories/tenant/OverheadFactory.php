<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\User;
use App\Entity\Tenant\Firm\Store\Store;
use App\Entity\Tenant\Firm\Store\Discount;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entity\Tenant\Firm\Store\Overhead::class, function (Faker $faker) {
    $status = ['in_active', 'active'];

    return [
        'store_id' => factory(Store::class),
        'discount_id' => factory(Discount::class),
        'number' => rand(10000, 90000),
        'created_by' => factory(User::class),
        'status' => $status[rand(0, 1)],
    ];
});
