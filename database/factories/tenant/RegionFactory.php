<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entity\Region;
use Faker\Generator as Faker;
use App\Entity\Country;

$factory->define(Region::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'is_active' => rand(0, 1),
    ];
});
