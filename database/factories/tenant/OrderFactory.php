<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\User;
use App\Entity\Tenant\Firm\Address;
use App\Entity\Tenant\Firm\Store\Store;
use App\Entity\Tenant\Firm\Customer\Customer;
use App\Entity\Tenant\Firm\Store\Order\Order;
use App\Entity\Tenant\Firm\Store\Order\OrderStatus;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Order::class, function (Faker $faker) {
    return [
        'order_no' => $faker->randomNumber(6),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone' => $faker->phoneNumber,
        'note' => $faker->text(rand(20, 200)),
        'discount_percent' => rand(2, 20),
        'amount' => $faker->randomFloat(5, 5, 20000),
        'delivery_cost' => $faker->randomFloat(2, 5, 15),
        'delivered_at' => now(),
        'delivery_method_id' => 3,
        'delivery_address_id' => factory(Address::class),
        'customer_id' => $faker->randomElement(
            Customer::pluck('id')->all()
        ),
        'custom_fields' => json_encode(['key' => $faker->randomNumber()]),
        'store_id' => $faker->randomElement(
            Store::pluck('id')->all()
        ),
        'payment_method_id' => 1,
        'status_id' => $faker->randomElement(
            OrderStatus::pluck('id')->all()
        ),
        'responsible_user_id' => $faker->randomElement(
            User::pluck('id')->all()
        ),
    ];
});
