<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\Store\Manufacture;
use App\Entity\Tenant\Firm\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entity\Tenant\Firm\Store\Product::class, function (Faker $faker) {
    $status = ['active', 'deactivated'];

    return [
        'name' => $faker->name,
        'barcode' => $faker->bankAccountNumber,
        'short_description' => $faker->text(rand(20, 200)),
        'manufacture_id' => factory(Manufacture::class),
        'status' => $status[rand(0, 1)],
        'created_by' => factory(User::class),
    ];
});
