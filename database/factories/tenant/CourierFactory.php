<?php

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\User;
use App\Entity\Tenant\Firm\Store\Courier;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Courier::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class),
        'total_orders' => $faker->randomNumber(2),
        'is_free' => rand(0, 1),
        'lat' => $faker->latitude(40.22, 40, 29),
        'lng' => $faker->longitude(69.52, 69.80),
    ];
});
