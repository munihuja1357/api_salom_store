<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\Address;
use App\Entity\Tenant\Firm\City;
use App\Entity\Tenant\Firm\User;
use App\Entity\Tenant\Firm\Customer\Customer;
use App\Entity\Tenant\Firm\Store\Courier;
use App\Entity\Tenant\Firm\Store\Store;

/*
 *

  `order_id`,
  ``,
  ``,
  `price`,
  ``,
  ``,
  ``,
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Address::class, function (Faker $faker) {
    $ownerTables = [
        User::class,
        Customer::class,
        Store::class,
    ];
    $ownerTableType = $faker->randomElement($ownerTables);
    $ownerTable = factory($ownerTableType)->create();

    $alias = ['Офис', 'Дом', 'Квартира'];

    return [
        'city_id' => $faker->randomElement(
            City::pluck('id')->all()
        ),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone' => $faker->unique()->phoneNumber,
        'additional_phone' => $faker->unique()->phoneNumber,
        'address' => $faker->address,
        'address_secondary' => $faker->address,
        'company' => $faker->company,
        'alias' => $faker->randomElement($alias),
        'lat' => $faker->latitude(40.22, 40, 29),
        'lng' => $faker->longitude(69.52, 69.80),
        'owner_id' => $ownerTable->id,
        'owner_type' => $ownerTable->getMorphClass(),
    ];
});
