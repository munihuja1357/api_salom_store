<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\Store\Overhead;
use App\Entity\Tenant\Firm\Store\Discount;
use App\Entity\Tenant\Firm\Store\Product;
use App\Entity\Tenant\Firm\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entity\Tenant\Firm\Store\PriceList::class, function (Faker $faker) {
    $status = ['in_stock', 'out_stock'];

    return [
        'old_price' => $faker->randomFloat(2, 0.1, 200),
        'price' => $faker->randomFloat(2, 0.1, 250),
        'quantity' => rand(1, 15),
        'overhead_id' => factory(Overhead::class),
        'discount_id' => factory(Discount::class),
        'product_id' => factory(Product::class),
        'status' => $status[rand(0, 1)],
        'created_by' => factory(User::class),
        'updated_by' => factory(User::class),
    ];
});
