<?php

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\AccountType;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(AccountType::class, function (Faker $faker) {
    return [
        'name' => 'Основной счет',
    ];
});
