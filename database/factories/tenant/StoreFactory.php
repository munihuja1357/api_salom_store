<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Entity\Tenant\Firm\Store\Store;
use App\Entity\Tenant\Firm\User;

/*
 *

  `order_id`,
  ``,
  ``,
  `price`,
  ``,
  ``,
  ``,
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Store::class, function (Faker $faker) {
    $status = ['active', 'deactivated'];

    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'responsible_user_id' => factory(User::class),
        'owner_id' => factory(User::class),
        'status' => $status[rand(0, 1)],
        'lat' => $faker->latitude(40.22, 40, 29),
        'lng' => $faker->longitude(69.52, 69.80),
    ];
});
